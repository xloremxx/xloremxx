all:
	$(MAKE) -C xloremxx-000-simple-window
	$(MAKE) -C xloremxx-001-xlib-window
	$(MAKE) -C xloremxx-002-close-window-escape-key
	$(MAKE) -C xloremxx-003-draw-line
	$(MAKE) -C xloremxx-004-disable-resize-window
	$(MAKE) -C xloremxx-005-move-window-center
	$(MAKE) -C xloremxx-006-draw-text
	$(MAKE) -C xloremxx-007-mouse-left-click
	$(MAKE) -C xloremxx-008-change-window-background
	$(MAKE) -C xloremxx-009-mouse-left-press-release
	$(MAKE) -C xloremxx-010-draw-red-line
	$(MAKE) -C xloremxx-011-draw-pixel-center-window
	$(MAKE) -C xloremxx-012-circle-center-window
	$(MAKE) -C xloremxx-013-rect-center-window
	$(MAKE) -C xloremxx-014-filled-rect-center-win
	$(MAKE) -C xloremxx-015-pixel-mouse-press-loc
	$(MAKE) -C xloremxx-016-pixel-mouse-movement-loc
	$(MAKE) -C xloremxx-017-window-enter-leave
	$(MAKE) -C xloremxx-018-buffer
	$(MAKE) -C xloremxx-019-image
	$(MAKE) -C xloremxx-020-xlib-opengl
	$(MAKE) -C xloremxx-021-xlib-unblock-next-event
	$(MAKE) -C xloremxx-022-unblock-socket
	$(MAKE) -C xloremxx-023-half-pyramid
	$(MAKE) -C xloremxx-024-pyramid
	$(MAKE) -C xloremxx-025-xlib-grid
	$(MAKE) -C xloremxx-026-button
	$(MAKE) -C xloremxx-027-glad-test
	$(MAKE) -C xloremxx-028-opengl-triangle
	$(MAKE) -C xloremxx-029-xlib-text
	$(MAKE) -C xloremxx-030-pipe-and-fork
	$(MAKE) -C xloremxx-031-opengl-rect
	$(MAKE) -C xloremxx-032-opengl-texture
	$(MAKE) -C xloremxx-033-opengl-rotation
	$(MAKE) -C xloremxx-034-opengl-cube
	$(MAKE) -C xloremxx-035-xlib-child-window

	$(MAKE) -C xloremxx-036-winapi-window
	$(MAKE) -C xloremxx-037-winapi-opengl
	$(MAKE) -C xloremxx-038-time
	$(MAKE) -C xloremxx-039-conio-graphics
	$(MAKE) -C xloremxx-040-directx-test
	$(MAKE) -C xloremxx-041-random-number
	$(MAKE) -C xloremxx-042-alsa-test
	$(MAKE) -C xloremxx-043-alsa-test-2
	$(MAKE) -C xloremxx-044-alsa-sound-playback
	$(MAKE) -C xloremxx-045-alsa-sound-playback-2
	$(MAKE) -C xloremxx-046-clib-opengl-cube
	$(MAKE) -C xloremxx-047-xlib-resize-event
	$(MAKE) -C xloremxx-048-xlib-opengl-text
	$(MAKE) -C xloremxx-049-xlib-opengl-fps
	$(MAKE) -C xloremxx-050-read-dir
	$(MAKE) -C xloremxx-051-fork-test
	$(MAKE) -C xloremxx-052-xlib-grid-2
	$(MAKE) -C xloremxx-053-xcb-window
	$(MAKE) -C xloremxx-054-xcb-opengl
	$(MAKE) -C xloremxx-055-administration
	$(MAKE) -C xloremxx-056-xlib-random-lines
	$(MAKE) -C xloremxx-057-xlib-motion
	$(MAKE) -C xloremxx-058-xlib-mouse-wheel
	$(MAKE) -C xloremxx-059-clib-container-box

install:
	$(MAKE) -C xloremxx-000-simple-window install
	$(MAKE) -C xloremxx-001-xlib-window install
	$(MAKE) -C xloremxx-002-close-window-escape-key install
	$(MAKE) -C xloremxx-003-draw-line install
	$(MAKE) -C xloremxx-004-disable-resize-window install
	$(MAKE) -C xloremxx-005-move-window-center install
	$(MAKE) -C xloremxx-006-draw-text install
	$(MAKE) -C xloremxx-007-mouse-left-click install
	$(MAKE) -C xloremxx-008-change-window-background install
	$(MAKE) -C xloremxx-009-mouse-left-press-release install
	$(MAKE) -C xloremxx-010-draw-red-line install
	$(MAKE) -C xloremxx-011-draw-pixel-center-window install
	$(MAKE) -C xloremxx-012-circle-center-window install
	$(MAKE) -C xloremxx-013-rect-center-window install
	$(MAKE) -C xloremxx-014-filled-rect-center-win install
	$(MAKE) -C xloremxx-015-pixel-mouse-press-loc install
	$(MAKE) -C xloremxx-016-pixel-mouse-movement-loc install
	$(MAKE) -C xloremxx-017-window-enter-leave install
	$(MAKE) -C xloremxx-018-buffer install
	$(MAKE) -C xloremxx-019-image install
	$(MAKE) -C xloremxx-020-xlib-opengl install
	$(MAKE) -C xloremxx-021-xlib-unblock-next-event install
	$(MAKE) -C xloremxx-022-unblock-socket install
	$(MAKE) -C xloremxx-023-half-pyramid install
	$(MAKE) -C xloremxx-024-pyramid install
	$(MAKE) -C xloremxx-025-xlib-grid install
	$(MAKE) -C xloremxx-026-button install
	$(MAKE) -C xloremxx-027-glad-test install
	$(MAKE) -C xloremxx-028-opengl-triangle install
	$(MAKE) -C xloremxx-029-xlib-text install
	$(MAKE) -C xloremxx-030-pipe-and-fork install
	$(MAKE) -C xloremxx-031-opengl-rect install
	$(MAKE) -C xloremxx-032-opengl-texture install
	$(MAKE) -C xloremxx-033-opengl-rotation install
	$(MAKE) -C xloremxx-034-opengl-cube install
	$(MAKE) -C xloremxx-035-xlib-child-window install
	$(MAKE) -C xloremxx-036-winapi-window install
	$(MAKE) -C xloremxx-037-winapi-opengl install
	$(MAKE) -C xloremxx-038-time install
	$(MAKE) -C xloremxx-039-conio-graphics install
	$(MAKE) -C xloremxx-040-directx-test install
	$(MAKE) -C xloremxx-041-random-number install
	$(MAKE) -C xloremxx-042-alsa-test install
	$(MAKE) -C xloremxx-043-alsa-test-2 install
	$(MAKE) -C xloremxx-044-alsa-sound-playback install
	$(MAKE) -C xloremxx-045-alsa-sound-playback-2 install
	$(MAKE) -C xloremxx-046-clib-opengl-cube install
	$(MAKE) -C xloremxx-047-xlib-resize-event install
	$(MAKE) -C xloremxx-048-xlib-opengl-text install
	$(MAKE) -C xloremxx-049-xlib-opengl-fps install
	$(MAKE) -C xloremxx-050-read-dir install
	$(MAKE) -C xloremxx-051-fork-test install
	$(MAKE) -C xloremxx-052-xlib-grid-2 install
	$(MAKE) -C xloremxx-053-xcb-window install
	$(MAKE) -C xloremxx-054-xcb-opengl install
	$(MAKE) -C xloremxx-055-administration install
	$(MAKE) -C xloremxx-056-xlib-random-lines install
	$(MAKE) -C xloremxx-057-xlib-motion install
	$(MAKE) -C xloremxx-058-xlib-mouse-wheel install
	$(MAKE) -C xloremxx-059-clib-container-box install

clean:
	$(MAKE) -C xloremxx-000-simple-window clean
	$(MAKE) -C xloremxx-001-xlib-window clean
	$(MAKE) -C xloremxx-002-close-window-escape-key clean
	$(MAKE) -C xloremxx-003-draw-line clean
	$(MAKE) -C xloremxx-004-disable-resize-window clean
	$(MAKE) -C xloremxx-005-move-window-center clean
	$(MAKE) -C xloremxx-006-draw-text clean
	$(MAKE) -C xloremxx-007-mouse-left-click clean
	$(MAKE) -C xloremxx-008-change-window-background clean
	$(MAKE) -C xloremxx-009-mouse-left-press-release clean
	$(MAKE) -C xloremxx-010-draw-red-line clean
	$(MAKE) -C xloremxx-011-draw-pixel-center-window clean
	$(MAKE) -C xloremxx-012-circle-center-window clean
	$(MAKE) -C xloremxx-013-rect-center-window clean
	$(MAKE) -C xloremxx-014-filled-rect-center-win clean
	$(MAKE) -C xloremxx-015-pixel-mouse-press-loc clean
	$(MAKE) -C xloremxx-016-pixel-mouse-movement-loc clean
	$(MAKE) -C xloremxx-017-window-enter-leave clean 
	$(MAKE) -C xloremxx-018-buffer clean
	$(MAKE) -C xloremxx-019-image clean 
	$(MAKE) -C xloremxx-020-xlib-opengl clean 
	$(MAKE) -C xloremxx-021-xlib-unblock-next-event clean
	$(MAKE) -C xloremxx-022-unblock-socket clean
	$(MAKE) -C xloremxx-023-half-pyramid clean
	$(MAKE) -C xloremxx-024-pyramid clean
	$(MAKE) -C xloremxx-025-xlib-grid clean
	$(MAKE) -C xloremxx-026-button clean
	$(MAKE) -C xloremxx-027-glad-test clean
	$(MAKE) -C xloremxx-028-opengl-triangle clean
	$(MAKE) -C xloremxx-029-xlib-text clean
	$(MAKE) -C xloremxx-030-pipe-and-fork clean
	$(MAKE) -C xloremxx-031-opengl-rect clean 
	$(MAKE) -C xloremxx-032-opengl-texture clean 
	$(MAKE) -C xloremxx-033-opengl-rotation clean
	$(MAKE) -C xloremxx-034-opengl-cube clean
	$(MAKE) -C xloremxx-035-xlib-child-window clean
	$(MAKE) -C xloremxx-036-winapi-window clean
	$(MAKE) -C xloremxx-037-winapi-opengl clean
	$(MAKE) -C xloremxx-038-time clean
	$(MAKE) -C xloremxx-039-conio-graphics clean
	$(MAKE) -C xloremxx-040-directx-test clean
	$(MAKE) -C xloremxx-041-random-number clean
	$(MAKE) -C xloremxx-042-alsa-test clean
	$(MAKE) -C xloremxx-043-alsa-test-2 clean
	$(MAKE) -C xloremxx-044-alsa-sound-playback clean
	$(MAKE) -C xloremxx-045-alsa-sound-playback-2 clean
	$(MAKE) -C xloremxx-046-clib-opengl-cube clean
	$(MAKE) -C xloremxx-047-xlib-resize-event clean
	$(MAKE) -C xloremxx-048-xlib-opengl-text clean
	$(MAKE) -C xloremxx-049-xlib-opengl-fps clean
	$(MAKE) -C xloremxx-050-read-dir clean
	$(MAKE) -C xloremxx-051-fork-test clean
	$(MAKE) -C xloremxx-052-xlib-grid-2 clean
	$(MAKE) -C xloremxx-053-xcb-window clean
	$(MAKE) -C xloremxx-054-xcb-opengl clean
	$(MAKE) -C xloremxx-055-administration clean
	$(MAKE) -C xloremxx-056-xlib-random-lines clean
	$(MAKE) -C xloremxx-057-xlib-motion clean
	$(MAKE) -C xloremxx-058-xlib-mouse-wheel clean
	$(MAKE) -C xloremxx-059-clib-container-box clean

uninstall:
	$(MAKE) -C xloremxx-000-simple-window uninstall
	$(MAKE) -C xloremxx-001-xlib-window uninstall
	$(MAKE) -C xloremxx-002-close-window-escape-key uninstall
	$(MAKE) -C xloremxx-003-draw-line uninstall
	$(MAKE) -C xloremxx-004-disable-resize-window uninstall
	$(MAKE) -C xloremxx-005-move-window-center uninstall
	$(MAKE) -C xloremxx-006-draw-text uninstall
	$(MAKE) -C xloremxx-007-mouse-left-click uninstall
	$(MAKE) -C xloremxx-008-change-window-background uninstall
	$(MAKE) -C xloremxx-009-mouse-left-press-release uninstall
	$(MAKE) -C xloremxx-010-draw-red-line uninstall
	$(MAKE) -C xloremxx-011-draw-pixel-center-window uninstall
	$(MAKE) -C xloremxx-012-circle-center-window uninstall
	$(MAKE) -C xloremxx-013-rect-center-window uninstall
	$(MAKE) -C xloremxx-014-filled-rect-center-win uninstall
	$(MAKE) -C xloremxx-015-pixel-mouse-press-loc uninstall
	$(MAKE) -C xloremxx-016-pixel-mouse-movement-loc uninstall
	$(MAKE) -C xloremxx-017-window-enter-leave uninstall 
	$(MAKE) -C xloremxx-018-buffer uninstall
	$(MAKE) -C xloremxx-019-image uninstall 
	$(MAKE) -C xloremxx-020-xlib-opengl uninstall 
	$(MAKE) -C xloremxx-021-xlib-unblock-next-event uninstall
	$(MAKE) -C xloremxx-022-unblock-socket uninstall
	$(MAKE) -C xloremxx-023-half-pyramid uninstall
	$(MAKE) -C xloremxx-024-pyramid uninstall
	$(MAKE) -C xloremxx-025-xlib-grid uninstall
	$(MAKE) -C xloremxx-026-button uninstall
	$(MAKE) -C xloremxx-027-glad-test uninstall
	$(MAKE) -C xloremxx-028-opengl-triangle uninstall
	$(MAKE) -C xloremxx-029-xlib-text uninstall
	$(MAKE) -C xloremxx-030-pipe-and-fork uninstall
	$(MAKE) -C xloremxx-031-opengl-rect uninstall 
	$(MAKE) -C xloremxx-032-opengl-texture uninstall 
	$(MAKE) -C xloremxx-033-opengl-rotation uninstall
	$(MAKE) -C xloremxx-034-opengl-cube uninstall
	$(MAKE) -C xloremxx-035-xlib-child-window uninstall

	$(MAKE) -C xloremxx-036-winapi-window uninstall
	$(MAKE) -C xloremxx-037-winapi-opengl uninstall
	$(MAKE) -C xloremxx-038-time uninstall
	$(MAKE) -C xloremxx-039-conio-graphics uninstall
	$(MAKE) -C xloremxx-040-directx-test uninstall
	$(MAKE) -C xloremxx-041-random-number uninstall
	$(MAKE) -C xloremxx-042-alsa-test uninstall
	$(MAKE) -C xloremxx-043-alsa-test-2 uninstall
	$(MAKE) -C xloremxx-044-alsa-sound-playback uninstall
	$(MAKE) -C xloremxx-045-alsa-sound-playback-2 uninstall
	$(MAKE) -C xloremxx-046-clib-opengl-cube uninstall
	$(MAKE) -C xloremxx-047-xlib-resize-event uninstall
	$(MAKE) -C xloremxx-048-xlib-opengl-text uninstall
	$(MAKE) -C xloremxx-049-xlib-opengl-fps uninstall
	$(MAKE) -C xloremxx-050-read-dir uninstall
	$(MAKE) -C xloremxx-051-fork-test uninstall
	$(MAKE) -C xloremxx-052-xlib-grid-2 uninstall
	$(MAKE) -C xloremxx-053-xcb-window uninstall
	$(MAKE) -C xloremxx-054-xcb-opengl uninstall
	$(MAKE) -C xloremxx-055-administration uninstall
	$(MAKE) -C xloremxx-056-xlib-random-lines uninstall
	$(MAKE) -C xloremxx-057-xlib-motion uninstall
	$(MAKE) -C xloremxx-058-xlib-mouse-wheel uninstall
	$(MAKE) -C xloremxx-059-clib-container-box uninstall
