#include "app.h"
#include <linmath.h>

void vec2_set(vec3 v, float x, float y) {
  v[X] = x;
  v[Y] = y;
}

void vec3_set(vec3 v, float x, float y, float z) {
  v[X] = x;
  v[Y] = y;
  v[Z] = z;
}

void vec4_set(vec3 v, float x, float y, float z, float w) {
  v[X] = x;
  v[Y] = y;
  v[Z] = z;
  v[W] = w;
}

void vec3_set_vec3(vec3 a, vec3 b) {
  a[X] = b[X];
  a[Y] = b[Y];
  a[Z] = b[Z];
}
