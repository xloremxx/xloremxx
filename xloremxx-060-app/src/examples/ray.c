#include <stdio.h>
#include <stdlib.h>
#include "../app.h"

#define VAO_CUBE 0
#define VBO_CUBE 0
#define EBO_CUBE 0

#define VAO_2D_LINE 1
#define VBO_2D_LINE 1

#define VAO_2D_RECT 2
#define VBO_2D_RECT 2

#define LOCAL_CAMERA_MAIN 0
#define LOCAL_CAMERA_SECONDARY  1

static int activeCamera = LOCAL_CAMERA_MAIN;

static void animate();
static struct Camera *mainCamera;
static struct Camera secondaryCamera;

static float width, height;

static struct Mouse *mouse;

static unsigned int vaos[3];
static unsigned int vbos[3];
static unsigned int ebos[1];

static void keyboard(int key, int scancode, int action, int mods) {
  if (key == KEY_F2 && action == PRESS) {
    if (activeCamera == LOCAL_CAMERA_MAIN) {
      activeCamera = LOCAL_CAMERA_SECONDARY;
      CameraSetActiveCamera(&secondaryCamera);
    } else {
      activeCamera = LOCAL_CAMERA_MAIN;
      CameraSetActiveCamera(mainCamera);
    }
  }
}

void LoadExampleRay() {
  WindowGetDimensions(&width, &height);
  mainCamera = MainReturnCamera();
  mouse = WindowReturnMousePointer();

  WindowAddKeyboard(keyboard);

  glGenVertexArrays(3, vaos);
  glGenBuffers(3, vbos);
  glGenBuffers(1, ebos);

  static float cube_vertices[] = {
    /* backside */
    -1.0f, -1.0f, -1.0f,
    +1.0f, -1.0f, -1.0f,
    +1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f,

    /* frontside */
    -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f,
  };

  static unsigned int cube_indices[] = {
    /* back side */
    0, 1, 2, 
    0, 2, 3,
    /* front side */
    4, 5, 6,
    4, 6, 7,
    /* left side */
    4, 0, 3,
    4, 3, 7,
    /* right side */
    5, 1, 2, 
    5, 2, 6,
    /* bottom side */
    4, 5, 1,
    4, 1, 0,
    /* top side */
    7, 6, 2, 
    7, 2, 3,
  };

  static float rect_vertices[] = {
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,

    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
  };

  glBindVertexArray(vaos[VAO_2D_RECT]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_2D_RECT]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(rect_vertices), rect_vertices, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  static float line_vertices[2][3] = {
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
  };

  glBindVertexArray(vaos[VAO_2D_LINE]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_2D_LINE]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(line_vertices), line_vertices, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glBindVertexArray(vaos[VAO_CUBE]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_CUBE]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebos[EBO_CUBE]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  CameraCreate(&secondaryCamera, 
      0.0f, 0.0f, -10.0f, /* position */
      0.0f, 1.0f, 0.0f,  /* up */
      90.0f, 0.0f /* yaw, pitch */
      );

  MainAddLoop(animate);
}

static void BindVao(int index) {
  glBindVertexArray(vaos[index]);
}

static void DrawRay(float origin[3], float direction[3], float step, float maxLength, float color[4], float scale) {

  glBindVertexArray(vaos[VAO_CUBE]);

  float pos[3];
  mat4x4 model;

  for (float p = 0.0f; p < maxLength; p += step) {
    pos[X] = origin[X] + direction[X] * p;
    pos[Y] = origin[Y] + direction[Y] * p;
    pos[Z] = origin[Z] + direction[Z] * p;

    mat4x4_translate(model, pos[X], pos[Y], pos[Z]);
    mat4x4_scale_aniso(model, model, scale, scale, scale);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    ShaderColor4f(PROGRAM_DEFAULT, color[0], color[1], color[2], color[3]);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
  }

  glBindVertexArray(0);
}

static void DrawLine2D(float x1, float y1, float x2, float y2, float color[4]) {
  /* ShaderUse(PROGRAM_DEFAULT); */
  glBindVertexArray(vaos[VAO_2D_LINE]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_2D_LINE]);

  mat4x4 model;
  mat4x4_identity(model);

  float vertices[2][3] = {
    x1, y1, 0.0f,
    x2, y2, 0.0f,
  };

  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderSetModel(PROGRAM_DEFAULT, model);
  ShaderColor4f(PROGRAM_DEFAULT, color[0], color[1], color[2], color[3]);
  glDrawArrays(GL_LINES, 0, 2);

  glBindVertexArray(0);

}

static void DrawRay2D(float origin[2], float direction[2], float length, float color[4]) {
  float x1 = origin[X];
  float y1 = origin[Y];

  float x2 = origin[X] + direction[X] * length;
  float y2 = origin[Y] + direction[Y] * length;

  DrawLine2D(x1, y1, x2, y2, color);
}

static void DrawRect2D(float x, float y, float width, float height) {
  mat4x4 model;
  mat4x4_identity(model);

  float vertices[6][3] = {
    x, y, 0.0f,
    x + width, y, 0.0f,
    x + width, y + height, 0.0f,

    x + width, y + height, 0.0f,
    x, y + height, 0.0f,
    x, y, 0.0f,
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbos[VAO_2D_RECT]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(vaos[VAO_2D_RECT]);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0);
}

static int LineIntersect(float a[2], float b[2], float c[2], float d[2]) {
  /* get it in our additive vector form */
  
  /* r = ( b - a ) */
  /* s = ( d - c ) */

  /* d = r.x * s.y - r.y * s.x  */

  float r[2];

  r[X] = b[X] + -a[X];
  r[Y] = b[Y] + -a[Y];

  float s[2];

  s[X] = d[X] + -c[X];
  s[Y] = d[Y] + -c[Y];

  float n = r[X] * s[Y] - r[Y] * s[X];

  float u = ( (c[X]-a[X]) * r[Y] - (c[Y]-a[Y] * r[X]) ) / n;
  float t = ( (c[X]-a[X]) * s[Y] - (c[Y]-a[Y] * s[X]) ) / n;

  if (0.0f <= u && u <= 1.0f && 0.0f <= t && t <= 1.0f) {
    float h[2] = { a[X] + t * r[X], a[Y] * t * r[Y] };
    return 1;
  }


  return 0;
}

static void animate() {

  float color[4] = { 1.0f, 0.0f, 1.0f, 1.0f };
  float green[4] = {0.0f, 1.0f, 0.0f, 1.0f};
  float blue[4] = { 0.309f, 0.462f, 0.890f, 1.0f };
  float red[4] = { 0.909f, 0.462f, 0.890f, 1.0f };
  float orange[4] = { 0.890f, 0.639f, 0.309f, 1.0f };

  mat4x4 model, view, projection;
  CameraGetActiveViewMatrix(view);
  ShaderGetProjection(projection, PROJECTION_PERSPECTIVE_DEFAULT);

  ShaderUse(PROGRAM_DEFAULT);
  BindVao(VAO_CUBE);
  ShaderIdentityAllLazy(PROGRAM_DEFAULT);

  /* set view and projection matrix */
  ShaderSetView(PROGRAM_DEFAULT, view);
  ShaderSetProjection(PROGRAM_DEFAULT, projection);

  /* draw cube */
  /* mat4x4_translate(model, 0.0f, 0.0f, 0.0f); */
  /* mat4x4_scale_aniso(model, model, 0.2f, 0.2f, 0.2f); */
  /* ShaderSetModel(PROGRAM_DEFAULT, model); */
  /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 0.2f); */
  /* glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0); */

  /* secondary camera cube */
  if (activeCamera != LOCAL_CAMERA_SECONDARY) {
    mat4x4_translate(model, secondaryCamera.position[X], secondaryCamera.position[Y], secondaryCamera.position[Z]);
    mat4x4_scale_aniso(model, model, 0.2f, 0.2f, 0.2f);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 0.7f);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
  }

  /* secondary camera rays */
  if (activeCamera != LOCAL_CAMERA_SECONDARY) {
    /* draw secondary camera right */
    float color0[4] = {1.0f, 0.0f, 0.0f, 0.7f};
    DrawRay(secondaryCamera.position, secondaryCamera.right, 0.5f, 5.0f, color0, 0.025f);

    /* draw secondary camera front */
    float color1[4] = {0.0f, 0.0f, 1.0f, 0.7f};
    DrawRay(secondaryCamera.position, secondaryCamera.front, 0.5f, 5.0f, color1, 0.025f);

    /* draw secondary camera up */
    float color2[4] = {0.0f, 1.0f, 0.0f, 0.7f};
    DrawRay(secondaryCamera.position, secondaryCamera.up, 0.5f, 5.0f, color2, 0.025f);
  }

  /* main camera rays */
  if (activeCamera != LOCAL_CAMERA_MAIN) {
    /* draw main camera right */
    float color0[4] = {1.0f, 0.0f, 0.0f, 0.7f};
    DrawRay(mainCamera->position, mainCamera->right, 0.5f, 5.0f, color0, 0.025f);

    /* draw main camera front */
    float color1[4] = {0.0f, 0.0f, 1.0f, 0.7f};
    DrawRay(mainCamera->position, mainCamera->front, 0.5f, 5.0f, color1, 0.025f);

    /* draw main camera up */
    float color2[4] = {0.0f, 1.0f, 0.0f, 0.7f};
    DrawRay(mainCamera->position, mainCamera->up, 0.5f, 5.0f, color2, 0.025f);
  }

  /* secondary camera movement */
  if (WindowKeyIsDown(KEY_UP)) {
    CameraMove(&secondaryCamera, CAMERA_FORWARD, 0.001f);
  }
  if (WindowKeyIsDown(KEY_DOWN)) {
    CameraMove(&secondaryCamera, CAMERA_BACKWARD, 0.001f);
  }
  if (WindowKeyIsDown(KEY_LEFT)) {
    CameraMove(&secondaryCamera, CAMERA_LEFT, 0.001f);
  }
  if (WindowKeyIsDown(KEY_RIGHT)) {
    CameraMove(&secondaryCamera, CAMERA_RIGHT, 0.001f);
  }

  /* calculate ray mouse position */
  {
    mat4x4 inversedView, inversedProjection;
    mat4x4_invert(inversedProjection, projection);
    mat4x4_invert(inversedView, view);

    /* calculate ray in normalized device space */
    vec3 rayNds;

    rayNds[X] = (2.0f * mouse->position[X]) / width - 1.0f;
    rayNds[Y] = 1.0f -  (2.0f * mouse->position[Y]) / height;
    rayNds[Z] = 1.0f;

    /* calculate clip coordinates */
    vec4 rayClip;
    rayClip[X] = rayNds[X];
    rayClip[Y] = rayNds[Y];
    rayClip[Z] = -1.0f;
    rayClip[W] = +1.0f;

    /* /1* calculate eye coordinates *1/ */
    vec4 rayEye;
    mat4x4_mul_vec4(rayEye, inversedProjection, rayClip);
    rayEye[Z] = -1.0f;
    rayEye[W] = 0.0f;

    /* /1* calculate world coorinates *1/ */
    vec3 rayWorld;
    mat4x4_mul_vec4(rayWorld, inversedView, rayEye);
    vec3_norm(rayWorld, rayWorld);

    static struct {
      float origin[3];
      float direction[3];
    } ray;

    vec3_set(ray.origin, mainCamera->position[X], mainCamera->position[Y], mainCamera->position[Z]);
    vec3_set(ray.direction, rayWorld[X], rayWorld[Y], rayWorld[Z]);
    float color[4] = {1.0f, 1.0f, 0.0f, 1.0f};
    if (activeCamera != LOCAL_CAMERA_MAIN) {
      DrawRay(ray.origin, ray.direction, 0.5f, 5.0f, color, 0.025f);
    }

  }

  /* 2d plane intersection */
  {
    static struct {
      float origin[2];
      float direction[2];
    } ray;
    vec2_set(ray.origin, 3.0f, 2.0f);
    vec2_set(ray.direction, -0.4f, -0.5f);

    static struct {
      float position[2];
      float width;
      float color[4];
      float normal[2];
    } plane;

    vec4_set(plane.color, 1.0f, 1.0f, 0.0f, 1.0f);
    vec2_set(plane.position, 1.0f, -2.0f);
    vec2_set(plane.normal, 0.0f, 1.0f);
    plane.width = 3.0f;

    float color[4] = { 1.0f, 0.0f, 1.0f, 1.0f };
    float green[4] = {0.0f, 1.0f, 0.0f, 1.0f};
    float blue[4] = { 0.309f, 0.462f, 0.890f, 1.0f };
    float orange[4] = { 0.890f, 0.639f, 0.309f, 1.0f };

    float distance = sqrtf((plane.position[X]-ray.origin[X])*(plane.position[X]-ray.origin[X]) + (plane.position[Y]-ray.origin[Y])*(plane.position[Y]-ray.origin[Y]));

    /* DrawLine2D(-1.0f, 0.0f, +1.0f, 0.0f, color); */
    /* DrawLine2D(0.0f, -1.0f, 0.0f, 1.0f, color); */

    /* All points P on plane: P * n + distance = 0 */

    /* p1 = p1 * n + distance  */

    /* printf("distance: %.2f\n", distance); */

    /* plane.width * n + distance */

    /* float ax = plane.width * plane.normal[X] + distance; */
    /* float ay = plane.width * plane.normal[Y] - distance; */

    /* DrawRect2D(ax, ay, 1.0f, 1.0f); */

    /* float p1[2] = { */
    /*   1.0f * plane.normal[X] + distance, */
    /*   1.0f * plane.normal[Y] - distance, */
    /* }; */

    /* float p2[2] = { */
    /*   2.0f * plane.normal[X] + distance, */
    /*   2.0f * plane.normal[Y] - distance, */
    /* }; */

    /* printf("NORMAL: [%.2f, %.2f].\n", plane.normal[X], plane.normal[Y]); */

    /* DrawRect2D(p1[X], p1[Y], 1.0f, 1.0f); */
    /* DrawRect2D(p2[X], p2[Y], 1.0f, 1.0f); */

    /* DrawRect2D(p1[X], p1[Y], 1.0f, 1.0f); */

    /* origin * normal + distance /  */

    /* origin + direction * t */
    /* t = -( (origin * normal + distance) / (direction * normal) ) */

    /* float ox = ray.origin[X]; */
    /* float oy = ray.origin[Y]; */

    /* float nx = plane.normal[X]; */
    /* float ny = plane.normal[Y]; */

    /* float d = distance; */

    /* float Dx = ray.direction[X]; */
    /* float Dy = ray.direction[Y]; */

    /* origin * normal */
    /* x = ox*nx */
    /* y = oy*ny */
    /* v / v */

    /* origin - normal */
    /* x = ox + -nx */
    /* y = oy + -ny */

    /* draw plane */
    /* DrawLine2D(plane.position[X] - plane.width/2, plane.position[Y], plane.position[X] + plane.width/2, plane.position[Y], plane.color); */
    /* /1* draw plane normal *1/ */
    /* DrawLine2D(plane.position[X], plane.position[Y], plane.position[X] + plane.normal[X], plane.position[Y] + plane.normal[Y], color); */
    /* /1* draw 2d ray *1/ */
    /* DrawRay2D(ray.origin, ray.direction, 9.0f, orange); */
  }

  /* line intersection */
  {
    static float angle = 0.0f;

    angle += 0.0005f;

    /* line ab */

    /* a -> origin */
    /* b -> direction */
    /* */

    float a[2] = { 4.0f, 4.0f };
    float b[2] = { 6.0f, 3.0f };
    float c[2] = { 4.0f, 2.0f };
    float d[2] = { 6.0f, 5.0f };

    float adjacent = b[X] - a[X];
    float opposite = b[Y] - a[Y];
    float len = sqrtf(adjacent * adjacent + opposite * opposite);

    b[X] = a[X] + cosf(angle) * len;
    b[Y] = a[Y] + sinf(angle) * len;

    /* b[X] = cosf(angle) * len; */
    /* b[Y] = sinf(angle) * len; */

    /* printf("len: %.2f\n", len); */

    float *color = blue;
    if (LineIntersect(a, b, c, d)) {
      color = red;
    } 
    /* DrawLine2D(a[X], a[Y], b[X], b[Y], color); */
    /* DrawLine2D(c[X], c[Y], d[X], d[Y], color); */

  }

  /* draw line */
  {

    /* y = 4.0f * x - 1.0f */

    /* y = -x + 2.0f */

    /* 4 * x - 1 = -x * 2 */
    /*   */

    float y;

    float size = 0.05f;

    for (y=0.0f; y < 3.0f; y+=0.1f) {
      float x = 4.0f * y - 1.0f;
      /* DrawRect2D(x, y, size, size); */
    }

    for (y=0.0f; y < 3.0f; y+=0.1f) {
      float x = -y + 2.0f;
      /* DrawRect2D(x, y, size, size); */
    }
  }

  /* unbind vertex array object */
  glBindVertexArray(0);
}
