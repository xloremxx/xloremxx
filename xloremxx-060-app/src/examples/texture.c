#include "../app.h"
#include <stb_image.h>
#include <stdlib.h>

static unsigned int vao, vbo, texture;

static void loop() {
  ShaderUse(PROGRAM_TEXTURE);

  mat4x4 model;
  mat4x4_translate(model, 5.0f, 5.0f, 0.0f);
  ShaderSetModel(PROGRAM_TEXTURE, model);

  glBindTexture(GL_TEXTURE_2D, texture);

  glBindVertexArray(vao);
  glDrawArrays(GL_TRIANGLES, 0, 3);
  glBindVertexArray(0);
}

void LoadExampleTexture() {
  float vertices[3][3][2] = {
    -1.0f, -1.0f, 0.0f,   0.0f, 0.0f,
    +1.0f, -1.0f, 0.0f,   1.0f, 0.0f,
    +0.0f, +1.0f, 0.0f,   0.5f, 1.0f,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);  
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  int width, height, nrChannels;
  unsigned char *data;
  data = stbi_load("textures/wall.jpg", &width, &height, &nrChannels, 0); 
  if (!data) {
    data = stbi_load("/usr/share/xloremxx/xloremxx-060-app/textures/wall.jpg", &width, &height, &nrChannels, 0); 
    if (!data) {
      fprintf(stderr, "ERROR: Failed to load wall texture.\n");
      exit(EXIT_FAILURE);
    }
  }
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  stbi_image_free(data);
  glBindTexture(GL_TEXTURE_2D, 0);

  MainAddLoop(loop);
}
