#include <stdio.h>
#include "../app.h"

static int frame = 0;
static int changeDirectionAtFrame = 0;

static float width, height;

static struct {
  float rotation;
  float speed;
  float position[3];
  float direction[3];
} bot;

static void loop() {
  DrawCircleA2(bot.position, 0.1f);

  float B[3];

  vec3_add(B, bot.position, bot.direction);
  DrawLineA2(bot.position, B);

  float velocity[3];
  velocity[X] = bot.direction[X] * bot.speed;
  velocity[Y] = bot.direction[Y] * bot.speed;
  velocity[Z] = 0.0f;
  vec3_add(bot.position, bot.position, velocity);

  if (bot.position[X] > 10.0f) {
    bot.position[X] = -10.0f;
  }
  if (bot.position[X] < -10.0f) {
    bot.position[X] = 10.0f;
  }

  if (bot.position[Y] > 10.0f) {
    bot.position[Y] = -10.0f;
  }
  if (bot.position[Y] < -10.0f) {
    bot.position[Y] = 10.0f;
  }

  if (frame >= changeDirectionAtFrame) {
    frame = 0;
    changeDirectionAtFrame = CommonRandomI(3000, 10000);
    float a = CommonRandomF(0.0f, M_PI * 2);
    float n = CommonRandomF(-1.0f, 1.0f);

    bot.direction[X] = sqrtf(1.0f - n*n) * cos(a);
    bot.direction[Y] = sqrtf(1.0f - n*n) * sin(a);
    vec3_norm(bot.direction, bot.direction);
  }

  frame += 1;

}

void LoadExampleDemo2() {

  WindowGetDimensions(&width, &height);

  bot.speed = 0.01f;
  vec3_set(bot.position, 0.0f, 0.0f, 0.0f);
  vec3_set(bot.direction, 1.0f, 0.0f, 0.0f);

  changeDirectionAtFrame = CommonRandomI(3000, 10000);

  float a = CommonRandomF(0.0f, M_PI * 2);
  float n = CommonRandomF(-1.0f, 1.0f);

  bot.direction[X] = sqrtf(1 - n*n) * cos(a);
  bot.direction[Y] = sqrtf(1 - n*n) * sin(a);
  vec3_norm(bot.direction, bot.direction);

  MainAddLoop(loop);

}
