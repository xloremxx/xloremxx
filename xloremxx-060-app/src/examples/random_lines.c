#include "../app.h"

static void animate();

void LoadExampleRandomLines() {
  MainAddLoop(animate);
}

static void animate() {
  float red[4] = {1.0f, 0.0f, 0.0f, 1.0f};

  ShaderUse(PROGRAM_DEFAULT);

  int i = 0;

  for (i=0; i<30; i += 1) {
    float x1 = CommonRandomF(-5.0f, -4.0f);
    float y1 = CommonRandomF(-5.0f, -4.0f);
    float x2 = CommonRandomF(-5.0f, -4.0f);
    float y2 = CommonRandomF(-5.0f, -4.0f);
    CommonDrawLine2D(x1, y1, x2, y2, red);
  }

}
