#include "../app.h"
#include <stb_image.h>

static unsigned int vao, vbo, ebo, texture;

static struct Object ghost;

static void loop() {
  ShaderUse(PROGRAM_TEXTURE);

  glBindTexture(GL_TEXTURE_2D, texture);

  mat4x4 model;
  mat4x4_translate(model, 0.0f, 0.0f, 0.0f);
  mat4x4_translate(model, ghost.position[X], ghost.position[Y], ghost.position[Z]);
  ShaderSetModel(PROGRAM_TEXTURE, model);

  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  /* vec3_add(ghost.position, ghost.position, ghost.velocity); */
  ghost.position[X] += ghost.direction[X] * ghost.velocity[X];
  ghost.position[Y] += ghost.direction[Y] * ghost.velocity[Y];
  ghost.position[Z] += ghost.direction[Z] * ghost.velocity[Z];

  if (ghost.position[X] > +10.0f) {
    ghost.position[X] = -10.0f;
  }
  if (ghost.position[X] < -10.0f) {
    ghost.position[X] = +10.0f;
  }
  if (ghost.position[Y] > +10.0f) {
    ghost.position[Y] = -10.0f;
  }
  if (ghost.position[Y] < -10.0f) {
    ghost.position[Y] = +10.0f;
  }
}

void LoadExampleGhost() {
  MainAddLoop(loop);

  float vertices[4][3][2] = {
    -1.0f, -1.0f, 0.0f,   +0.0f, +0.0f,
    +1.0f, -1.0f, 0.0f,   +1.0f, +0.0f,
    +1.0f, +1.0f, 0.0f,   +1.0f, +1.0f,
    -1.0f, +1.0f, 0.0f,   +0.0f, +1.0f,
  };

  unsigned int indices[2][3] = {
    0, 1, 2,
    0, 2, 3,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); */
  /* float borderColor[] = { 1.0f, 0.0f, 0.0f, 1.0f }; */
  /* glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor); */  
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER); */	
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER); */ 
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); */	
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); */
  stbi_set_flip_vertically_on_load(GL_TRUE);  
  int width, height, nrChannels;
  unsigned char *data;
  data = stbi_load("textures/ghost.png", &width, &height, &nrChannels, 0); 
  if (data) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  } else {
    data = stbi_load("/usr/share/xloremxx/xloremxx-060-app/textures/ghost.png", &width, &height, &nrChannels, 0); 
    if (!data) {
      fprintf(stderr, "Failed to load texture.\n");
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  }
  glBindTexture(GL_TEXTURE_2D, 0);
  stbi_image_free(data);

  ghost.position[X] = +5.0f;
  ghost.position[Y] = -5.0f;
  ghost.position[Z] = +0.0f;

  ghost.velocity[X] = +0.005f;
  ghost.velocity[Y] = +0.005f;
  ghost.velocity[Z] = +0.000f;

  ghost.direction[X] = +0.0f;
  ghost.direction[Y] = +0.0f;
  ghost.direction[Z] = +0.0f;
}

struct Object *ExampleGhostGetPointer() {
  return &ghost;
}
