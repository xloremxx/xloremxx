#include <stdio.h>
#include "../app.h"

static unsigned int vao, vbo, ebo;

static struct {
  float V0[3];
  float V1[3];
} line = {
  -5.0f, 5.0f, 5.0f,
  -5.0f, 0.0f, -5.0f,
};

static struct {
  float position[3];
} box = {
  -5.0f, 10.0f, 0.0f,
};

static struct {
  float point[3];
  float normal[3];
} plane = {
  0.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f,
};

static float F[3] = {0.0f, 0.0f, 0.0f};

static int intersection = 0;

static void loop() {
  float s = 0.0f;

  mat4x4 model;

  ShaderUse(PROGRAM_DEFAULT);

  {
    /* draw quad */
    float p1[3] = {+10.0f, +15.0f, +0.0f};
    float p2[3] = {+10.0f, +14.0f, +0.0f};
    float p3[3] = {+15.0f, +14.0f, +0.0f};
    float p4[3] = {+15.0f, +14.0f, +0.0f};

    DrawQuad(p1, p2, p3, p4);

    /* draw plane */
    DrawPlane(plane.point, plane.normal);

    float P1[3] = { +3.0f, 25.0f, 0.0f };
    float P2[3] = { -3.0f, 30.0f, 0.0f };
    float n[3] = { 1.0f, 1.0f, 0.0f };

    DrawPlane2P(P1, P2, n);
  }

  /* draw arrow from P1 to P2 */
  {
    float P1[3] = { 0.0f, 0.0f, 0.0f };
    float P2[3] = { 0.0f, 5.0f, 3.0f };
    /* DrawArrow2P(P1, P2); */
  }

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 1.0f);
  DrawLineA1(line.V0[X], line.V0[Y], line.V0[Z], line.V1[X], line.V1[Y], line.V1[Z]);
  DrawLineA1(line.V0[X], line.V0[Y], line.V0[Z], line.V1[X], line.V0[Y], line.V1[Z]);
  DrawLineA1(line.V1[X], line.V1[Y], line.V1[Z], line.V1[X], line.V0[Y], line.V1[Z]);

  glBindVertexArray(vao);

  float distance = sqrtf( (line.V1[X]-line.V0[X])*(line.V1[X]-line.V0[X]) + (line.V1[Y]-line.V0[Y])*(line.V1[Y]-line.V0[Y]) + (line.V1[Z]-line.V0[Z])*(line.V1[Z]-line.V0[Z]) );
  float direction[3];
  vec3_sub(direction, line.V1, line.V0);
  vec3_norm(direction, direction);

  for (s=0.0f; s<=distance; s+=1.0f) {
    float position[3];

    position[X] = line.V0[X] + direction[X] * s;
    position[Y] = line.V0[Y] + direction[Y] * s;
    position[Z] = line.V0[Z] + direction[Z] * s;

    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 1.0f);
    mat4x4_translate(model, position[X], position[Y], position[Z]);
    mat4x4_scale_aniso(model, model, 0.05f, 0.05f, 0.05f);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
  }

  mat4x4_translate(model, box.position[X], box.position[Y], box.position[Z]);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
  if (intersection) {
  } else {
    ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 0.0f, 1.0f, 1.0f);
  }
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

  {
    F[X] = (box.position[X] + 2.0f - line.V0[X]) / (line.V1[X] - line.V0[X]);
    F[Y] = (box.position[Y] + 2.0f - line.V0[Y]) / (line.V1[Y] - line.V0[Y]);
    F[Z] = (box.position[Z] + 2.0f - line.V0[X]) / (line.V1[Z] - line.V0[Z]);

    float I[3];
    float B[3];

    B[X] = line.V1[X] - line.V0[X];
    B[Y] = line.V1[Y] - line.V0[Y];
    B[Z] = line.V1[Z] - line.V0[Z];

    I[X] = line.V0[X] + B[X] * F[X];
    I[Y] = line.V0[Y] + B[Y] * F[Y];
    I[Z] = line.V0[Z] + B[Z] * F[Z];

    float P[3];

    /* printf("F: %.2f, %.2f, %.2f\n", F[X], F[Y], F[Z]); */
    /* printf("I: %.2f, %.2f, %.2f\n", I[X], I[Y], I[Z]); */

    I[X] = 0.0f;
    I[Z] = 1.0f;

    mat4x4_translate(model, I[X], I[Y], I[Z]);
    mat4x4_scale_aniso(model, model, 0.1f, 0.1f, 0.1f);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    ShaderColor4f(PROGRAM_DEFAULT, 0.5f, 0.0f, 1.0f, 1.0f);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

    /* printf("%.2f\n", box.position[Y]); */
    /* float x = (box.position[X] - 1.0f - line.V0[X]) / (line.V1[X] - line.V0[X]); */
    /* float y = ((box.position[Y] - 1.0f) - line.V0[Y]) / (line.V1[Y] - line.V0[Y]); */
    /* float z = (box.position[Z] - 1.0f - (line.V0[Z])) / (line.V1[Z] - line.V0[Z]); */
    /* draw Fy point */
    /* float y = (box.position[Y] - line.V0[Y]) / (line.V1[Y]-line.V0[Y]); */
    /* mat4x4_translate(model, line.V1[X], y, line.V1[Z]); */
    /* mat4x4_scale_aniso(model, model, 0.1f, 0.1f, 0.1f); */
    /* ShaderSetModel(PROGRAM_DEFAULT, model); */
    /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f); */
    /* glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0); */
  }

  { 
    /* draw F */
    mat4x4_translate(model, F[X], F[Y], F[Z]);
    mat4x4_scale_aniso(model, model, 0.1f, 0.1f, 0.1f);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    ShaderColor4f(PROGRAM_DEFAULT, 0.5f, 0.0f, 1.0f, 1.0f);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
  }

  glLineWidth(2.0f);
  ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 0.0f, 0.0f, 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f, box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f, box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f);
  DrawLineA1(box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f);
  DrawLineA1(box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f, box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f, box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f, box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f, box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f, box.position[X] + 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] - 1.0f, box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] - 1.0f);
  DrawLineA1(box.position[X] - 1.0f, box.position[Y] + 1.0f, box.position[Z] + 1.0f, box.position[X] + 1.0f, box.position[Y] - 1.0f, box.position[Z] + 1.0f);

  glLineWidth(1.0f);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 1.0f);

  DrawLineA1(   box.position[X] - 20.0f,  box.position[Y] - 1.0f,   box.position[Z] + 0.0f,   box.position[X] + 20.0f, box.position[Y] - 1.0f,  box.position[Z] + 0.0f);
  DrawLineA1(   box.position[X] - 20.0f,  box.position[Y] + 1.0f,   box.position[Z] + 0.0f,   box.position[X] + 20.0f, box.position[Y] + 1.0f,  box.position[Z] + 0.0f);

  ShaderColor4f(PROGRAM_DEFAULT, 0.0f, box.position[Y]+1.0f, box.position[Z]+0.0f, 1.0f);

  DrawLineA1(   box.position[X] + 1.0f,   box.position[Y] - 20.0f,  box.position[Z] + 0.0f,   box.position[X] + 1.0f, box.position[Y] + 20.0f,  box.position[Z] + 0.0f);
  DrawLineA1(   box.position[X] - 1.0f,   box.position[Y] - 20.0f,  box.position[Z] + 0.0f,   box.position[X] - 1.0f, box.position[Y] + 20.0f,  box.position[Z] + 0.0f);

  ShaderColor4f(PROGRAM_DEFAULT, 0.0f, box.position[Y]+0.0f, box.position[Z]+1.0f, 1.0f);

  DrawLineA1(   box.position[X] + 1.0f,   box.position[Y] - 0.0f,   box.position[Z] + 20.0f,  box.position[X] + 1.0f, box.position[Y] + 0.0f,   box.position[Z] - 20.0f);
  DrawLineA1(   box.position[X] - 1.0f,   box.position[Y] - 0.0f,   box.position[Z] + 20.0f,  box.position[X] - 1.0f, box.position[Y] + 0.0f,   box.position[Z] - 20.0f);

  glLineWidth(1.0f);

  float speed = 0.01f;
  if (WindowKeyIsDown(KEY_LEFT)) {
    line.V1[X] -= speed;
  }
  if (WindowKeyIsDown(KEY_RIGHT)) {
    line.V1[X] += speed;
  }
  if (WindowKeyIsDown(KEY_UP)) {
    line.V1[Y] += speed;
  }
  if (WindowKeyIsDown(KEY_DOWN)) {
    line.V1[Y] -= speed;
  }

  {
    /* draw plane */
    /* C - plane.point */
    /* n - plane.normal */
  }
}

void LoadExampleAABB() {
  MainAddLoop(loop);

  static float vertices[] = {
    /* backside */
    -1.0f, -1.0f, -1.0f,
    +1.0f, -1.0f, -1.0f,
    +1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f,

    /* frontside */
    -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f,
  };

  static unsigned int indices[] = {
    /* back side */
    0, 1, 2, 
    0, 2, 3,
    /* front side */
    4, 5, 6,
    4, 6, 7,
    /* left side */
    4, 0, 3,
    4, 3, 7,
    /* right side */
    5, 1, 2, 
    5, 2, 6,
    /* bottom side */
    4, 5, 1,
    4, 1, 0,
    /* top side */
    7, 6, 2, 
    7, 2, 3,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}
