#include <stdio.h>
#include "../app.h"

#define CIRCLE_SEGMENTS 70

static struct {
  unsigned int circle;
} vao;

static struct {
  unsigned int circle;
} vbo;

static void DrawCircle(float x, float y, float z, float radius) {

  float vertices[CIRCLE_SEGMENTS][3];

  int i;
  float a;

  for (i=0; i<CIRCLE_SEGMENTS; i+=1) {
    float a = 2 * M_PI * i / CIRCLE_SEGMENTS;

    vertices[i][X] = x + cosf(a) * radius;
    vertices[i][Y] = y + sinf(a) * radius;
    vertices[i][Z] = z + 0.0f;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vbo.circle);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderIdentityModel(PROGRAM_DEFAULT);

  glBindVertexArray(vao.circle);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
  glDrawArrays(GL_TRIANGLE_FAN, 0, CIRCLE_SEGMENTS);
  glBindVertexArray(0);
}

static void loop() {
  ShaderUse(PROGRAM_DEFAULT);
  ShaderIdentityAllLazy(PROGRAM_DEFAULT);

  mat4x4 model, view, projection;

  /* get view and projection matrix */
  CameraGetActiveViewMatrix(view);
  ShaderGetProjection(projection, PROJECTION_PERSPECTIVE_DEFAULT);

  /* set view and projection matrix */
  ShaderSetView(PROGRAM_DEFAULT, view);
  ShaderSetProjection(PROGRAM_DEFAULT, projection);

  DrawCircle(-15.0f, 0.0f, 0.0f, 0.5f);
}

void LoadExampleCircle() {

  {
    float a;
    int i = 0;

    float vertices[CIRCLE_SEGMENTS][3] = {0};
    glGenVertexArrays(1, &vao.circle);
    glGenBuffers(1, &vbo.circle);
    glBindVertexArray(vao.circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.circle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  MainAddLoop(loop);
}
