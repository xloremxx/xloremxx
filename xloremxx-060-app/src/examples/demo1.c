#include "../app.h"

struct Circle {
  float origin[3];
  float angle;
  float radius;
  float position[3];
};

struct Circle circle;
struct Circle circleA;
struct Circle circleB;

static void CircleInitialize(struct Circle *circle, float x, float y, float z, float r) {
  vec3_set(circle->position, x, y, z);
  vec3_set_vec3(circle->origin, circle->position);
  circle->radius = r;

  circle->angle = 0.0f;
}

static void DrawVector(vec3 V) {
  vec3 A = {0.0f, 0.0f, 0.0f};
  vec3 B = { V[X], V[Y], V[Z] };

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 0.9f);
  DrawLineA2(A, B);

  vec3 C = { V[X], 0.0f, V[Z] };
  DrawLineA2(A, C);

  vec3 D = { C[X], V[Y], V[Z] };
  DrawLineA2(C, D);

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
}

static void loop() {
  mat4x4 view, projection;

  /* get view and projection matrix */
  CameraGetActiveViewMatrix(view);
  ShaderGetProjection(projection, PROJECTION_PERSPECTIVE_DEFAULT);

  /* set view and projection matrix */
  ShaderSetView(PROGRAM_DEFAULT, view);
  ShaderSetProjection(PROGRAM_DEFAULT, projection);

  /* draw circle */
  /* DrawCircleA1(0.0f, 0.0f, 0.0f, 0.5f); */

  DrawCircleA2(circle.position, circle.radius);
  DrawCircleA2(circleA.position, circleA.radius);
  DrawCircleA2(circleB.position, circleB.radius);

  /* float radius = 1.0f; */

  /* circle.position[X] = cosf(circle.angle) * radius; */
  /* circle.position[Y] = sinf(circle.angle) * radius; */

  /* circle.angle += 0.01f; */

  {
    /* circle a update */

    /* c - circle a */
    /* a - angle */
    /* o - offset */
    /* s - speed */

    struct Circle *c = &circleA;

    static float s = 0.01f;
    static float o = 1.0f;
    static float a = 0.0f;

    c->position[X] = c->origin[X] + cosf(a) * o;
    c->position[Y] = c->origin[Y] + sinf(a) * o;

    a += s;

    DrawCircleB2(c->origin, o);
    /* DrawVector(c->position); */
  }

  /* circle b update */
  {
    struct Circle *c = &circleB;
    static float a = 0.0f;
    static float s = 0.01f;
    static float o = 2.0f;

    c->position[Y] = c->origin[Y] + sinf(a) * o;

    a += s;

    DrawLineA1(c->origin[X], c->origin[Y] + o, c->origin[Z], c->origin[X], c->origin[Y] - o, c->origin[Z]);
    /* DrawVector(c->position); */
  }

  /* draw line from circle a to circle b */
  {
    DrawLineA2(circleA.position, circleB.position);
  }

  {
    struct Circle *c = &circle;
    struct Circle *a = &circleA;
    struct Circle *b = &circleB;

    /* mid point M */
    /* Mx = (Ax + Bx) / 2 */
    /* My = (Ay + By) / 2 */

    vec3 M, *A, *B;

    A = &a->position;
    B = &b->position;

    M[X] = ((*A)[X] + (*B)[X]) / 2;
    M[Y] = ((*A)[Y] + (*B)[Y]) / 2;

    M[Z] = 0.0f;

    c->position[X] = M[X];
    c->position[Y] = M[Y];
    c->position[Z] = M[Z];
  }
}

void LoadExampleDemo1() {
  MainAddLoop(loop);

  CircleInitialize(&circle, 0.0f, 0.0f, 0.0f, 0.1f);
  CircleInitialize(&circleA, -8.0f, -8.0f, 0.0f, 0.1f);
  CircleInitialize(&circleB, -5.0f, 0.0f, 0.0f, 0.1f);
}
