#include "../app.h"
#include <stb_image.h>
#include <stdlib.h>

static unsigned int vao, vbo, ebo, texture;
static struct Object pacman;
static struct Object *ghost;
static int frame = 0;
static int changeDirectionAtFrame = 0;

static void randomize_pacman_direction() {
  float a = CommonRandomF(0.0f, M_PI * 2);
  float n = CommonRandomF(-1.0f, 1.0f);

  pacman.direction[X] = sqrtf(1 - n*n) * cos(a);
  pacman.direction[Y] = sqrtf(1 - n*n) * sin(a);
  vec3_norm(pacman.direction, pacman.direction);

  changeDirectionAtFrame = CommonRandomI(3000, 10000);

  frame = 0;
}

static void loop() {
  ShaderUse(PROGRAM_TEXTURE);
  glBindTexture(GL_TEXTURE_2D, texture);

  mat4x4 model;
  mat4x4_translate(model, 0.0f, 0.0f, 0.0f);
  mat4x4_translate(model, pacman.position[X], pacman.position[Y], pacman.position[Z]);
  mat4x4_rotate_Z(model, model, pacman.rotation);

  ShaderSetModel(PROGRAM_TEXTURE, model);

  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  /* vec3_add(pacman.position, pacman.position, pacman.velocity); */
  pacman.position[X] += pacman.direction[X] * pacman.velocity[X];
  pacman.position[Y] += pacman.direction[Y] * pacman.velocity[Y];
  pacman.position[Z] += pacman.direction[Z] * pacman.velocity[Z];

  /* vec3_sub(ghost->position, ghost->position, pacman.position); */

  if (pacman.position[X] > +10.0f) {
    pacman.position[X] = -10.0f;
  }
  if (pacman.position[X] < -10.0f) {
    pacman.position[X] = +10.0f;
  }
  if (pacman.position[Y] > +10.0f) {
    pacman.position[Y] = -10.0f;
  }
  if (pacman.position[Y] < -10.0f) {
    pacman.position[Y] = +10.0f;
  }

  vec3_sub(ghost->direction, pacman.position, ghost->position);
  vec3_norm(ghost->direction, ghost->direction);

  if (frame >= changeDirectionAtFrame) {
    randomize_pacman_direction();
  }

  {
    /* d - distance
     * p - pacman
     * g - ghost
     */

    float PG[2];

    PG[X] = ghost->position[X] - pacman.position[X];
    PG[Y] = ghost->position[Y] - pacman.position[Y];

    float d = sqrtf( PG[X]*PG[X] + PG[Y]*PG[Y] );
    float a = asinf(PG[Y]/d);
    /* printf("Angle: %.2f\n", a * 180.0f / M_PI); */

    if (d < 2.0f) {
      float ra = CommonRandomF(0.0f, M_PI);
      pacman.direction[X] = cosf(a + M_PI/2.0f + ra);
      pacman.direction[Y] = sinf(a + M_PI/2.0f + ra);
      vec3_norm(pacman.direction, pacman.direction);
    }
    /* printf("DISTANCE: %.2f\n", d); */

    /* struct Object *p = &pacman; */
    /* struct Object *g = ghost; */

    /* float x1 = p->position[X]; */
    /* float y1 = p->position[Y]; */

    /* float AB[2]; */

    /* AB[X] = p->position[X] - g->position[X]; */
    /* AB[Y] = p->position[Y] - g->position[Y]; */
    /* float d = sqrtf(AB[X]*AB[X]+AB[Y]*AB[Y]); */

    /* if (d < 7.0f) { */

    /*   float a = atanf(AB[Y] / AB[X]); */
    /*   float ra = CommonRandomF(0.0f, M_PI); */
    /*   /1* printf("a -> ra [%.2f -> %.2f]\n", a * 180 / M_PI, ra * 180 / M_PI); *1/ */

    /*   p->direction[X] = cosf(a + M_PI / 2.0f + ra); */
    /*   p->direction[Y] = sinf(a + M_PI / 2.0f + ra); */
    /*   vec3_norm(p->direction, p->direction); */

    /* } */

    /* /1* float x2 = g->position[X]; *1/ */
    /* /1* float y2 = g->position[Y]; *1/ */

    /* /1* float o = y2 - y1; *1/ */
    /* /1* float a = asinf(AB[Y]/d); *1/ */

    /* ShaderUse(PROGRAM_DEFAULT); */
    /* DrawLineA1(0.0f, 0.0f, 0.0f, AB[X], AB[Y], 0.0f); */

    /* printf("a: %.2f\n", a * 180 / M_PI); */
    /* printf("distance: d: %.2f, o: %.2f, a: %.2f\n", d, o, a); */
    /* if (d < 3.0f) { */

    /* float a = atanf(o/d); */

    /* } */

  }

  {

    /* static float m = 0.0f; */
    /* static float r = 1.0f; */

    /* m += 0.001f; */

    /* float A[2] = { 1.0f, -1.0f }; */
    /* float B[2] = { 2.0f, 0.0f }; */

    /* float AB[2] = { B[X] - A[X], B[Y] - A[Y] }; */

    /* float a = atanf(AB[Y] / AB[X]); */

    /* float ra = CommonRandomF(0.0f, M_PI) + M_PI/2; */

    /* float M[2]; */
    /* M[X] = cosf(ra); */
    /* M[Y] = sinf(ra); */

    /* printf("%.2f <-> %.2f\n", (a-M_PI/2)*180/M_PI, (a+M_PI/2)*180/M_PI); */
    /* printf("Angle: %.2f\n", a * 180.0f / M_PI); */
    /* printf("Angle: %.2f\n", ra * 180.0f / M_PI); */

    /* ShaderUse(PROGRAM_DEFAULT); */
    /* DrawLineA1(A[X], A[Y], 0.0f, B[X], B[Y], 0.0f); */
    /* DrawLineA1(0.0f, 0.0f, 0.0f, AB[X], AB[Y], 0.0f); */
    /* DrawLineA1(0.0f, 0.0f, 0.0f, M[X], M[Y], 0.0f); */
  }

  frame += 1;

  {
    float rotation = atan2f(pacman.direction[Y], pacman.direction[X]);
    pacman.rotation = rotation;
    /* printf("rotation: %.2f\n", rotation * 180.0f / M_PI); */
  }
}

void LoadExamplePacman() {
  ghost = ExampleGhostGetPointer();

  MainAddLoop(loop);

  float vertices[4][3][2] = {
    -1.0f, -1.0f, 0.0f,   +0.0f, +0.0f,
    +1.0f, -1.0f, 0.0f,   +1.0f, +0.0f,
    +1.0f, +1.0f, 0.0f,   +1.0f, +1.0f,
    -1.0f, +1.0f, 0.0f,   +0.0f, +1.0f,
  };

  unsigned int indices[2][3] = {
    0, 1, 2,
    0, 2, 3,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); */
  /* float borderColor[] = { 1.0f, 0.0f, 0.0f, 1.0f }; */
  /* glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor); */  
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER); */	
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER); */ 
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); */	
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); */
  stbi_set_flip_vertically_on_load(GL_TRUE);  
  int width, height, nrChannels;
  unsigned char *data;
  data = stbi_load("textures/retro-pacman.png", &width, &height, &nrChannels, 0); 
  if (!data) {
    data = stbi_load("/usr/share/xloremxx/xloremxx-060-app/textures/retro-pacman.png", &width, &height, &nrChannels, 0); 
    if (!data) {
      fprintf(stderr, "Failed to load pacman texture.\n");
      exit(EXIT_FAILURE);
    }
  }
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  stbi_image_free(data);

  pacman.position[X] = +5.0f;
  pacman.position[Y] = -5.0f;
  pacman.position[Z] = +0.0f;

  pacman.velocity[X] = +0.009f;
  pacman.velocity[Y] = +0.009f;
  pacman.velocity[Z] = +0.000f;

  pacman.direction[X] = +0.0f;
  pacman.direction[Y] = +0.0f;
  pacman.direction[Z] = +0.0f;

  randomize_pacman_direction();
}
