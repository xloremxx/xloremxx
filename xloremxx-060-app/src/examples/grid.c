#include <stdio.h>
#include "../app.h"

static unsigned int vao, vbo;
static float scale = 10.0f;

static void animate() {
  ShaderUse(PROGRAM_DEFAULT);
  /* ShaderIdentityAllLazy(PROGRAM_DEFAULT); */

  float x_axis_alpha = 0.3f;
  float y_axis_alpha = 0.2f;
  float z_axis_alpha = 0.2f;

  mat4x4 model;
  mat4x4_translate(model, 0.0f, 0.0f, 0.0f);
  mat4x4_scale_aniso(model, model, scale, scale, scale);

  glBindVertexArray(vao);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, x_axis_alpha);

  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 21*2);

  mat4x4_rotate_Z(model, model, M_PI / 2);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 21*2);

  ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 1.0f, 0.0f, y_axis_alpha);
  mat4x4_rotate_X(model, model, M_PI / 2);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 21*2);

  mat4x4_rotate_Z(model, model, M_PI / 2);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 21*2);

  ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 0.0f, 1.0f, z_axis_alpha);
  mat4x4_rotate_X(model, model, M_PI / 2);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 21*2);

  mat4x4_rotate_Z(model, model, M_PI / 2);
  ShaderSetModel(PROGRAM_DEFAULT, model);

  glDrawArrays(GL_LINES, 0, 21*2);

  glBindVertexArray(0);
}

void LoadExampleGrid() {
  int i = 0;
  float vertices[21*2][3];

  float n = -1.0f;

  for (i=0; i<21*2; i+=2) {
    vertices[i][0] = n;
    vertices[i][1] = -1.0f;
    vertices[i][2] = 0.0f;

    vertices[i+1][0] = n;
    vertices[i+1][1] = +1.0f;
    vertices[i+1][2] = 0.0f;

    n += 0.1f;
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  MainAddLoop(animate);
}
