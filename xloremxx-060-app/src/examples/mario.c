#include <stdlib.h>
#include <stdio.h>
#include "../app.h"
#include <stb_image.h>

unsigned vao, vbo, ebo, texture;
static struct Object *mario;

static struct Object *MakeMario() {
  struct Object *mario = malloc(sizeof(struct Object));

  vec3_set(mario->position, 6.0f, 3.0f, 0.0f);
  vec3_set(mario->velocity, 0.0f, 0.0f, 0.0f);
  vec3_set(mario->gravity, 0.0f, -0.0002f, 0.0f);

  return mario;
}

static void keyboard(int key, int scancode, int action, int mode) {
  if (key == KEY_UP && action == PRESS) {
    vec3_set(mario->velocity, 0.0f, 0.05f, 0.0f);
  }
}

static void MarioUpdate() {
  float speed = 0.01f;

  vec3_add(mario->position, mario->position, mario->velocity);
  vec3_add(mario->velocity, mario->velocity, mario->gravity);

  if (WindowKeyIsDown(KEY_LEFT)) {
    mario->position[X] -= speed;
  }
  if (WindowKeyIsDown(KEY_RIGHT)) {
    mario->position[X] += speed;
  }

  if (mario->position[Y] < 0.0f) {
    mario->position[Y] = 0.0f;
  }
}

static void loop() {
  ShaderUse(PROGRAM_TEXTURE);
  glBindTexture(GL_TEXTURE_2D, texture);
  glBindVertexArray(vao);
  mat4x4 model;

  mat4x4_translate(model, mario->position[X], mario->position[Y], mario->position[Z]);
  ShaderSetModel(PROGRAM_TEXTURE, model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  MarioUpdate();
}

void LoadExampleMario() {
  MainAddLoop(loop);
  WindowAddKeyboard(keyboard);

  float vertices[4][3][2] = {
    -1.0f, -1.0f, 0.0f,   +0.0f, +0.0f,
    +1.0f, -1.0f, 0.0f,   +1.0f, +0.0f,
    +1.0f, +1.0f, 0.0f,   +1.0f, +1.0f,
    -1.0f, +1.0f, 0.0f,   +0.0f, +1.0f,
  };

  unsigned int indices[2][3] = {
    0, 1, 2,
    0, 2, 3,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (3+2) * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  stbi_set_flip_vertically_on_load(GL_TRUE);  
  int width, height, nrChannels;
  unsigned char *data;
  data = stbi_load("textures/retro-mario.png", &width, &height, &nrChannels, 0); 
  if (!data) {
    data = stbi_load("/usr/share/xloremxx/xloremxx-060-app/textures/retro-mario.png", &width, &height, &nrChannels, 0); 
    if (!data) {
      fprintf(stderr, "Failed to load mario texture.\n");
      exit(EXIT_FAILURE);
    }
  } 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  stbi_image_free(data);

  mario = MakeMario();
}
