#include <stdio.h>
#include "../app.h"

static void loop() {

  float V0[3] = { 0.0f, 0.0f, 0.0f };

  static float A[3] = { 0.0f, 0.0f, 0.0f };
  static float B[3] = { 0.0f, 0.0f, 0.0f };
  static float C[3] = { 0.0f, 0.0f, 0.0f };

  static struct {
    float speed;
    float up[3];
    float right[3];
    float front[3];
    float position[3];
  } box = { 
    0.0001f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, 0.0f,
  };

  ShaderUse(PROGRAM_DEFAULT);
  mat4x4 model;
  mat4x4_identity(model);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
  glLineWidth(3.0f);

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
  DrawCube(box.position, 0.1f);

  /* draw box up vector */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 1.0f, 0.0f, 1.0f);
    DrawLine(V0, box.up);
  }

  /* draw box position vector */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 1.0f);
    DrawLine(V0, box.position);
  }

  /* draw box right vector */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 1.0f, 1.0f, 1.0f);
    DrawLine(V0, box.right);
  }

  /* draw box front vector */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 1.0f, 1.0f);
    DrawLine(V0, box.front);
  }

  /* update box right vector */
  {
    vec3_mul_cross(box.right, box.front, box.up);
  }
  /* set box front to be 45 degrees */
  {
    static float pitch = 0.0f * M_PI / 180.0f;
    static float yaw = -90.0f * M_PI / 180.0f;
    box.front[X] = cosf(yaw) * cosf(pitch);
    box.front[Y] = sinf(pitch);
    box.front[Z] = sinf(yaw) * cosf(pitch);
    /* yaw += 0.0005f; */
  }

  /* draw point A and move 1 distance right from box */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
    DrawCube(A, 0.1f);
    float distance = 1;
    /* position = box position + box right direction * distance */
    A[X] = box.position[X] + box.right[X] * distance;
    A[Y] = box.position[Y] + box.right[Y] * distance;
    A[Z] = box.position[Z] + box.right[Z] * distance;
  }

  /* draw point B and move 1 distance left from box */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
    DrawCube(B, 0.1f);
    float distance = 1;
    /* position = box position + box right direction * distance */
    B[X] = box.position[X] - box.right[X] * distance;
    B[Y] = box.position[Y] - box.right[Y] * distance;
    B[Z] = box.position[Z] - box.right[Z] * distance;
  }

  /* draw box C */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
    DrawCube(C, 0.1f);
    float velocity[3] = {0};
    vec3_add(C, C, velocity);
  }

  /* draw a line */
  {
    ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 0.0f, 1.0f);
    float L[2][3] = { 
      0.0f, 0.0f, 6.0f,
      0.0f, -5.0f, -5.0f,
    };
    DrawLine(L[0], L[1]);

    /* draw point at line start position */
    {
      static int forward = 1;
      static float direction[3];
      float velocity[3];
      float distance;
      static float current_distance = 0.0f;
      static float P[3];

      vec3_sub(direction, L[1], L[0]);
      distance = sqrtf(direction[X]*direction[X]+direction[Y]*direction[Y]+direction[Z]*direction[Z]);
      vec3_norm(direction, direction);
      /* draw direction line */
      {
        ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 0.0f, 1.0f);
        float pos[3];
        vec3_add(pos, L[0], direction);
        DrawLine(L[0], pos);
      }

      P[X] = L[0][X] + direction[X] * current_distance;
      P[Y] = L[0][Y] + direction[Y] * current_distance;
      P[Z] = L[0][Z] + direction[Z] * current_distance;

      ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 1.0f, 0.0f, 1.0f);
      DrawCube(P, 0.1f);

      if (current_distance > distance) {
        forward = 0;
      }
      if (current_distance < 0.0f) {
        current_distance = 0.0f;
        forward = 1;
      }

      if (forward) {
        current_distance += 0.005f;
      } else {
        current_distance -= 0.005f;
      }
    }

    /* draw plane */
    {
      float n[3] = {0.0f, 1.0f, 0.0f};
      float P[4][3] = {
        -8.0f, -3.0f, -8.0f,
        +8.0f, -3.0f, -8.0f,
        +8.0f, -3.0f, +8.0f,
        -8.0f, -3.0f, +8.0f,
      };
      ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
      DrawQuad(P[0], P[1], P[2], P[3]);

      /* point C on plane */
      float C[3] = {0.0f, -3.0f, 7.0f};
      ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 0.0f, 1.0f, 1.0f);
      DrawCube(C, 0.1f);


      /* draw normal */
      {
        float D[3];
        float start[3] = {L[0][X], L[0][Y], L[0][Z]};
        float end[3];

        /* vec3_sub(start, D, C); */
        vec3_add(end, start, n);
        ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 1.0f, 1.0f);
        DrawLine(start, end);
      }
      /* draw normal on plane */
      {
        /* float D[3]; */
        /* float start[3] = {0.0f, 0.0f, 0.0f}; */
        /* float end[3] = {0.0f, 0.0f, 0.0f}; */
        /* vec3_sub(start, D, C); */
        /* vec3_sub(end, C, L[0]); */
        /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 0.0f, 1.0f, 1.0f); */
        /* DrawLine(start, end); */
      }
      /* draw line from L[0] to point C */
      {
        ShaderColor4f(PROGRAM_DEFAULT, 0.0f, 1.0f, 1.0f, 1.0f);
        DrawLine(L[0], C);
      }
    }

  }

  glLineWidth(1.0f);
  glPointSize(1.0f);
  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);

  /* box move front */
  {
    float distance = 0.0001f;
    float step[3];
    vec3_scale(step, box.front, distance);
    vec3_add(box.position, box.position, step);
  }
}

void LoadExampleVectors() {
  MainAddLoop(loop);
}
