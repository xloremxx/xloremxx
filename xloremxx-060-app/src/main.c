#include <time.h>
#include <linmath.h>
#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "app.h"

static float width = 1200.0f, height = 700.0f;

static unsigned int vao, vbo;

static struct Camera camera;
static float deltaTime = 0.0f; /* time between current frame and last frame */
static float lastFrame = 0.0f;

static int firstMouse = 1;

static struct {
  float position[2];
  float lastPosition[2];
} mouse = {
  0.0f, 0.0f,
  0.0f, 0.0f,
};

void mousemove(float x, float y) {
  if (firstMouse) {
    mouse.lastPosition[X] = x;
    mouse.lastPosition[Y] = y;
    firstMouse = 0;
  }

  mouse.position[X] = x;
  mouse.position[Y] = y;

  CameraProcessMouseMovement(&camera, mouse.position[X] - mouse.lastPosition[X], mouse.lastPosition[Y] - mouse.position[Y]);

  mouse.lastPosition[X] = mouse.position[X];
  mouse.lastPosition[Y] = mouse.position[Y];
}

static int numMainLoops = 0;
static void (**loops)();
void MainAddLoop(void (*loop)()) {
  loops = realloc(loops, sizeof(void (*)()) * (numMainLoops + 1));
  loops[numMainLoops] = loop;
  numMainLoops += 1;
}

struct Camera *MainReturnCamera() {
  return &camera;
}

int main(int argc, char *argv[]) {
  srand(time(NULL));

  printf("ARG: '%s'.\n", argv[0]);

  if (WindowStart(width, height) == -1) {
    fprintf(stderr, "ERROR: 'Failed to start window.'.\n");
    return -1;
  }

  WindowAddMouseMove(mousemove);

  if (ShaderStart() == -1) {
    fprintf(stderr, "ERROR: 'Shader was unable to start'.\n");
    return -1;
  }

  if (CommonInitialize() == -1) {
    fprintf(stderr, "ERROR: 'Common was unable to initialize.\n'");
    return -1;
  }

  DrawInitialize();

  CameraCreate(&camera, 
      0.0f, 0.0f, 20.0f, /* position */
      0.0f, 1.0f, 0.0f,  /* up */
      -90.0f, 0.0f /* yaw, pitch */
      );
  CameraSetActiveCamera(&camera);

  float vertices[3*3] = {
    -1.0f, -1.0f, 0.0f,
    +1.0f, -1.0f, 0.0f,
    +0.0f, +1.0f, 0.0f,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f);
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  LoadExampleGrid();
  LoadExampleVectors();
  LoadExampleRay();
  LoadExampleRandomLines();
  LoadExampleLineIntersection();
  LoadExampleCircle();
  LoadExampleDemo1();
  LoadExampleDemo2();
  LoadExampleTexture();
  LoadExamplePacman();
  LoadExampleGhost();
  LoadExampleMario();
  LoadExampleAABB();

  while (WindowOpen()) {
    int i;

    {
      ShaderUse(PROGRAM_TEXTURE);
      mat4x4 view, projection;

      CameraGetActiveViewMatrix(view);
      ShaderGetProjection(projection, PROJECTION_PERSPECTIVE_DEFAULT);

      ShaderSetView(PROGRAM_TEXTURE, view);
      ShaderSetProjection(PROGRAM_TEXTURE, projection);
      ShaderIdentityModel(PROGRAM_TEXTURE);
    }

    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    ShaderIdentityAllLazy(PROGRAM_DEFAULT);
    ShaderLoadPerspective(PROGRAM_DEFAULT, 45.0f, width / height, 0.0f, 1000.0f);

    mat4x4 view;
    CameraGetActiveViewMatrix(view);
    /* CameraGetViewMatrix(&camera, view); */
    ShaderSetView(PROGRAM_DEFAULT, view);

    ShaderUse(PROGRAM_DEFAULT);
    glBindVertexArray(vao);
    mat4x4 model;
    mat4x4_translate(model, 0.0f, 0.0f, -5.0f);
    ShaderSetModel(PROGRAM_DEFAULT, model);
    /* glDrawArrays(GL_TRIANGLES, 0, 3); */
    glBindVertexArray(0);

    mat4x4 projection;

    /* get view and projection matrix */
    CameraGetActiveViewMatrix(view);
    ShaderGetProjection(projection, PROJECTION_PERSPECTIVE_DEFAULT);

    /* set view and projection matrix */
    ShaderSetView(PROGRAM_DEFAULT, view);
    ShaderSetProjection(PROGRAM_DEFAULT, projection);

    for (i=0; i<numMainLoops; i+=1) {
      loops[i]();
    }

    if (WindowKeyIsDown(KEY_W)) {
      CameraMove(&camera, CAMERA_FORWARD, deltaTime);
    }
    if (WindowKeyIsDown(KEY_S)) {
      CameraMove(&camera, CAMERA_BACKWARD, deltaTime);
    }
    if (WindowKeyIsDown(KEY_A)) {
      CameraMove(&camera, CAMERA_LEFT, deltaTime);
    }
    if (WindowKeyIsDown(KEY_D)) {
      CameraMove(&camera, CAMERA_RIGHT, deltaTime);
    }
    if (WindowKeyIsDown(KEY_LEFT_SHIFT)) {
      CameraMove(&camera, CAMERA_DOWN, deltaTime);
    }
    if (WindowKeyIsDown(KEY_SPACE)) {
      CameraMove(&camera, CAMERA_UP, deltaTime);
    }
    if (WindowKeyIsDown(KEY_Q)) {
      CameraRotate(&camera, CAMERA_LEFT, deltaTime);
    }
    if (WindowKeyIsDown(KEY_E)) {
      CameraRotate(&camera, CAMERA_RIGHT, deltaTime);
    }
  }

  WindowEnd();

  return 0;
}
