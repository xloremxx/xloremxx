#ifndef MY_APP_H
#define MY_APP_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <linmath.h>

#define X 0
#define Y 1
#define Z 2
#define W 3

#define CAMERA_FORWARD 0
#define CAMERA_BACKWARD 1
#define CAMERA_LEFT 2
#define CAMERA_RIGHT 3
#define CAMERA_UP 4
#define CAMERA_DOWN 5

#define KEY_W GLFW_KEY_W
#define KEY_A GLFW_KEY_A
#define KEY_S GLFW_KEY_S
#define KEY_D GLFW_KEY_D
#define KEY_F1 GLFW_KEY_F1
#define KEY_F2 GLFW_KEY_F2
#define KEY_LEFT_SHIFT GLFW_KEY_LEFT_SHIFT
#define KEY_SPACE GLFW_KEY_SPACE
#define KEY_Q GLFW_KEY_Q
#define KEY_E GLFW_KEY_E
#define PRESS GLFW_PRESS

#define KEY_UP GLFW_KEY_UP
#define KEY_DOWN GLFW_KEY_DOWN
#define KEY_LEFT GLFW_KEY_LEFT
#define KEY_RIGHT GLFW_KEY_RIGHT

#define PROJECTION_PERSPECTIVE_DEFAULT 0

/* window constants */
#define KEY_ESCAPE GLFW_KEY_ESCAPE

/* program constants */
#define PROGRAM_DEFAULT 0
#define PROGRAM_TEXTURE 1

struct Object {
  float rotation;
  float gravity[3];
  float position[3];
  float velocity[3];
  float direction[3];
};

struct Mouse {
  float position[2];
};

/* main prototypes */
void MainAddLoop(void (*loop)());
struct Camera *MainReturnCamera();

/* example prototypes */
void LoadExampleGrid();
void LoadExampleRay();
void LoadExampleRandomLines();
void LoadExampleLineIntersection();
void LoadExampleCircle();
void LoadExampleDemo1();
void LoadExampleDemo2();
void LoadExampleTexture();
void LoadExamplePacman();
void LoadExampleGhost();
void LoadExampleMario();
void LoadExampleAABB();
void LoadExampleVectors();

struct Object *ExampleGhostGetPointer();

/* window function prototypes */
int WindowStart(float width, float height);
void WindowEnd();
int WindowOpen();
GLFWwindow* WindowReturn();
void WindowAddMouseMove(void (*mousemove)(float x, float y));
int WindowKeyIsDown(int key);
void WindowAddKeyboard(void (*keyboard)(int key, int scancode, int action, int mods));
struct Mouse* WindowReturnMousePointer();
void WindowGetDimensions(float *w, float *h);

/* common function prototypes */
char *CommonLoadSource(const char *path);
void CommonPrintMatrix(mat4x4 M);
int CommonInitialize();
void CommonDrawLine2D(float x1, float y1, float x2, float y2, float color[4]);
float CommonRandomF(float min, float max);
int CommonRandomI(int min, int max);
void CommonPrintVector3(vec3 V);

/* shader function prototypes */
int ShaderStart();
void ShaderUse(int index);
void ShaderIdentityAllLazy(int index);
void ShaderLoadPerspective(int index, float fov, float aspect, float near, float far);
void ShaderSetModel(int index, mat4x4 model);
void ShaderSetView(int index, mat4x4 view);
void ShaderSetProjection(int index, mat4x4 projection);
void ShaderColor4f(int index, float r, float g, float b, float a);
void ShaderGetProjection(mat4x4 out, int index);
void ShaderIdentityModel(int index);

/* vec function prototypes */
void vec3_set(vec3 v, float x, float y, float z);
void vec4_set(vec3 v, float x, float y, float z, float w);
void vec2_set(vec3 v, float x, float y);
void vec3_set_vec3(vec3 a, vec3 b);

/* camera prototypes */
struct Camera {
  float position[3];
  float front[3];
  float right[3];
  float up[3];
  float yaw;
  float pitch;
  float speed;
  float sensitivity[2];
  float worldUp[3];
};

void CameraProcessMouseMovement(struct Camera *camera, float offsetX, float offsetY);
void CameraCreate(struct Camera *camera, float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);
void CameraGetViewMatrix(struct Camera *camera, mat4x4 view);
void CameraMove(struct Camera *camera, int direction, float deltaTime);
void CameraProcessMouseMovement(struct Camera *camera, float offsetX, float offsetY);
void CameraRotate(struct Camera *camera, int direction, float deltaTime);
void CameraGetActiveViewMatrix(mat4x4 view);
void CameraSetActiveCamera(struct Camera *camera);

/* draw prototypes */
void DrawInitialize();
void DrawCircleA1(float x, float y, float z, float radius);
void DrawCircleA2(float position[3], float radius);
void DrawLineA2(vec3 A, vec3 B);
void DrawLineA1(float Ax, float Ay, float Az, float Bx, float By, float Bz);
void DrawCircleB1(float x, float y, float z, float radius);
void DrawCircleB2(float position[3], float radius);
void DrawQuad(float p1[3], float p2[3], float p3[3], float p4[3]);
void DrawPlane(float P[3], float n[3]);
void DrawPlane2P(float P1[3], float P2[3], float n[3]);
void DrawArrow2P(float P1[3], float P2[3]);
void DrawCube(float P[3], float size);
void DrawLine(float A[3], float B[3]);

#endif
