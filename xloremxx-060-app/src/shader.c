#include <stdio.h>
#include <glad/glad.h>
#include <linmath.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "app.h"

static float width, height;

static int programs[1];
static mat4x4 projections[1];

static float identity[] = {
  1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f,
};

static int CreateShader(const char *source, GLenum type) {
  int shader = glCreateShader(type);
  glShaderSource(shader, 1, (const char **)&source, NULL);
  glCompileShader(shader);

  int success = GL_FALSE;
  char info[512];

  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

  if (!success) {
    glGetShaderInfoLog(shader, 512, NULL, info);
    fprintf(stderr, "%s", info);
    return -1;
  }

  return shader;
}

static int CreateShaderProgram(const char *vertexPath, const char *fragmentPath) {

  char *vertexSource = CommonLoadSource(vertexPath);

  if (vertexSource == NULL) {
    fprintf(stderr, "ERROR: Unable to load vertex source '%s'.\n", vertexPath);
    return -1;
  }

  char *fragmentSource = CommonLoadSource(fragmentPath);

  if (fragmentSource == NULL) {
    fprintf(stderr, "ERROR: Unable to load fragment source '%s'.\n", fragmentPath);
    return -1;
  }

  int vertexShader = CreateShader(vertexSource, GL_VERTEX_SHADER);

  if (vertexShader == -1) {
    fprintf(stderr, "ERROR: 'Unable to compile vertex shader.'\n");
    return -1;
  }

  int fragmentShader = CreateShader(fragmentSource, GL_FRAGMENT_SHADER);

  if (fragmentShader == -1) {
    fprintf(stderr, "ERROR: 'Unable to compile fragment shader.'\n");
    return -1;
  }

  int program = glCreateProgram();
  glAttachShader(program, vertexShader);
  glAttachShader(program, fragmentShader);
  glLinkProgram(program);

  int success = GL_FALSE;
  char info[512];

  glGetProgramiv(program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(program, 512, NULL, info);
    fprintf(stderr, "%s", info);
    return -1;
  }

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  free(vertexSource);
  free(fragmentSource);

  return program;
}

int ShaderStart() {

  programs[PROGRAM_DEFAULT] = CreateShaderProgram("shaders/default.vs", "shaders/default.fs");

  if (programs[PROGRAM_DEFAULT] == -1) {
    fprintf(stderr, "ERROR: Unable to create shader program 'PROGRAM_DEFAULT.'\n");
    return -1;
  }

  programs[PROGRAM_TEXTURE] = CreateShaderProgram("shaders/texture.vs", "shaders/texture.fs");

  if (programs[PROGRAM_TEXTURE] == -1) {
    fprintf(stderr, "ERROR: Unable to create shader program 'PROGRAM_TEXTURE.'\n");
    return -1;
  }

  WindowGetDimensions(&width, &height);

  mat4x4_perspective(projections[PROJECTION_PERSPECTIVE_DEFAULT], 45.0f, width / height, 0.1f, 1000.0f);

  return 0;
}

void ShaderGetProjection(mat4x4 out, int index) {
  mat4x4_dup(out, projections[index]);
}

void ShaderUse(int index) {
  glUseProgram(programs[index]);
}

void ShaderIdentityAllLazy(int index) {
  float identity[] = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f,
  };
  float color[] = {1.0f, 1.0f, 1.0f, 1.0f};
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "model"), 1, GL_FALSE, identity);
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "view"), 1, GL_FALSE, identity);
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "projection"), 1, GL_FALSE, identity);
  glUniform4fv(glGetUniformLocation(programs[index], "color"), 1, color);
}

void ShaderLoadPerspective(int index, float fov, float aspect, float near, float far) {
  mat4x4 projection;
  mat4x4_perspective(projection, fov, aspect, near, far);
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "projection"), 1, GL_FALSE, (float *)projection);
}

void ShaderSetModel(int index, mat4x4 model) {
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "model"), 1, GL_FALSE, (float *)model);
}
void ShaderSetView(int index, mat4x4 view) {
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "view"), 1, GL_FALSE, (float *)view);
}
void ShaderSetProjection(int index, mat4x4 projection) {
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "projection"), 1, GL_FALSE, (float *)projection);
}

void ShaderColor4f(int index, float r, float g, float b, float a) {
  glUniform4f(glGetUniformLocation(programs[index], "color"), r, g, b, a);
}

void ShaderIdentityModel(int index) {
  glUniformMatrix4fv(glGetUniformLocation(programs[index], "model"), 1, GL_FALSE, identity);
}
