#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include "app.h"

static GLFWwindow* window;
static int keys[1024];

static float width, height;

static int inputMode = GLFW_CURSOR_NORMAL;
static struct Mouse mouse;

static int numMouseMoveCallbacks = 0;
static void (**mouseMoveCallbacks)(float x, float y);
void WindowAddMouseMove(void (*mousemove)(float x, float y)) {
  mouseMoveCallbacks = realloc(mouseMoveCallbacks, sizeof(void (*)()) * (numMouseMoveCallbacks+1));
  mouseMoveCallbacks[numMouseMoveCallbacks] = mousemove;
  numMouseMoveCallbacks += 1;
}
static int numKeyCallbacks = 0;
static void (**keyCallbacks)(int key, int scancode, int action, int mods);
void WindowAddKeyboard(void (*keyboard)(int key, int scancode, int action, int mods)) {
  keyCallbacks = realloc(keyCallbacks, sizeof(void (*)()) * (numKeyCallbacks + 1));
  keyCallbacks[numKeyCallbacks] = keyboard;
  numKeyCallbacks += 1;
}

static void keyboard(GLFWwindow* window, int key, int scancode, int action, int mods) {
  int i;

  if (key == KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);

  if (key == KEY_F1 && action == GLFW_PRESS) {
    if (inputMode == GLFW_CURSOR_NORMAL) {
      inputMode = GLFW_CURSOR_DISABLED;
    } else {
      inputMode = GLFW_CURSOR_NORMAL;
    }
    glfwSetInputMode(window, GLFW_CURSOR, inputMode);
  }

  if (action == GLFW_PRESS) {
    keys[key] = 1;
  }
  if (action == GLFW_RELEASE) {
    keys[key] = 0;
  }

  for (i=0; i<numKeyCallbacks; i+=1) {
    keyCallbacks[i](key, scancode, action, mods);
  }
}

static void mousemove(GLFWwindow* window, double xpos, double ypos) {
  int i;

  mouse.position[X] = (float)xpos;
  mouse.position[Y] = (float)ypos;

  for (i=0; i<numMouseMoveCallbacks; i+=1) {
    mouseMoveCallbacks[i]((float)xpos, (float)ypos);
  }
}

int WindowStart(float w, float h) {
  if (!glfwInit())
    return -1;

  width = w;
  height = h;

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow((int)width, (int)height, "create-window", NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    return -1;
  }

  /* make cursor image */
  unsigned char pixels[8 * 8 * 4];
  memset(pixels, 0xff, sizeof(pixels));

  GLFWimage image;
  image.width = 8;
  image.height = 8;
  image.pixels = pixels;

  GLFWcursor* cursor = glfwCreateCursor(&image, 0, 0);
  glfwSetCursor(window, cursor);

  /* Make the window's context current */
  glfwMakeContextCurrent(window);
  glfwSetKeyCallback(window, keyboard);
  glfwSetCursorPosCallback(window, mousemove);
  /* glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); */
  /* glfwSetMouseButtonCallback(window, mouse_button_callback); */

  /* load all opengl function pointers */
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    fprintf(stderr, "Failed to initialize GLAD");
    return -1;
  }
}

void WindowEnd() {
  glfwTerminate();
}

int WindowOpen() {
  glfwPollEvents();
  glfwSwapBuffers(window);
  return !glfwWindowShouldClose(window);
}

GLFWwindow* WindowReturn() {
  return window;
}

int WindowKeyIsDown(int key) {
  return keys[key];
}

struct Mouse* WindowReturnMousePointer() {
  return &mouse;
}

void WindowGetDimensions(float *w, float *h) {
  *w = width;
  *h = height;
}
