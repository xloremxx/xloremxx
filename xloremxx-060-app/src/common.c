#include <linmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <app.h>

static struct {
  unsigned int line;
} vao;

static struct {
  unsigned int line;
} vbo;

char *CommonLoadSource(const char *path) 
{
  char *source = NULL;

  /* open file */
  FILE *fp;

  fp = fopen(path, "r");

  if (fp == NULL) {
    char str[512] = { '\0' };
    sprintf(str, "/usr/share/xloremxx/xloremxx-060-app/%s", path);
    fp = fopen(str, "r");
    if (fp == NULL) {
      fprintf(stderr, "ERROR: Failed to load source '%s'.\n", str);
      return NULL;
    }
  }

  /* go to the end of the file */
  if (fseek(fp, 0L, SEEK_END) != 0) {
    return NULL;
  }

  /* get the file size */
  long bufsize = ftell(fp);

  if (bufsize == -1) {
    return NULL;
  }

  /* allocate buffer to file size */
  source = malloc(sizeof(char) * (bufsize + 1));

  /* go back to start of the file */
  if (fseek(fp, 0L, SEEK_SET) != 0) {
    return NULL;
  }

  /* read the entire file into memory */
  size_t new_len = fread(source, sizeof(char), bufsize, fp);

  if (ferror(fp) != 0) {
    return NULL;
  }

  /* to be safe */
  source[new_len++] = '\0';

  /* close the file */
  fclose(fp);

  return source;
}


void CommonPrintMatrix(mat4x4 M) {
  int i, j;
  for (i=0; i<4; i++) {
    for (j=0; j<4; j++) {
      printf("%.2f, ", M[i][j]);
    }
  }
  printf("\n");
}
void CommonDrawLine2D(float x1, float y1, float x2, float y2, float color[4]) {
  glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
  float vertices[2][3] = {
    x1, y1, 0.0f,
    x2, y2, 0.0f,
  };
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(vao.line);

  mat4x4 model;
  mat4x4_identity(model);

  ShaderSetModel(PROGRAM_DEFAULT, model);
  ShaderColor4f(PROGRAM_DEFAULT, color[0], color[1], color[2], color[3]);
  glDrawArrays(GL_LINES, 0, 2);

  glBindVertexArray(0);
}

static unsigned int CreateVao(float *vertices, int vertices_size, float *indices, int indices_size) {

  unsigned int vao;
  glGenVertexArrays(1, &vao);

  glBindVertexArray(vao);

  if (vertices != NULL) {
    unsigned int vbo;
    glGenBuffers(1, &vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices_size, vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
  if (indices == NULL) {
    printf("NO INDICES\n");
  }

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0);

  return vao;
}

int CommonInitialize() {

  /* create line vao */
  {
    float vertices[2][3] = {
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f,
    };

    glGenVertexArrays(1, &vao.line);
    glGenBuffers(1, &vbo.line);

    glBindVertexArray(vao.line);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  return 0;
}

float CommonRandomF(float min, float max) {
  float scale = rand() / (float) RAND_MAX;
  return min + scale * ( max - min );
}
int CommonRandomI(int min, int max) {
  int scale = rand() / RAND_MAX;
  return min + scale * ( max - min );
}

void CommonPrintVector3(vec3 V) {
  printf("VECTOR: [%.2f, %.2f, %.2f].\n", V[X], V[Y], V[Z]);
}
