#include <linmath.h>
#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "app.h"

static struct Camera *activeCamera;

static void CameraUpdateVectors(struct Camera *camera) {
  /* calculate new front vectors */
  vec3 front;
  front[X] = cosf(camera->yaw * M_PI / 180.0f) * cosf(camera->pitch * M_PI / 180.0f);
  front[Y] = sinf(camera->pitch * M_PI / 180.0f);
  front[Z] = sinf(camera->yaw * M_PI / 180.0f) * cosf(camera->pitch * M_PI / 180.0f);
  vec3_norm(camera->front, front);

  /* re-calculate right and up vectors */
  vec3 right, up;

  vec3_mul_cross(right, camera->front, camera->worldUp);
  vec3_norm(camera->right, right);

  vec3_mul_cross(up, camera->right, camera->front);
  vec3_norm(camera->up, up);

}

void CameraCreate(struct Camera *camera, float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) {
  camera->position[X] = posX;
  camera->position[Y] = posY;
  camera->position[Z] = posZ;

  camera->up[X] = upX;
  camera->up[Y] = upY;
  camera->up[Z] = upZ;

  camera->worldUp[X] = upX;
  camera->worldUp[Y] = upY;
  camera->worldUp[Z] = upZ;

  camera->yaw = yaw;
  camera->pitch = pitch;

  camera->sensitivity[X] = 0.1f;
  camera->sensitivity[Y] = 0.1f;

  camera->speed = 8.5f;

  vec3_set(camera->front, 0.0f, 0.0f, -1.0f);
  CameraUpdateVectors(camera);
}

void CameraGetViewMatrix(struct Camera *camera, mat4x4 view) {
  vec3 center;
  vec3_add(center, camera->position, camera->front);
  mat4x4_look_at(view, camera->position, center, camera->up);
  /* mat4x4_look_at(view, eye, center, up); */
}

void CameraSetActiveCamera(struct Camera *camera) {
  activeCamera = camera;
}

void CameraGetActiveViewMatrix(mat4x4 view) {
  vec3 center;
  vec3_add(center, activeCamera->position, activeCamera->front);
  mat4x4_look_at(view, activeCamera->position, center, activeCamera->up);
}

void CameraMove(struct Camera *camera, int direction, float deltaTime) {
  float velocity = camera->speed * deltaTime;

  if (direction == CAMERA_FORWARD) {
    vec3 step;
    vec3_scale(step, camera->front, velocity);
    vec3_add(camera->position, camera->position, step);
  }
  if (direction == CAMERA_BACKWARD) {
    vec3 step;
    vec3_scale(step, camera->front, velocity);
    vec3_sub(camera->position, camera->position, step);
  }
  if (direction == CAMERA_LEFT) {
    vec3 step;
    vec3_scale(step, camera->right, velocity);
    vec3_sub(camera->position, camera->position, step);
  }
  if (direction == CAMERA_RIGHT) {
    vec3 step;
    vec3_scale(step, camera->right, velocity);
    vec3_add(camera->position, camera->position, step);
  }
  if (direction == CAMERA_UP) {
    camera->position[Y] += velocity;
  }
  if (direction == CAMERA_DOWN) {
    camera->position[Y] -= velocity;
  }
}

void CameraRotate(struct Camera *camera, int direction, float deltaTime) {
  float velocity = deltaTime * 100.0f;
  if (direction == CAMERA_LEFT) {
    camera->yaw -= velocity;
  }
  if (direction == CAMERA_RIGHT) {
    camera->yaw += velocity;
  }
  CameraUpdateVectors(camera);
}

void CameraProcessMouseMovement(struct Camera *camera, float offsetX, float offsetY) {
  offsetX *= camera->sensitivity[X];
  offsetY *= camera->sensitivity[Y];

  camera->yaw += offsetX;
  camera->pitch += offsetY;

  /* constrain pitch */
  if (camera->pitch > 89.0f) {
    camera->pitch = 89.0f;
  }
  if (camera->pitch < -89.0f) {
    camera->pitch = -89.0f;
  }

  CameraUpdateVectors(camera);
}
