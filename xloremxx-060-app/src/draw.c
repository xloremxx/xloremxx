#include <stdio.h>
#include "app.h"

#define CIRCLE_SEGMENTS 70

static struct {
  unsigned int line;
  unsigned int circle;
  unsigned int quad;
  unsigned int arrow;
  unsigned int cube;
} vao;

static struct {
  unsigned int line;
  unsigned int circle;
  unsigned int quad;
  unsigned int arrow;
  unsigned int cube;
} vbo;

static struct {
  unsigned int quad;
  unsigned int arrow;
  unsigned int cube;
} ebo;

void DrawInitialize() {
  /* make arrow vao, vbo, ebo */
  {
    float P1[3] = { 0.0f, 0.0f, 0.0f };
    float P2[3] = { 0.0f, 5.0f, 0.0f };

    float D = 0.2f;

    float vertices[13][3] = {
      /* P1[X], P1[Y], P1[Z], */
      /* P2[X], P2[Y], P2[Z], */

      /* bottom vertices */

      P1[X] - D, P1[Y], P1[Z] - D,
      P1[X] + D, P1[Y], P1[Z] - D,
      P1[X] + D, P1[Y], P1[Z] + D,
      P1[X] - D, P1[Y], P1[Z] + D,

      P2[X] - D, P2[Y] - D*2, P2[Z] - D,
      P2[X] + D, P2[Y] - D*2, P2[Z] - D,
      P2[X] + D, P2[Y] - D*2, P2[Z] + D,
      P2[X] - D, P2[Y] - D*2, P2[Z] + D,

      P2[X] - D*2, P2[Y] - D*2, P2[Z] - D*2,
      P2[X] + D*2, P2[Y] - D*2, P2[Z] - D*2,
      P2[X] + D*2, P2[Y] - D*2, P2[Z] + D*2,
      P2[X] - D*2, P2[Y] - D*2, P2[Z] + D*2,

      P2[X], P2[Y], P2[Z],

      /* -0.5f, -0.5f, 0.0f, */
      /* +0.5f, -0.5f, 0.0f, */
      /* +0.5f, +0.5f, 0.0f, */
      /* -0.5f, +0.5f, 0.0f, */
    };
    unsigned int indices[22][3] = {
      0, 1, 2,
      2, 3, 0,
      0, 4, 5,
      5, 1, 0,
      1, 6, 5,
      6, 1, 2,
      3, 2, 6,
      3, 6, 7,
      0, 3, 4,
      3, 7, 4,
      6, 10, 9,
      6, 9, 5,
      8, 9, 5,
      5, 8, 4,
      8, 4, 7,
      7, 11, 8,
      6, 7, 10,
      10, 7, 11,
      10, 9, 12,
      9, 8, 12,
      8, 11, 12,
      11, 10, 12
    };
    glGenVertexArrays(1, &vao.arrow);
    glGenBuffers(1, &vbo.arrow);
    glGenBuffers(1, &ebo.arrow);
    glBindVertexArray(vao.arrow);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.arrow);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo.arrow);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  /* make quad vao, vbo */
  {
    float vertices[4][3] = {0};
    unsigned int indices[6] = {
      0, 1, 2,
      2, 3, 0,
    };
    glGenVertexArrays(1, &vao.quad);
    glGenBuffers(1, &vbo.quad);
    glGenBuffers(1, &ebo.quad);
    glBindVertexArray(vao.quad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo.quad);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  /* make circle vao, vbo */
  {
    float vertices[CIRCLE_SEGMENTS][3] = {0};
    glGenVertexArrays(1, &vao.circle);
    glGenBuffers(1, &vbo.circle);
    glBindVertexArray(vao.circle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.circle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  /* make line vao, vbo */
  {
    float vertices[2][3] = {0};
    glGenVertexArrays(1, &vao.line);
    glGenBuffers(1, &vbo.line);
    glBindVertexArray(vao.line);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  /* make cube vao, vbo, ebo */
  {
    float vertices[8][3] = {
      /* backside */
      -1.0f, -1.0f, -1.0f,
      +1.0f, -1.0f, -1.0f,
      +1.0f, +1.0f, -1.0f,
      -1.0f, +1.0f, -1.0f,

      /* frontside */
      -1.0f, -1.0f, +1.0f,
      +1.0f, -1.0f, +1.0f,
      +1.0f, +1.0f, +1.0f,
      -1.0f, +1.0f, +1.0f,
    };

    unsigned int indices[12][3] = {
      /* back side */
      0, 1, 2, 
      0, 2, 3,
      /* front side */
      4, 5, 6,
      4, 6, 7,
      /* left side */
      4, 0, 3,
      4, 3, 7,
      /* right side */
      5, 1, 2, 
      5, 2, 6,
      /* bottom side */
      4, 5, 1,
      4, 1, 0,
      /* top side */
      7, 6, 2, 
      7, 2, 3,
    };
    glGenVertexArrays(1, &vao.cube);
    glGenBuffers(1, &vbo.cube);
    glGenBuffers(1, &ebo.cube);
    glBindVertexArray(vao.cube);
    glBindBuffer(GL_ARRAY_BUFFER, vbo.cube);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo.cube);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
}

void DrawCircleA1(float x, float y, float z, float radius) {
  float vertices[CIRCLE_SEGMENTS][3];

  int i;
  float a;

  for (i=0; i<CIRCLE_SEGMENTS; i+=1) {
    float a = 2 * M_PI * i / CIRCLE_SEGMENTS;

    vertices[i][X] = x + cosf(a) * radius;
    vertices[i][Y] = y + sinf(a) * radius;
    vertices[i][Z] = z + 0.0f;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vbo.circle);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderIdentityModel(PROGRAM_DEFAULT);

  glBindVertexArray(vao.circle);
  /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f); */
  glDrawArrays(GL_TRIANGLE_FAN, 0, CIRCLE_SEGMENTS);
  glBindVertexArray(0);
}

void DrawCircleA2(float position[3], float radius) {
  DrawCircleA1(position[X], position[Y], position[Z], radius);
}

void DrawCircleB1(float x, float y, float z, float radius) {
  float vertices[CIRCLE_SEGMENTS][3];

  int i;
  float a;

  for (i=0; i<CIRCLE_SEGMENTS; i+=1) {
    float a = 2 * M_PI * i / CIRCLE_SEGMENTS;

    vertices[i][X] = x + cosf(a) * radius;
    vertices[i][Y] = y + sinf(a) * radius;
    vertices[i][Z] = z + 0.0f;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vbo.circle);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderIdentityModel(PROGRAM_DEFAULT);

  glBindVertexArray(vao.circle);
  /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f); */
  glDrawArrays(GL_LINE_LOOP, 0, CIRCLE_SEGMENTS);
  glBindVertexArray(0);
}

void DrawCircleB2(float position[3], float radius) {
  DrawCircleB1(position[X], position[Y], position[Z], radius);
}

void DrawLineA1(float Ax, float Ay, float Az, float Bx, float By, float Bz) {
  mat4x4 model;
  mat4x4_identity(model);

  float vertices[2][3];

  vertices[0][X] = Ax;
  vertices[0][Y] = Ay;
  vertices[0][Z] = Az;

  vertices[1][X] = Bx;
  vertices[1][Y] = By;
  vertices[1][Z] = Bz;

  glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderIdentityModel(PROGRAM_DEFAULT);

  glBindVertexArray(vao.line);
  /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f); */
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 2);
  glBindVertexArray(0);
}

void DrawLineA2(vec3 A, vec3 B) {
  float vertices[2][3];

  vertices[0][X] = A[X];
  vertices[0][Y] = A[Y];
  vertices[0][Z] = A[Z];

  vertices[1][X] = B[X];
  vertices[1][Y] = B[Y];
  vertices[1][Z] = B[Z];

  glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  ShaderIdentityModel(PROGRAM_DEFAULT);

  glBindVertexArray(vao.line);
  /* ShaderColor4f(PROGRAM_DEFAULT, 1.0f, 1.0f, 1.0f, 1.0f); */
  glDrawArrays(GL_LINES, 0, 2);
  glBindVertexArray(0);
}

void DrawPlane(float P[3], float n[3]) {
  float N[4][3];

  N[0][X] = 0.0f;
  N[0][Y] = 0.0f;
  N[0][Z] = 0.0f;

  N[1][X] = 0.0f;
  N[1][Y] = 0.0f;
  N[1][Z] = 0.0f;

  N[2][X] = 0.0f;
  N[2][Y] = 0.0f;
  N[2][Z] = 0.0f;

  N[3][X] = 0.0f;
  N[3][Y] = 0.0f;
  N[3][Z] = 0.0f;


  DrawQuad(N[0], N[1], N[2], N[3]);
}

void DrawQuad(float p1[3], float p2[3], float p3[3], float p4[3]) {
  mat4x4 model;
  mat4x4_identity(model);

  float vertices[4][3] = {
    p1[X], p1[Y], p1[Z],
    p2[X], p2[Y], p2[Z],
    p3[X], p3[Y], p3[Z],
    p4[X], p4[Y], p4[Z],
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo.quad);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(vao.quad);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

}

void DrawPlane2P(float P1[3], float P2[3], float n[3]) {
  float D = 5.0f;

  float t[3];

  vec3_sub(t, P2, P1);
  vec3_norm(t, t);

  float b[3];
  vec3_mul_cross(b, t, n);

  float P[4][3];

  P[0][X] = P1[X] - t[X]*D - b[X]*D;
  P[0][Y] = P1[Y] - t[Y]*D - b[Y]*D;
  P[0][Z] = P1[Z] - t[Z]*D - b[Z]*D;

  P[1][X] = P1[X] + t[X]*D - b[X]*D;
  P[1][Y] = P1[Y] + t[Y]*D - b[Y]*D;
  P[1][Z] = P1[Z] + t[Z]*D - b[Z]*D;

  P[2][X] = P1[X] + t[X]*D + b[X]*D;
  P[2][Y] = P1[Y] + t[Y]*D + b[Y]*D;
  P[2][Z] = P1[Z] + t[Z]*D + b[Z]*D;

  P[3][X] = P1[X] - t[X]*D + b[X]*D;
  P[3][Y] = P1[Y] - t[Y]*D + b[Y]*D;
  P[3][Z] = P1[Z] - t[Z]*D + b[Z]*D;

  DrawQuad(P[0], P[1], P[2], P[3]);
}

void DrawArrow2P(float P1[3], float P2[3]) {

  float D = 0.2f;
  float vertices[13][3] = {
    /* P1[X], P1[Y], P1[Z], */
    /* P2[X], P2[Y], P2[Z], */

    /* bottom vertices */

    P1[X] - D, P1[Y] - D, P1[Z] - D,
    P1[X] + D, P1[Y] - D, P1[Z] - D,
    P1[X] + D, P1[Y] - D, P1[Z] + D,
    P1[X] - D, P1[Y] - D, P1[Z] + D,

    P2[X] - D, P2[Y] - D, P2[Z] - D,
    P2[X] + D, P2[Y] - D, P2[Z] - D,
    P2[X] + D, P2[Y] - D, P2[Z] + D,
    P2[X] - D, P2[Y] - D, P2[Z] + D,

    P2[X] - D*2, P2[Y] - D, P2[Z] - D*2,
    P2[X] + D*2, P2[Y] - D, P2[Z] - D*2,
    P2[X] + D*2, P2[Y] - D, P2[Z] + D*2,
    P2[X] - D*2, P2[Y] - D, P2[Z] + D*2,

    P2[X], P2[Y], P2[Z],

    /* -0.5f, -0.5f, 0.0f, */
    /* +0.5f, -0.5f, 0.0f, */
    /* +0.5f, +0.5f, 0.0f, */
    /* -0.5f, +0.5f, 0.0f, */
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo.arrow);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(vao.arrow);
  glPointSize(20.0f);
  glDrawElements(GL_TRIANGLES, 22*3, GL_UNSIGNED_INT, 0);
  /* glDrawArrays(GL_POINTS, 0, 13); */
  glBindVertexArray(0);
}

void DrawCube(float P[3], float size) {
  mat4x4 model;
  mat4x4_translate(model, P[X], P[Y], P[Z]);
  mat4x4_scale_aniso(model, model, size, size, size);
  ShaderSetModel(PROGRAM_DEFAULT, model);
  glBindVertexArray(vao.cube);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
}

void DrawLine(float A[3], float B[3]) {
  mat4x4 model;
  mat4x4_identity(model);

  float vertices[2][3] = {
    A[X], A[Y], A[Z],
    B[X], B[Y], B[Z],
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo.line);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(vao.line);

  ShaderSetModel(PROGRAM_DEFAULT, model);
  glDrawArrays(GL_LINES, 0, 2);

  glBindVertexArray(0);
}

