#include <stdio.h>
#include <linmath.h/linmath.h>

void CommonPrintMatrix(mat4x4 M) {
  int i, j;

  for (i=0; i<4; i+=1) {
    for (j=0; j<4; j+=1) {
      printf("%.2f, ", M[i][j]);
    }
  }

  printf("\n");
}

void common_vec4_divide(vec4 vec, float n) {
  vec[0] /= n;
  vec[1] /= n;
  vec[2] /= n;
  vec[3] /= n;
}

void common_vec3_multi(vec3 V, float x) {
  V[0] *= x;
  V[1] *= x;
  V[2] *= x;
}

void common_set_vec3(vec3 V, float x, float y, float z) {
  V[0] = x;
  V[1] = y;
  V[2] = z;
}

float common_dot_product(vec3 v1, vec3 v2) {
  return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}
