#include <glad/glad.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <linmath.h>
#include <main.h>
#include <string.h>

static GLFWwindow* window;

static unsigned int width, height;

static int mouse_button_callbacks_count = 0;
static void (**mouse_button_callbacks)(int button, int action, int mods);

void window_register_mouse_button_callback(void (*mouse_button_callback)(int button, int action, int mods)) {
  mouse_button_callbacks = realloc(mouse_button_callbacks, sizeof(void*) * mouse_button_callbacks_count+1);
  mouse_button_callbacks[mouse_button_callbacks_count] = mouse_button_callback;
  mouse_button_callbacks_count += 1;
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
  int i;

  for (i=0; i<mouse_button_callbacks_count; i++) {
    mouse_button_callbacks[i](button, action, mods);
  }
}

GLFWwindow* window_start(unsigned int input_width, unsigned int input_height) {
  width = input_width;
  height = input_height;

  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow(width, height, "create-window", NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    return NULL;
  }

  /* make cursor image */
  unsigned char pixels[8 * 8 * 4];
  memset(pixels, 0xff, sizeof(pixels));

  GLFWimage image;
  image.width = 8;
  image.height = 8;
  image.pixels = pixels;

  GLFWcursor* cursor = glfwCreateCursor(&image, 0, 0);
  glfwSetCursor(window, cursor);

  /* Make the window's context current */
  glfwMakeContextCurrent(window);
  /* glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); */
  glfwSetMouseButtonCallback(window, mouse_button_callback);

  return window;
}

void window_get_size_i(int *width_ptr, int *height_ptr) {
  *width_ptr = width;
  *height_ptr = height;
}
void window_get_size_f(float *width_ptr, float *height_ptr) {
  *width_ptr = (float)width;
  *height_ptr = (float)height;
}
void window_set_cursor_disabled() {
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}
void window_set_cursor_normal() {
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

GLFWwindow *window_return_pointer() {
  return window;
}

void window_get_mouse_position_f(float *x, float *y) {
  double xpos, ypos;

  glfwGetCursorPos(window, &xpos, &ypos);

  *x = (float) xpos;
  *y = (float) ypos;
}

void window_get_size(float *w, float *h) {
  *w = (float)width;
  *h = (float)height;
}

void WindowLoadMousePosition(float position[3]) {
  double xpos, ypos;
  glfwGetCursorPos(window, &xpos, &ypos);

  position[0] = (float)xpos;
  position[1] = (float)ypos;
}

void WindowLoadSize(float *width, float *height) {
  int win_width;
  int win_height;
  glfwGetWindowSize(window, &win_width, &win_height);
  *width = win_width;
  *height = win_height;
}
