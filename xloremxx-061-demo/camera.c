#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <linmath.h/linmath.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <string.h>
#include <GLFW/glfw3.h>
#include <main.h>

static int first_mouse = 0;

static GLFWwindow* window;
static int program;

static mat4x4 view;
static mat4x4 perspective;

static int constrain_pitch = 1;

static float direction[3];

static float speed[3] = {0.005f, 0.005f, 0.005f};

static float front[3] = {0.0f, 0.0f, 1.0f};;
static float position[3] = {0, 0, 15};
static float up[3] = {0, 1, 0};
static float world_up[3] = {0, 1, 0};

static float right[3] = {0.0f, 0.0f, 0.0f};

static float yaw = -95.0f;
static float pitch = 0.0f;

static int keys[1024];

static struct {
  int locked;
  float last[3];
  float position[3];
} mouse = {
  0,
  0, 0, 0,
  0, 0, 0,
};

static void update_vectors() {
  float _front[3];

  up[0] = 0.0f;
  up[1] = 1.0f;
  up[2] = 0.0f;

  _front[0] = cosf( yaw * M_PI / 180 ) * cosf( pitch * M_PI / 180 );
  _front[1] = sinf( pitch * M_PI / 180 );
  _front[2] = sinf( yaw * M_PI / 180 ) * cosf( pitch * M_PI / 180 );

  vec3_norm(front, _front);

  /* recalculate up and right vectors */

  /* right = normalize( cross( front, up ) ) */
  vec3_mul_cross(right, front, up);
  vec3_norm(right, right);

  /* up = normalize( cross( right, front ) ) */
  vec3_mul_cross(up, right, front);
  vec3_norm(up, up);
}

static void mousemove_callback(float x, float y) {
  mouse.position[0] = x;
  mouse.position[1] = y;

  /* only do this when mouse is locked, [F1]] */
  if (!mouse.locked) {
    return;
  }

  if (first_mouse == 0) {

    mouse.last[0] = x;
    mouse.last[1] = y;

    first_mouse = 1;
  }

  float offset[3];

  offset[0] = mouse.position[0] - mouse.last[0];
  offset[1] = mouse.last[1] - mouse.position[1];

  mouse.last[0] = mouse.position[0];
  mouse.last[1] = mouse.position[1];

  float sensitivity[3] = { 0.1f, 0.1f };

  offset[0] *= sensitivity[0];
  offset[1] *= sensitivity[1];

  yaw += offset[0];
  pitch += offset[1];

  if (constrain_pitch) {
    if (pitch > 89.0f) {
      pitch = 89.0f;
    }
    if (pitch < -89.0f) {
      pitch = -89.0f;
    }
  }

  update_vectors();
}

static void keyboard_callback(int key, int scancode, int action, int mods) {

  if (key == GLFW_KEY_F1 && action == GLFW_PRESS) {
    if (mouse.locked == 0) {
      mouse.locked = 1;
    } else {
      mouse.locked = 0;
    }

    if (mouse.locked == 1) {
      window_set_cursor_disabled();
      first_mouse = 0;
    } else {
      window_set_cursor_normal();
    }
  }

  if (action == GLFW_PRESS)
    keys[key] = 1;
  else if (action == GLFW_RELEASE) {
    keys[key] = 0;
  }
}

void camera_start(GLFWwindow* window, int program_id) {
  program = program_id;

  main_register_callback_mousemove(mousemove_callback);
  main_register_callback_keyboard(keyboard_callback);

  glUseProgram(program);

  world_up[0] = up[0];
  world_up[1] = up[1];
  world_up[2] = up[2];

  update_vectors();
}

void camera_load_perspective() {

}

void camera_load_view_matrix(mat4x4 M) {
  vec3 center;
  vec3_add(center, position, front);

  mat4x4_look_at(M, position, center, world_up);
}

void camera_update() {
  if (keys[GLFW_KEY_W]) { /* move forward */

    /* position += front * velocity */
    /* or */
    /* position += front * speed */

    float step[3];

    step[0] = front[0] * speed[0];
    step[1] = front[1] * speed[1];
    step[2] = front[2] * speed[2];

    vec3_add(position, position, step);
  }

  if (keys[GLFW_KEY_S]) { /* move backwards */

    /* position -= front * velocity */

    float step[3];

    step[0] = front[0] * speed[0];
    step[1] = front[1] * speed[1];
    step[2] = front[2] * speed[2];

    vec3_sub(position, position, step);
  }

  if (keys[GLFW_KEY_A]) { /* move left */

    /* position -= right * velocity */

    float step[3];

    step[0] = right[0] * speed[0];
    step[1] = right[1] * speed[1];
    step[2] = right[2] * speed[2];
    vec3_sub(position, position, step);

  }

  if (keys[GLFW_KEY_D]) { /* move right */
    /* position += right * velocity */

    float step[3];

    step[0] = right[0] * speed[0];
    step[1] = right[1] * speed[1];
    step[2] = right[2] * speed[2];
    vec3_add(position, position, step);
  }

  if (keys[GLFW_KEY_SPACE]) { /* move up */
    position[1] += speed[1];
  }
  if (keys[GLFW_KEY_LEFT_SHIFT]) { /* move down */
    position[1] -= speed[1];
  }
}

void camera_update_matrix() {
  mat4x4 M;
  camera_load_view_matrix(M);
  program_set_matrix_view(M);
}
void camera_update_matrix_and_get(mat4x4 view_matrix) {
  mat4x4 M;
  camera_load_view_matrix(M);
  program_set_matrix_view(M);
  view_matrix = M;
}

void camera_calculate_view_matrix(mat4x4 M) {
  vec3 center;
  vec3_add(center, position, front);
  mat4x4_look_at(M, position, center, world_up);
}

void camera_get_position(float pos[3]) {
  pos[0] = position[0];
  pos[1] = position[1];
  pos[2] = position[2];
}

void camera_get_up(float v[3]) {
  v[0] = world_up[0];
  v[1] = world_up[1];
  v[2] = world_up[2];
}
