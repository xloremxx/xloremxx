#include <stdio.h>
#include <stdlib.h>

#include <main.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <linmath.h>

static mat4x4 identity_matrix;

static mat4x4 matrix_model;
static mat4x4 matrix_view;
static mat4x4 matrix_projection;

static mat4x4 matrix_perspective;
static mat4x4 matrix_ortho;

static int program;

static int width, height;

static struct {
  int model;
  int view;
  int projection;
  int color;
} uniform_locations;

static int get_uniform_location(const char *location) {
  int loc = glGetUniformLocation(program, location);

  if (loc == -1) {
    fprintf(stderr, "Error: unable to locate uniform '%s'.\n", location);
    return -1;
  }

  return loc;
}

int program_start() {
  mat4x4_identity(identity_matrix);

  /* create vertex shader */
  char *vertex_source = common_load_source("default.vs");
  if (vertex_source == NULL) {
    fprintf(stderr, "Failed to load vertex source!");
    return -1;
  }
  int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, (const char **)&vertex_source, NULL);
  glCompileShader(vertex_shader);

  int success = GL_FALSE;
  char info[512];

  glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertex_shader, 512, NULL, info);
    fprintf(stderr, "Error: vertex compilation error\n%s\n", info);
    return -1;
  }

  /* create fragment shader */
  char *fragment_source = common_load_source("default.fs");
  if (fragment_source == NULL) {
    fprintf(stderr, "Failed to load fragment source!");
    return -1;
  }
  int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, (const char **)&fragment_source, NULL);
  glCompileShader(fragment_shader);

  glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragment_shader, 512, NULL, info);
    fprintf(stderr, "Error: fragment compilation error\n%s\n", info);
    return -1;
  }

  /* create shader program */
  program = glCreateProgram();
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);
  glLinkProgram(program);

  glGetProgramiv(program, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(program, 512, NULL, info);
    fprintf(stderr, "Error: shader program linkiing error.\n%s\n", info);
    return -1;
  }

  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);

  free(vertex_source);
  free(fragment_source);

  glUseProgram(program);

  uniform_locations.model = get_uniform_location("model");
  uniform_locations.view = get_uniform_location("view");
  uniform_locations.projection = get_uniform_location("projection");
  uniform_locations.color = get_uniform_location("color");

  window_get_size_i(&width, &height);

  mat4x4_perspective(matrix_perspective, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
  mat4x4_ortho(matrix_ortho, 0.0f, (float)width, 0.0f, (float)height, 0.0f, 100.0f);

  return program;
}

void program_set_matrix_model(mat4x4 model) {
  glUniformMatrix4fv(uniform_locations.model, 1, GL_FALSE, (const float *)model);
}
void program_set_matrix_view(mat4x4 view) {
  glUniformMatrix4fv(uniform_locations.view, 1, GL_FALSE, (const float *)view);
}
void program_set_matrix_projection(mat4x4 projection) {
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)projection);
}

void program_load_identity() {
  glUniformMatrix4fv(uniform_locations.model, 1, GL_FALSE, (const float *)identity_matrix);
  glUniformMatrix4fv(uniform_locations.view, 1, GL_FALSE, (const float *)identity_matrix);
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)identity_matrix);
}

void program_load_identity_model() {
  glUniformMatrix4fv(uniform_locations.model, 1, GL_FALSE, (const float *)identity_matrix);
}

void program_load_identity_view() {
  glUniformMatrix4fv(uniform_locations.view, 1, GL_FALSE, (const float *)identity_matrix);
}

void program_load_identity_projection() {
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)identity_matrix);
}

void program_load_projection_perspective() {
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)matrix_perspective);
}

void program_update_and_get_matrices(mat4x4 view_matrix, mat4x4 projection_matrix) {
  projection_matrix = matrix_perspective;
  mat4x4_perspective(matrix_perspective, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
  camera_update_matrix_and_get(view_matrix);
}

void program_make_perspective_matrix(mat4x4 M) {
  mat4x4_perspective(M, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
}

void program_load_default() {
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)matrix_perspective);
  camera_update_matrix();
}

void program_load_ortho() {
  glUniformMatrix4fv(uniform_locations.projection, 1, GL_FALSE, (const float *)matrix_ortho);
}

void program_set_color(float r, float g, float b) {
  glUniform3f(uniform_locations.color, r, g, b);
}

void program_load_normalized_device_space() {
  program_load_identity();
}

float * ProgramGetMatrix(const unsigned int type) {
  if (type == PERSPECTIVE_MATRIX) {
    return (float *)matrix_perspective;
  }
}

void ProgramGetInvertedMatrix(mat4x4 M, const unsigned int type) {
  if (type == PERSPECTIVE_MATRIX) {
    mat4x4_invert(M, matrix_perspective);
  }
  if (type == CAMERA_MATRIX) {
    mat4x4 view_matrix;
    camera_load_view_matrix(view_matrix);
    mat4x4_invert(M, view_matrix);
  }
}
