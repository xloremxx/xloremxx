#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <linmath.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <main.h>
#include <string.h>

#include <GLFW/glfw3.h>

/* * * * * * * * * * * * * * * * * *
 * plane
 * cube
 * circle
 * grid
 *
 * rect
 *
 * mouse
 * camera
 * ground
 *
 * vector
 * matrix
 *
 * normalize
 *
 * translate
 * rotete
 * scale
 *
 * perspective
 * ortho
 * * * * * * * * * * * * * * * * * * */

/* add vector */
/* subtract vector */
/* scale vector */
/* multiply inner vector */
/* vector length */
/* vector normalize */
/* vector min */
/* vector max */
/* vector multiply cross */
/* vector reflect */
/* matrix identify */
/* matrix duplicate */
/* matrix row */
/* matrix column */
/* matrix transpose */
/* matrix add */
/* matrix scale */
/* matrix scale aniso */
/* matrix multiply */
/* matrix multiply by vector */
/* matrix translate */
/* matrix translate in place */
/* matrix from vec to outer */
/* matrix rotate */
/* matrix rotate X */
/* matrix rotate Y */
/* matrix rotate Z */
/* matrix invert */
/* matrix orthonormalize */
/* matrix frustum */
/* matrix ortho */
/* matrix perspective */
/* matrix look at */

#define PROJECT_COUNT 3
static GLFWwindow* window;

int DYNAMIC_PROJECT_COUNT = 0;
struct {
  int count;
  void (*initializers[PROJECT_COUNT])();
  void (*animaters[PROJECT_COUNT])();
  void (**dynamic_initializers)();
  void (**dynamic_animators)();
} project;

struct Grid {
  vec3 position;
  vec3 scale;
};

static struct World world;

struct Ground {
  vec3 position;
  vec3 color;
  vec3 scale;
};

struct Cube {
  float speed;
  vec3 scale;
  vec3 position;
};

struct Rect {
  float scale;
  float speed;
  vec3 pos;
  vec3 color;
};


static int program;

struct Cube cube;
struct Ground ground;
struct Grid grid;
struct Camera camera;
struct {
  unsigned int grid;
} vaos;
struct {
  unsigned int grid;
} vbos;
struct {
  int color;
  int model;
  int view;
  int projection;
} uniform;

unsigned int width = 1300, height = 735;

int firstMouse = 1;

int keys[1024];
struct Mouse mouse;

static int mousemove_callbacks_count = 0;
static void (**mousemove_callbacks)(float x, float y);

void main_register_mousemove(void (*mousemove)(float x, float y)) {
  mousemove_callbacks = realloc(mousemove_callbacks, sizeof(void*) * mousemove_callbacks_count+1);
  mousemove_callbacks[mousemove_callbacks_count] = mousemove;
  mousemove_callbacks_count += 1;
}

void main_register_callback_mousemove(void (*mousemove_callback)(float x, float y)) {
  mousemove_callbacks = realloc(mousemove_callbacks, sizeof(void*) * mousemove_callbacks_count+1);
  mousemove_callbacks[mousemove_callbacks_count] = mousemove_callback;
  mousemove_callbacks_count += 1;
}

static int keyboard_callbacks_count = 0;
static void (**keyboard_callbacks)(int key, int scancode, int action, int mods);

void main_register_keyboard(void (*keyboard)(int key, int scancode, int action, int mods)) {
  keyboard_callbacks = realloc(keyboard_callbacks, sizeof(void*) * keyboard_callbacks_count+1);
  keyboard_callbacks[keyboard_callbacks_count] = keyboard;
  keyboard_callbacks_count += 1;
}
void main_register_callback_keyboard(void (*keyboard_callback)(int key, int scancode, int action, int mods)) {
  keyboard_callbacks = realloc(keyboard_callbacks, sizeof(void*) * keyboard_callbacks_count+1);
  keyboard_callbacks[keyboard_callbacks_count] = keyboard_callback;
  keyboard_callbacks_count += 1;
}

struct Mouse *main_load_mouse() {
  struct Mouse *m = malloc(sizeof(struct Mouse));

  m = &mouse;

  return m;
}
void main_load_dimensions(float *w, float *h) {
  *w = (float)width;
  *h = (float)height;
}
void main_set_color_3f(float r, float g, float b) {
  glUniform3f(uniform.color, r, g, b);
}
void main_load_perspective() {
  mat4x4 T;
  /* load perspective projection */
  mat4x4_identity(T);
  mat4x4_perspective(T, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
  glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float*)T);
}
void main_load_ortho() {
  mat4x4 T;
  mat4x4_identity(T);
  mat4x4_ortho(T, 0.0f, (float)width, 0.0f, (float)height, 0.0f, 100.0f);
  glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float*)T);
}
void main_load_camera_view() {
  /* vec3 center; */
  /* vec3_add(center, camera.position, camera.front); */

  /* mat4x4 T; */
  /* mat4x4_identity(T); */
  /* mat4x4_look_at(T, camera.position, center, camera.up); */
  mat4x4 view;
  camera_load_view_matrix(view);

  glUniformMatrix4fv(uniform.view, 1, GL_FALSE, (const float*)view);
}
int main_program() {
  return program;
}
int *main_load_keys() {
  return keys;
  /* int *tmp = malloc(sizeof(int*)); */

  /* tmp = keys; */

  /* return tmp; */
}
void main_load_world(struct World *ptr) {
  *ptr = world;
}
void main_set_model(const mat4x4 M) {
  glUniformMatrix4fv(uniform.model, 1, GL_FALSE, (const float *)M);
}
void main_set_view(const mat4x4 M) {
  glUniformMatrix4fv(uniform.view, 1, GL_FALSE, (const float *)M);
}
void main_set_projection(const mat4x4 M) {
  glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float *)M);
}
void main_identity_all() {
  float matrix[16] = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f,
  };
  glUniformMatrix4fv(uniform.model, 1, GL_FALSE, (const float *)matrix);
  glUniformMatrix4fv(uniform.view, 1, GL_FALSE, (const float *)matrix);
  glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float *)matrix);
  glUniform3f(uniform.color, 1.0f, 1.0f, 1.0f);
}

void keyboard(GLFWwindow* window, int key, int scancode, int action, int mods) {
  int i;

  for (i=0; i<keyboard_callbacks_count; i++) {
    keyboard_callbacks[i](key, scancode, action, mods);
  }

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) 
    glfwSetWindowShouldClose(window, GLFW_TRUE);

  if (action == GLFW_PRESS)
    keys[key] = 1;
  else if (action == GLFW_RELEASE) {
    keys[key] = 0;
  }

}

void mousemove(GLFWwindow* window, double xpos, double ypos) {
  int i;

  for (i=0; i<mousemove_callbacks_count; i++) {
    mousemove_callbacks[i]((float)xpos, (float)ypos);
  }

  mouse.default_position[0] = (float)xpos;
  mouse.default_position[1] = (float)ypos;

  mouse.pos[0] = (float)xpos;
  mouse.pos[1] = -((float)ypos - (float)height);
  mouse.pos[2] = 0.0f;

  if (firstMouse) {
    /*   mouse.lastPos[0] = (float)xpos; */
    /*   mouse.lastPos[1] = (float)ypos; */
    firstMouse = 0;
  }

  /* float x = (float)xpos - mouse.lastPos[0]; */
  /* float y = mouse.lastPos[1] - (float)ypos; */

  /* mouse.lastPos[0] = (float)xpos; */
  /* mouse.lastPos[1] = (float)ypos; */

  /* x *= camera.sensitivity; */
  /* y *= camera.sensitivity; */

  /* camera.yaw += x; */
  /* camera.pitch += y; */

  /* if (camera.pitch > 89.0f * M_PI / 180) { */
  /*   camera.pitch = 89.0f * M_PI / 180; */
  /* } */
  /* if (camera.pitch < -89.0f * M_PI / 180) { */
  /*   camera.pitch = -89.0f * M_PI / 180; */
  /* } */

  /* camera.front[0] = cosf(camera.yaw) * cosf(camera.pitch); */
  /* camera.front[1] = sinf(camera.pitch); */
  /* camera.front[2] = sinf(camera.yaw) * cosf(camera.pitch); */
  /* vec3_norm(camera.front, camera.front); */
}

void mouse_button_callback(int button, int action, int mods) {
  if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {

  }

  if (action == GLFW_PRESS) {
    mouse.button[button] = 1;
  } else if (action == GLFW_RELEASE) {
    mouse.button[button] = 0;
  }
}

char *common_load_source(const char *path) 
{
  char *source = NULL;

  /* open file */
  FILE *fp;

  fp = fopen(path, "r");

  if (fp == NULL) {
    char str[512] = { '\0' };

    sprintf(str, "/usr/share/xloremxx/xloremxx-061-demo/%s", path);

    fp = fopen(str, "r");

    if (fp == NULL) {
      return NULL;
    }
  }

  /* go to the end of the file */
  if (fseek(fp, 0L, SEEK_END) != 0) {
    return NULL;
  }

  /* get the file size */
  long bufsize = ftell(fp);

  if (bufsize == -1) {
    return NULL;
  }

  /* allocate buffer to file size */
  source = malloc(sizeof(char) * (bufsize + 1));

  /* go back to start of the file */
  if (fseek(fp, 0L, SEEK_SET) != 0) {
    return NULL;
  }

  /* read the entire file into memory */
  size_t new_len = fread(source, sizeof(char), bufsize, fp);

  if (ferror(fp) != 0) {
    return NULL;
  }

  /* to be safe */
  source[new_len++] = '\0';

  /* close the file */
  fclose(fp);

  return source;
}

void common_load_identity_3(int location1, int location2, int location3) {
  mat4x4 matrix;
  mat4x4_identity(matrix);
  glUniformMatrix4fv(location1, 1, GL_FALSE, (const float*)matrix);
  glUniformMatrix4fv(location2, 1, GL_FALSE, (const float*)matrix);
  glUniformMatrix4fv(location3, 1, GL_FALSE, (const float*)matrix);
}

void common_load_perspective(int uniformLocation) {
  mat4x4 matrix;
  mat4x4_identity(matrix);

  mat4x4_perspective(matrix, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
  glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, (const float*)matrix);
}

void common_matrix4fv(int uniformLocation, const mat4x4 matrix) {
  glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, (const float*)matrix);
}

void common_set_matrix_uniform(int uniformLocation, const mat4x4 T) {
  glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, (const float*)T);
}

void common_translate(mat4x4 T, vec3 v) {
  mat4x4_translate(T, v[0], v[1], v[2]); 
}

void common_set_uniform_vec(int uniformLocation, vec3 v) {
  glUniform3fv(uniformLocation, 1, (const float *)v);
}

void common_set_camera_matrix(int uniformLocation) {
  /* vec3 center; */
  /* vec3_add(center, camera.position, camera.front); */

  /* mat4x4_identity(T); */
  /* mat4x4_look_at(T, camera.position, center, camera.up); */
  mat4x4 T;
  camera_load_view_matrix(T);

  glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, (const float*)T);
}

static void initialize() {
  printf("INITIALIZE\n");
}

static void animate() {
  printf("ANIMATE\n");
}

static void registerer(int index, void (*initialize)(), void (*animate)()) {
  project.initializers[index] = initialize;
  project.animaters[index] = animate;
}

static void dynamic_registerer(void (*initialize)(), void (*animate)()) {
  project.dynamic_initializers = realloc(project.dynamic_initializers, sizeof(void*) * DYNAMIC_PROJECT_COUNT+1);
  project.dynamic_animators = realloc(project.dynamic_animators, sizeof(void*) * DYNAMIC_PROJECT_COUNT+1);

  project.dynamic_initializers[DYNAMIC_PROJECT_COUNT] = initialize;
  project.dynamic_animators[DYNAMIC_PROJECT_COUNT] = animate;

  DYNAMIC_PROJECT_COUNT += 1;
}

void project_demo_start_dynamic(void (*dynamic_registerer)(void (*initialize)(), void (*animate)())) {
  dynamic_registerer(initialize, animate);
}

void demo1() {
  printf("DMEO 1\n");
}
void demo2() {
  printf("DMEO 2\n");
}
void demo3() {
  printf("DMEO 3\n");
}

void (**demos)();

int count = 0;

void add(void (*demo)()) {
  demos = realloc(demos, sizeof(void *) * count+1);
  demos[count] = demo;
  count += 1;
}

int main(void)
{
  srand(time(NULL));
  int i = 0;

  project_circle_start(0, registerer);
  project_random_lines_start(1, registerer);
  project_sin_wave_start(2, registerer);

  project_demo_start(dynamic_registerer);
  project_lissajous_curve_start(dynamic_registerer);
  project_circles_around_start(dynamic_registerer);
  project_arrow_start(dynamic_registerer);
  project_velocity_start(dynamic_registerer);
  project_acceleration_start(dynamic_registerer);
  project_advanced_acceleration_start(dynamic_registerer);
  project_ship_start(dynamic_registerer);
  project_ray_start(dynamic_registerer);
  project_cube_start(dynamic_registerer);
  project_click_to_move_start(dynamic_registerer);

  /* project_sin_wave_start(0, registerer); */
  /* project_sin_wave_start(1, registerer); */
  /* project_sin_wave_start(2, registerer); */

  /* project_circle_start(0, dynamic_registerer); */
  /* project_random_lines_start(1, dynamic_registerer); */
  /* project_sin_wave_start(2, dynamic_registerer); */


  /* project.count = 0; */

  /* /1* demos = malloc(sizeof(void *) * 10000*3); *1/ */

  /* for (i=0; i<100000; i++) { */
  /*   add(demo1); */
  /*   add(demo2); */
  /*   add(demo3); */
  /* } */


  /* for (i=0; i<count; i++) { */
  /*   /1* demos[i](); *1/ */
  /*   (*(demos + i))(); */
  /* } */

  /* project.dynamic_initializers = malloc(sizeof(void *)); */
  /* project.dynamic_initializers[0] = animate; */
  /* project.dynamic_initializers[1] = animate; */

  /* project.dynamic_initializers[0](); */
  /* project.dynamic_initializers[1](); */

  /* register projects */

  /* register dynamic */
  /* project_demo_start_dynamic(dynamic_registerer); */
  /* project_demo_start_dynamic(dynamic_registerer); */
  /* project_demo_start_dynamic(dynamic_registerer); */
  /* project_demo_start_dynamic(dynamic_registerer); */

  /* for (i=0; i<project.count; i++) { */
  /*   project.dynamic_initializers[i](); */
  /* } */

  /* for (i=0; i<project.count; i++) { */
  /*   project.dynamic_animators[i](); */
  /* } */

  /* set up window */
  /* Initialize the library */
  if (!glfwInit())
    return -1;

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  window = window_start(width, height);

  if (window == NULL) {
    return -1;
  }

  glfwSetKeyCallback(window, keyboard);
  /* glfwSetMouseButtonCallback(window, mouse_button_callback); */
  window_register_mouse_button_callback(mouse_button_callback);
  glfwSetCursorPosCallback(window, mousemove);

  /* load all opengl function pointers */
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    fprintf(stderr, "Failed to initialize GLAD");
    return -1;
  }

  program = program_start();

  /* /1* create vertex shader *1/ */
  /* char *vertex_source = load_source("default.vs"); */
  /* if (vertex_source == NULL) { */
  /*   fprintf(stderr, "Failed to load vertex source!"); */
  /*   return -1; */
  /* } */
  /* int vertex_shader = glCreateShader(GL_VERTEX_SHADER); */
  /* glShaderSource(vertex_shader, 1, (const char **)&vertex_source, NULL); */
  /* glCompileShader(vertex_shader); */

  /* int success = GL_FALSE; */
  /* char info[512]; */

  /* glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success); */
  /* if (!success) { */
  /*   glGetShaderInfoLog(vertex_shader, 512, NULL, info); */
  /*   fprintf(stderr, "Error: vertex compilation error\n%s\n", info); */
  /*   return -1; */
  /* } */

  /* /1* create fragment shader *1/ */
  /* char *fragment_source = load_source("default.fs"); */
  /* if (fragment_source == NULL) { */
  /*   fprintf(stderr, "Failed to load fragment source!"); */
  /*   return -1; */
  /* } */
  /* int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER); */
  /* glShaderSource(fragment_shader, 1, (const char **)&fragment_source, NULL); */
  /* glCompileShader(fragment_shader); */

  /* glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success); */
  /* if (!success) { */
  /*   glGetShaderInfoLog(fragment_shader, 512, NULL, info); */
  /*   fprintf(stderr, "Error: fragment compilation error\n%s\n", info); */
  /*   return -1; */
  /* } */

  /* /1* create shader program *1/ */
  /* program = glCreateProgram(); */
  /* glAttachShader(program, vertex_shader); */
  /* glAttachShader(program, fragment_shader); */
  /* glLinkProgram(program); */

  /* glGetProgramiv(program, GL_LINK_STATUS, &success); */
  /* if (!success) { */
  /*   glGetProgramInfoLog(program, 512, NULL, info); */
  /*   fprintf(stderr, "Error: shader program linkiing error.\n%s\n", info); */
  /*   return -1; */
  /* } */

  /* glDeleteShader(vertex_shader); */
  /* glDeleteShader(fragment_shader); */

  /* free(vertex_source); */
  /* free(fragment_source); */

  /* glUseProgram(program); */

  /* uniform locations */

  uniform.color = glGetUniformLocation(program, "color");

  uniform.model = glGetUniformLocation(program, "model");
  uniform.view = glGetUniformLocation(program, "view");
  uniform.projection = glGetUniformLocation(program, "projection");

  mat4x4 model;
  mat4x4 view;
  mat4x4 projection;

  /* PI / 4 = 45 deg */
  /* PI / 2 = 180 deg */

  /* set up vertex data */
  float vertices[] = {
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    1.0f, 1.0f, 0.0f,
    -1.0f, 1.0f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  unsigned int vao, vbo, ebo;

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  unsigned int cubeVao, cubeVbo;

  float cubeVertices[36 * 3] = {
    /* back side */
    -1.0f, -1.0f, -1.0f,
    +1.0f, -1.0f, -1.0f,
    +1.0f, +1.0f, -1.0f,

    +1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,

    /* front side */
    -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f,

    +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f,

    /* left side */
    -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f,

    -1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f,

    /* right side */
    +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f,
    +1.0f, +1.0f, -1.0f,

    +1.0f, +1.0f, -1.0f,
    +1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,

    /* top side */
    -1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f,

    +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f,

    /* bottom side */
    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,

    +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
  };

  glGenVertexArrays(1, &cubeVao);
  glGenBuffers(1, &cubeVbo);

  glBindVertexArray(cubeVao);

  glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  /* set up grid vertex data */

  float v[84][3];

  float c = -1.0f;

  for (i=0; i<84; i+=4) {
    float vec1[3], vec2[3];

    vec1[0] = c;
    vec1[1] = 1.0f;
    vec1[2] = 0.0f;

    vec2[0] = c;
    vec2[1] = -1.0f;
    vec2[2] = 0.0f;

    v[i][0] = vec1[0];
    v[i][1] = vec1[1];
    v[i][2] = vec1[2];

    v[i+1][0] = vec2[0];
    v[i+1][1] = vec2[1];
    v[i+1][2] = vec2[2];

    vec1[0] = 1.0f;
    vec1[1] = c;
    vec1[2] = 0.0f;

    vec2[0] = -1.0f;
    vec2[1] = c;
    vec2[2] = 0.0f;

    v[i+2][0] = vec1[0];
    v[i+2][1] = vec1[1];
    v[i+2][2] = vec1[2];

    v[i+3][0] = vec2[0];
    v[i+3][1] = vec2[1];
    v[i+3][2] = vec2[2];

    c += 0.1f;
  }

  glGenVertexArrays(1, &vaos.grid);
  glGenBuffers(1, &vbos.grid);
  glBindVertexArray(vaos.grid);
  glBindBuffer(GL_ARRAY_BUFFER, vbos.grid);
  glBufferData(GL_ARRAY_BUFFER, sizeof(v), v, GL_STATIC_DRAW);;
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

  /* cube / initialize cube / set up cube */
  cube.position[0] = 0.0f;
  cube.position[1] = 0.0f;
  cube.position[2] = 0.0f;

  cube.speed = 0.001f;

  cube.scale[0] = 0.5f;
  cube.scale[1] = 0.5f;
  cube.scale[2] = 0.5f;

  /* camera */
  camera.speed = 0.005;
  camera.sensitivity = 0.009f;
  camera.yaw = -M_PI * 2;
  camera.pitch = 0.0f;

  camera.position[0] = 0.0f;
  camera.position[1] = 3.0f;
  camera.position[2] = +15.0f;

  camera.up[0] = 0.0f;
  camera.up[1] = 1.0f;
  camera.up[2] = 0.0f;

  camera.front[0] = 0.0f;
  camera.front[1] = -0.2f;
  camera.front[2] = -1.0f;

  ground.position[0] = 0.0f;
  ground.position[1] = -2.0f;
  ground.position[2] = 0.0f;

  ground.scale[0] = 3.0f;
  ground.scale[1] = 3.0f;
  ground.scale[2] = 3.0f;

  ground.color[0] = 0.0f;
  ground.color[1] = 1.3f;
  ground.color[2] = 0.3f;

  struct Rect rect;

  rect.speed = 0.001f;

  rect.pos[0] = 0.0f;
  rect.pos[1] = 0.0f;
  rect.pos[2] = -5.0f;

  rect.color[0] = 1.0f;
  rect.color[1] = 1.0f;
  rect.color[2] = 0.3f;

  /* world initialize */
  world.width = (float)width;
  world.height = (float)height;

  /* grid initialize */
  grid.position[0] = 0.0f;
  grid.position[1] = 0.0f;
  grid.position[2] = 0.0f;

  grid.scale[0] = 10.0f;
  grid.scale[1] = 10.0f;
  grid.scale[2] = 10.0f;

  /* ortho rect */
  struct Rect o_rect;
  o_rect.speed = 0.001f;

  o_rect.scale = 25.0f;

  o_rect.pos[0] = 200.0f;
  o_rect.pos[1] = 200.0f;
  o_rect.pos[2] = -5.0f;

  o_rect.color[0] = 0.3f;
  o_rect.color[1] = 0.3f;
  o_rect.color[2] = 1.0f;

  for (i=0; i<PROJECT_COUNT; i++) {
    project.initializers[i]();
  }

  for (i=0; i<DYNAMIC_PROJECT_COUNT; i++) {
    project.dynamic_initializers[i]();
  }

  camera_start(window, program);

  renderer_start();
  project_circle2_start();
  project_ray_obb_start();

  /* Loop until the user closes the window */
  while (!glfwWindowShouldClose(window))
  {
    /* Render here */
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);
    glEnable(GL_DEPTH_TEST);

    glBindVertexArray(vao);

    vec3 center;
    vec3_add(center, camera.position, camera.front);

    camera_load_view_matrix(view);
    glUniformMatrix4fv(uniform.view, 1, GL_FALSE, (const float *)view);
    /* common_set_camera_matrix(uniform.view); */

    /* load perspective projection */
    mat4x4_identity(projection);
    mat4x4_perspective(projection, M_PI / 4, (float)width/(float)height, 0.1f, 100.0f);
    glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float*)projection);

    /* camera_load_perspective(); */

    /* draw rect */
    mat4x4_identity(model);
    mat4x4_translate(model, rect.pos[0], rect.pos[1], rect.pos[2]);
    mat4x4_rotate_X(model, model, (float) glfwGetTime());
    glUniformMatrix4fv(uniform.model, 1, GL_FALSE, (const float*)model);
    glUniform3fv(uniform.color, 1, (const float*)rect.color);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    /* draw ground */
    mat4x4_identity(model);
    common_translate(model, ground.position);
    mat4x4_scale_aniso(model, model, ground.scale[0], ground.scale[1], ground.scale[2]);
    mat4x4_rotate_X(model, model, M_PI / 2);
    common_set_matrix_uniform(uniform.model, model);
    common_set_uniform_vec(uniform.color, ground.color);
    /* glUniform3fv(uniform.color, 1, (const float*)ground.color); */
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    /* load identity view */
    mat4x4_identity(view);
    /* mat4x4_look_at(view, camera.position, center, camera.up); */
    glUniformMatrix4fv(uniform.view, 1, GL_FALSE, (const float*)view);
    /* load ortho projection */
    mat4x4_identity(projection);
    mat4x4_ortho(projection, 0.0f, (float)width, 0.0f, (float)height, 0.0f, 100.0f);
    glUniformMatrix4fv(uniform.projection, 1, GL_FALSE, (const float*)projection);

    /* draw ortho rect */

    o_rect.pos[0] = mouse.pos[0];
    o_rect.pos[1] = mouse.pos[1] - 100.0f;

    mat4x4_identity(model);
    mat4x4_translate(model, o_rect.pos[0], o_rect.pos[1], o_rect.pos[2]);
    mat4x4_rotate_Z(model, model, (float) glfwGetTime());
    mat4x4_scale_aniso(model, model, o_rect.scale, o_rect.scale, o_rect.scale);
    glUniformMatrix4fv(uniform.model, 1, GL_FALSE, (const float*)model);
    if (mouse.button[GLFW_MOUSE_BUTTON_RIGHT]) {
      glUniform3f(uniform.color, 1.0f, 0.0f, 0.0f);
    } else if (mouse.button[GLFW_MOUSE_BUTTON_LEFT]) {
      glUniform3f(uniform.color, 0.0f, 1.0f, 0.0f);
    } else if (mouse.button[GLFW_MOUSE_BUTTON_MIDDLE]) {
      glUniform3f(uniform.color, 1.0f, 0.3f, 0.5f);
    } else {
      glUniform3fv(uniform.color, 1, (const float*)o_rect.color);
    }
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    /* draw cube */
    glBindVertexArray(cubeVao);

    common_load_perspective(uniform.projection);
    common_set_camera_matrix(uniform.view);

    mat4x4_identity(model);
    mat4x4_translate(model, cube.position[0], cube.position[1], cube.position[2]);
    mat4x4_scale_aniso(model, model, cube.scale[0], cube.scale[1], cube.scale[2]);
    /* mat4x4_rotate_X(model, model, (float) glfwGetTime()); */
    common_matrix4fv(uniform.model, model);
    glUniform3f(uniform.color, 1.0f, 0.0f, 0.0f);

    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    /* draw grid */
    glBindVertexArray(vaos.grid);

    common_load_perspective(uniform.projection);
    common_set_camera_matrix(uniform.view);
    mat4x4_identity(model);
    mat4x4_translate(model, grid.position[0], grid.position[1], grid.position[2]);
    mat4x4_scale_aniso(model, model, grid.scale[0], grid.scale[1], grid.scale[2]);
    common_set_matrix_uniform(uniform.model, model);

    glPointSize(10.0f);
    glUniform3f(uniform.color, 0.0, 1.0, 0.0);
    glDrawArrays(GL_LINES, 0, 84);
    mat4x4_rotate_Y(model, model, M_PI / 2);
    common_set_matrix_uniform(uniform.model, model);
    glDrawArrays(GL_LINES, 0, 84);
    glBindVertexArray(0);

    /* update rect movement position */

    /* update cube gravitation */
    if (cube.position[1] > ground.position[1] + 0.5) {
      cube.position[1] -= 0.001f;
    }

    /* do cube movement */
    if (keys[GLFW_KEY_LEFT]) {
      cube.position[0] -= cube.speed;
    }
    if (keys[GLFW_KEY_RIGHT]) {
      cube.position[0] += cube.speed;
    }
    if (keys[GLFW_KEY_UP]) {
      cube.position[2] -= cube.speed;
    }
    if (keys[GLFW_KEY_DOWN]) {
      cube.position[2] += cube.speed;
    }

    /* camera keyboard keys 'q' 'E' || 'Q' 'E' */
    if (keys[GLFW_KEY_Q]) {
      camera.front[0] -= 0.0003f;
    }
    if (keys[GLFW_KEY_E]) {
      camera.front[0] += 0.0003f;
    }

    /* mouse position at window corner rotate camera */
    /* printf("mouse x: [%.2f]\n", mouse.default_position[0]); */

    if (!firstMouse && mouse.default_position[0] < 0 + 30.0f) {
      camera.front[0] -= 0.0003f;
    }
    if (!firstMouse && mouse.default_position[0] > (float)width - 30.0f) {
      camera.front[0] += 0.0003f;
    }
    if (!firstMouse && mouse.default_position[1] < 0 + 30.0f) {
      camera.front[1] += 0.0003f;
    }
    if (!firstMouse && mouse.default_position[1] > (float)height - 30.0f) {
      camera.front[1] -= 0.0003f;
    }

    for (i=0; i<PROJECT_COUNT; i++) {
      project.animaters[i]();
    }

    for (i=0; i<DYNAMIC_PROJECT_COUNT; i++) {
      project.dynamic_animators[i]();
    }

    vec3 step;

    /* if (keys[GLFW_KEY_W]) { */
    /*   step[0] = camera.speed * camera.front[0]; */
    /*   step[1] = camera.speed * camera.front[1]; */
    /*   step[2] = camera.speed * camera.front[2]; */

    /*   vec3_add(camera.position, camera.position, step); */
    /* } */
    /* if (keys[GLFW_KEY_S]) { */

    /*   step[0] = camera.speed * camera.front[0]; */
    /*   step[1] = camera.speed * camera.front[1]; */
    /*   step[2] = camera.speed * camera.front[2]; */

    /*   vec3_sub(camera.position, camera.position, step); */
    /* } */
    /* if (keys[GLFW_KEY_A]) { */
    /*   vec3_mul_cross(step, camera.front, camera.up); */
    /*   vec3_norm(step, step); */
    /*   step[0] *= camera.speed; */
    /*   step[1] *= camera.speed; */
    /*   step[2] *= camera.speed; */
    /*   vec3_sub(camera.position, camera.position, step); */

    /* } */
    /* if (keys[GLFW_KEY_D]) { */
    /*   vec3_mul_cross(step, camera.front, camera.up); */
    /*   vec3_norm(step, step); */
    /*   step[0] *= camera.speed; */
    /*   step[1] *= camera.speed; */
    /*   step[2] *= camera.speed; */
    /*   vec3_add(camera.position, camera.position, step); */
    /* } */
    /* if (keys[GLFW_KEY_SPACE]) { */
    /*   camera.position[1] += camera.speed; */
    /* } */
    /* if (keys[GLFW_KEY_LEFT_SHIFT]) { */
    /*   camera.position[1] -= camera.speed; */
    /* } */

    camera_update();

    renderer_update();
    /* Swap front and back buffers */
    glfwSwapBuffers(window);

    /* Poll for and process events */
    glfwPollEvents();
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);

  glfwTerminate();
  return 0;
}
