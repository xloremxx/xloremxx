#ifndef MY_MAIN_H
#define MY_MAIN_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <linmath.h>

void main_set_color_3f(float r, float g, float b);

/* constant unsigned integers */
#define PERSPECTIVE_MATRIX   0
#define CAMERA_MATRIX        1

/* project function prototypes */
void project_circle_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)()));
void project_random_lines_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)()));
void project_sin_wave_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)()));
void project_demo_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_ray_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_lissajous_curve_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_circles_around_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_acceleration_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_velocity_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_advanced_acceleration_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_ship_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_cube_start(void (*registerer)(void (*initialize)(), void (*animate)()));
void project_click_to_move_start(void (*registerer)(void (*initialize)(), void (*animate)()));
int project_ray_obb_start();

void project_circle2_start();

void project_arrow_start(void (*registerer)(void (*initialize)(), void (*animate)()));

void main_load_ortho();
void main_load_dimensions(float *w, float *h);
void main_identity_all();
void main_load_perspective();
int main_program();
void main_load_camera_view();
void main_set_model(const mat4x4 M);
void main_set_view(const mat4x4 M);
void main_set_projection(const mat4x4 M);

/* data function prototypes */
void data_load_vertices_cube(float vertices[36 * 3]);

/* window function prototypes */
GLFWwindow* window_start(unsigned int width, unsigned int height);
void window_get_size_i(int *width, int *height);
void window_get_size_f(float *width_ptr, float *height_ptr);
void window_get_size(float *w, float *h);
void window_set_cursor_disabled();
void window_set_cursor_normal();
GLFWwindow *window_return_pointer();
void window_get_mouse_position_f(float *x, float *y);
void window_register_mouse_button_callback(void (*mouse_button_callback)(int button, int action, int mods));
void WindowLoadMousePosition(float position[3]);
void WindowLoadSize(float *width, float *height);

/* common function prototypes */
char *common_load_source(const char *path);
void CommonPrintMatrix(mat4x4 M);
void common_vec4_divide(vec4 vec, float n);
void common_vec3_multi(vec3 V, float x);
void common_set_vec3(vec3 V, float x, float y, float z);
float common_dot_product(vec3 v1, vec3 v2);

/* camera function prototypes */
/* void camera_initialize(GLFWwindow* window, int program_id); */
void camera_start(GLFWwindow* window, int program_id);
void camera_update();
void camera_update_matrix();
void camera_load_view_matrix(mat4x4 M);
void camera_update_matrix_and_get(mat4x4 view_matrix);
void camera_calculate_view_matrix(mat4x4 M);
void camera_get_position(float pos[3]);
void camera_get_up(float v[3]);

/* program function prototypes */
int program_start();
void program_set_matrix_model(mat4x4 model);
void program_set_matrix_view(mat4x4 view);
void program_set_matrix_projection(mat4x4 projection);
void program_load_default();
void program_load_identity();
void program_load_identity_model();
void program_set_color(float r, float g, float b);
void program_load_ortho();
void program_load_identity_view();
void program_load_normalized_device_space();
void program_load_identity_projection();
void program_load_projection_perspective();
void program_update_and_get_matrices(mat4x4 view_matrix, mat4x4 projection_matrix);
void program_make_perspective_matrix(mat4x4 M);
void ProgramGetInvertedMatrix(mat4x4 M, const unsigned int type);
float * ProgramGetMatrix(const unsigned int type);

/* renderer function prototypes */
void renderer_start();
void renderer_update();
void renderer_register_animate(void (*animate)());

int *main_load_keys();

struct Mouse {
  vec3 default_position;
  vec3 pos;
  vec3 lastPos;
  int button[1024];
};

struct Mouse *main_load_mouse();
void main_register_mousemove(void (*mousemove)(float x, float y));
void main_register_callback_mousemove(void (*mousemove_callback)(float x, float y));
void main_register_keyboard(void (*keyboard)(int key, int scancode, int action, int mods));
void main_register_callback_keyboard(void (*keyboard_callback)(int key, int scancode, int action, int mods));
void circle2_start();

struct Camera {
  float sensitivity;
  float speed;
  vec3 up;
  vec3 position;
  vec3 front;
  float yaw;
  float pitch;
};

struct World {
  float width;
  float height;
};
void main_load_world(struct World *ptr);
#endif
