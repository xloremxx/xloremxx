#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static float position[3] = {-5.0, 7.0, 0.0};
static unsigned int vao, vbo, ebo;

static int NUM_SQUARES = 0;

struct Square {
  float size;
  float position[3];
};
static struct Square **squares;

static struct Square *square_make(float size) {
  struct Square *square = malloc(sizeof(struct Square));
  squares = realloc(squares, sizeof(struct Square *) * NUM_SQUARES+1);
  squares[NUM_SQUARES] = square;

  square->position[0] = 0.0f;
  square->position[1] = 0.0f;
  square->position[2] = 0.0f;

  square->size = size;

  NUM_SQUARES += 1;
  return square;
}

static void square_update(struct Square *square) {
  mat4x4 model;
  mat4x4_translate(model, position[0], position[1], position[2]);
  mat4x4_translate_in_place(model, square->position[0], square->position[1], square->position[2]);
  mat4x4_scale_aniso(model, model, square->size, square->size, square->size);
  main_set_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static struct Square *square;

static void initialize() {
  int i;

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  square = square_make(0.2);

  float center[2] = { 0.0f, 0.0f };
  float radius = 2.0f;
  float count = 20;
  float slice = M_PI * 2 / count;

  for (i=0; i<count; i+=1) {
    struct Square *square = square_make(0.2);

    float angle = i * slice;

    square->position[0] = center[0] + cosf(angle) * radius;
    square->position[1] = center[1] + sinf(angle) * radius;
  }
}

static void animate() {
  int i;
  glBindVertexArray(vao);
  main_identity_all();
  main_load_perspective();
  main_load_camera_view();
  for (i=0; i<NUM_SQUARES; i+=1) {
    square_update(squares[i]);
  }
  glBindVertexArray(0);
}

void project_circles_around_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
