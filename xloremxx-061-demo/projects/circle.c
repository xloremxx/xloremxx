#include <stdio.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static int program;

static const int NUM_VERTICES = 3*31;

static unsigned int vao;
static unsigned int vbo;

static vec3 position;

static void make_circle_vertices(float *T, const int num) {
  int i, s = 0;

  int v = num / 3;

  for (i=0; i<v; i++) {
    float angle = 2 * M_PI * i / v;

    T[s] = cosf(angle);
    T[s+1] = sinf(angle);
    T[s+2] = 0.0f;

    s += 3;
  }
}

static void initialize() {

  program = main_program();

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  float vertices[NUM_VERTICES];

  make_circle_vertices(vertices, NUM_VERTICES);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  position[0] = -5.0f;
  position[1] = 0.0f;
  position[2] = 0.0f;
}

static void animate() {
  glBindVertexArray(vao);

  main_set_color_3f(0.2f, 0.5f, 0.3f);
  main_identity_all();
  main_load_perspective();
  main_load_camera_view();

  mat4x4 model;
  mat4x4_identity(model);
  mat4x4_translate(model, position[0], position[1], position[2]);
  main_set_model(model);

  glDrawArrays(GL_TRIANGLE_FAN, 0, NUM_VERTICES/3);
  glBindVertexArray(0);
}

void project_circle_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)())) {
  registerer(index, initialize, animate);
}
