#include <main.h>
#include <stdio.h>

static void animate();

static const unsigned int VAO_RECTANGLE = 0;
static const unsigned int VBO_RECTANGLE = 0;
static const unsigned int EBO_RECTANGLE = 0;

static const unsigned int VAO_CUBE = 1;
static const unsigned int VBO_CUBE = 1;
static const unsigned int EBO_CUBE = 1;

static const unsigned int CAMERA = 0;
static const unsigned int PERSPECTIVE = 0;

static struct {
  float position[3];
} mouse;

static struct {
  float width;
  float height;
} window;

static unsigned int vaos[2];
static unsigned int vbos[2];
static unsigned int ebos[2];
static float vertices[] = {
  -1.0f, -1.0f, 0.0f,
  1.0f, -1.0f, 0.0f,
  1.0f, 1.0f, 0.0f,
  -1.0f, 1.0f, 0.0f,
};

/*         
 *     -1 +1 -1 ________________ +1 +1 -1
 *             /|(3)         (2)|
 *            / |             / |
 *           /  |            /  |
 *          /   |           /   |
 * -1 +1 +1/(7)_|_______(6)/+1 +1 +1 
 *         |    |          |    |
 *         |    |          |    |
 *         |    |          |    | 
 *     -1  -1 -1|(0)_______|_(1)| +1 -1 -1
 *         |   /           |   /
 *         |  /            |  /
 *         | /             | /
 *         |/(4)________(5)|/ 
 * -1 -1 +1                  +1 -1 +1
 *         
 *
 */

static unsigned int indices[] = {
  0, 1, 2,
  0, 2, 3,
};

static float cube_vertices[] = {
  /* backside */
  -1.0f, -1.0f, -1.0f,
  +1.0f, -1.0f, -1.0f,
  +1.0f, +1.0f, -1.0f,
  -1.0f, +1.0f, -1.0f,

  /* frontside */
  -1.0f, -1.0f, +1.0f,
  +1.0f, -1.0f, +1.0f,
  +1.0f, +1.0f, +1.0f,
  -1.0f, +1.0f, +1.0f,
};

static unsigned int cube_indices[] = {
  /* back side */
  0, 1, 2, 
  0, 2, 3,
  /* front side */
  4, 5, 6,
  4, 6, 7,
  /* left side */
  4, 0, 3,
  4, 3, 7,
  /* right side */
  5, 1, 2, 
  5, 2, 6,
  /* bottom side */
  4, 5, 1,
  4, 1, 0,
  /* top side */
  7, 6, 2, 
  7, 2, 3,
};

int project_ray_obb_start() {

  glGenVertexArrays(2, vaos);
  glGenBuffers(2, vbos);
  glGenBuffers(2, ebos);

  /* make rectangle vao */
  glBindVertexArray(vaos[VAO_RECTANGLE]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_RECTANGLE]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebos[EBO_RECTANGLE]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  /* make cube vao */
  glBindVertexArray(vaos[VAO_CUBE]);
  glBindBuffer(GL_ARRAY_BUFFER, vbos[VBO_CUBE]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebos[EBO_CUBE]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  renderer_register_animate(animate);
}

static void LoadProjection(unsigned int type) {
  if (type == PERSPECTIVE) {
    program_load_projection_perspective();
  }
}

static void LoadView(unsigned int type) {
  if (type == CAMERA) {
    camera_update_matrix();
  }
}

static void LoadVao(unsigned int id) {
  glBindVertexArray(vaos[id]);
}

static void DrawRectangleW(float x, float y, float z, float scale) {
  mat4x4 model;
  LoadVao(VAO_RECTANGLE);
  LoadProjection(PERSPECTIVE);
  LoadView(CAMERA);

  mat4x4_translate(model, x, y, z);
  mat4x4_scale_aniso(model, model, scale, scale, scale);
  program_set_matrix_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static void DrawCubeM(mat4x4 model) {
  LoadVao(VAO_CUBE);
  LoadProjection(PERSPECTIVE);
  LoadView(CAMERA);
  program_set_matrix_model(model);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
}

static void DrawCubeW(float x, float y, float z, float scale) {
  mat4x4 model;
  LoadVao(VAO_CUBE);
  LoadProjection(PERSPECTIVE);
  LoadView(CAMERA);

  static float rotation = 0.0f;
  rotation += 0.001f;

  mat4x4_translate(model, x, y, z);
  mat4x4_scale_aniso(model, model, scale, scale, scale);
  /* mat4x4_rotate(model, model, 1.0f, 0.0f, 1.0f, rotation); */
  program_set_matrix_model(model);

  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
}

static void animate() {

  int i;

  for (i=0; i<7; i++) {
    DrawRectangleW((i + i * 2.0f)-9.0f, -9.0f, 0.0f, 1.0f);
  }

  DrawCubeW(9.0f, 9.0f, 0.0f, 1.0f);

  /* 1. receive mouse position */
  WindowLoadMousePosition(mouse.position);

  /* 2. receive window size */
  WindowLoadSize(&window.width, &window.height);

  /* 3. convert screen position to world ray */
  vec4 ndc_start, ndc_end;

  ndc_start[0] = (mouse.position[0] / window.width - 0.5f) * 2.0f;
  ndc_start[1] = -(mouse.position[1] / window.height - 0.5f) * 2.0f;
  ndc_start[2] = -1.0f;
  ndc_start[3] = +1.0f;

  ndc_end[0] = (mouse.position[0] / window.width - 0.5f) * 2.0f;
  ndc_end[1] = -(mouse.position[1] / window.height - 0.5f) * 2.0f;
  ndc_end[2] = +0.0f;
  ndc_end[3] = +1.0f;

  /* 4. invert view and projection matrix */
  mat4x4 inverted_projection_matrix, inversed_view_matrix;
  ProgramGetInvertedMatrix(inverted_projection_matrix, PERSPECTIVE_MATRIX);
  ProgramGetInvertedMatrix(inversed_view_matrix, CAMERA_MATRIX);

  /* 5. camera start / end ray */
  vec4 camera_start, camera_end;

  mat4x4_mul_vec4(camera_start, inverted_projection_matrix, ndc_start);
  common_vec4_divide(camera_start, camera_start[3]);

  mat4x4_mul_vec4(camera_end, inverted_projection_matrix, ndc_end);
  common_vec4_divide(camera_end, camera_end[3]);

  vec4 world_start, world_end;

  mat4x4_mul_vec4(world_start, inversed_view_matrix, camera_start);
  common_vec4_divide(world_start, world_start[3]);

  mat4x4_mul_vec4(world_end, inversed_view_matrix, camera_end);
  common_vec4_divide(world_end, world_end[3]);

  struct {
    vec3 direction;
    vec3 origin;
  } ray;

  vec3 world_direction;
  vec3_sub(world_direction, world_end, world_start);

  vec3_norm(world_direction, world_direction);

  ray.origin[0] = world_start[0];
  ray.origin[1] = world_start[1];
  ray.origin[2] = world_start[2];

  vec3_norm(ray.direction, world_direction);

  static struct {
    vec3 center;
    float radius;
    float angle;
    float size;
    vec3 position;
  } mesh;

  mesh.size = 0.05f;

  mesh.angle += 0.001f;
  mesh.radius = 1.0f;

  /* vec3_add(mesh.position, ray.origin, ray.direction); */
  /* common_vec3_multi(mesh.position, 2.0f); */

  mesh.center[0] = ray.origin[0] + ray.direction[0] * 5.0f;
  mesh.center[1] = ray.origin[1] + ray.direction[1] * 5.0f;
  mesh.center[2] = ray.origin[2] + ray.direction[2] * 5.0f;

  mesh.position[0] = mesh.center[0] + cosf(mesh.angle) * mesh.radius;
  mesh.position[1] = mesh.center[1] + sinf(mesh.angle) * mesh.radius;
  mesh.position[2] = mesh.center[2];

  /* position = origin + direction * length */

  DrawCubeW(mesh.position[0], mesh.position[1], mesh.position[2], mesh.size);

  static struct {
    float size;
    vec3 position;
  } box;

  box.size = 1.0f;
  common_set_vec3(box.position, 0.0f, 0.0f, 0.0f);

  int intersect = 1;

  mat4x4 model;
  mat4x4_translate(model, box.position[0], box.position[1], box.position[2]);
  DrawCubeM(model);

  /* test ray obb intersection */

  float intersection_distance;

  vec3 aabb_min;
  common_set_vec3(aabb_min, -1.0f, -1.0f, -1.0f);
  vec3 aabb_max;
  common_set_vec3(aabb_max, 1.0f, 1.0f, 1.0f);

  float t_min = 0.0f;
  float t_max = 100000.0f;

  vec3 obb_world_position;
  obb_world_position[0] = model[3][0];
  obb_world_position[1] = model[3][1];
  obb_world_position[2] = model[3][2];

  vec3 delta;
  vec3_sub(delta, obb_world_position, ray.origin);

  vec3 x_axis;
  x_axis[0] = model[0][0];
  x_axis[1] = model[0][1];
  x_axis[2] = model[0][2];

  float e = common_dot_product(x_axis, delta);
  float f = common_dot_product(ray.direction, x_axis);

  if (fabsf(f) > 0.001f) {
    float t1 = ( e + aabb_min[0] ) / f; /* intersection with 'left' plane */
    float t2 = ( e + aabb_max[0] ) / f; /* intersection with 'right' plane */
    printf("[t1, t2]: '%.2f', '%.2f' \n", t1, t2);

    if (t1 > t2) {
      float w = t1; t1 = t2; t2 = w; /* swap t1 and t2 */
    }

    /* t_max is the nearest 'far' intersection amongst the x,y,z plane pairs */
    if (t2 < t_max) {
      t_max = t2;
    }

    /* t_min is the farthest 'near' intersection */
    if (t1 > t_min) {
      t_min = t1;
    }

    /* trick: if 'far' is closer than 'near', then there is no intersection */
    if (t_max < t_min) 
      intersect = 0;
  } else {
    if (-e + aabb_min[0] > 0.0f || -e + aabb_max[0] < 0.0f) {
      intersect = 0;
    }
  }

  static int xx = 0;

  xx += 1;
  printf("[RAY]: origin -> '%.2f, %.2f, %.2f, direction -> '%.2f, %.2f, %.2f''.\n", ray.origin[0], ray.origin[1], ray.origin[2], ray.direction[0], ray.direction[1], ray.direction[2]);
  printf("[OBB]: '%.2f, %.2f, %.2f'.\n", obb_world_position[0], obb_world_position[1], obb_world_position[2]);
  printf("[DELTA]: '%.2f, %.2f, %.2f'.\n", delta[0], delta[1], delta[2]);
  printf("[t_min, t_max]: '%.2f', '%.2f' \n", t_min, t_max);
  printf("[f, e]: '%.2f', '%.2f' \n", f, e);
  printf("[aabb_min_x, aabb_max_x]: '%.2f', '%.2f' \n", aabb_min[0], aabb_max[0]);

  if (intersect) {
    printf("[INTERSECTION '%d']\n", xx);
  }

  /* vec3 delta; */
  /* vec3_sub(obb_world_position, ray.origin); */

  /* printf("----------------------------------------------------------\n"); */
  /* /1* printf("mouse: {%.2f, %.2f}.\n", mouse.position[0], mouse.position[1]); *1/ */
  /* /1* printf("window: {%.2f, %.2f}.\n", window.width, window.height); *1/ */
  /* printf("[NDC START]: '%.2f, %.2f, %.2f, %.2f'.\n", ndc_start[0], ndc_start[1], ndc_start[2], ndc_start[3]); */
  /* printf("[NDC END]: '%.2f, %.2f, %.2f, %.2f'.\n", ndc_end[0], ndc_end[1], ndc_end[2], ndc_end[3]); */
  /* printf("[CAMEAR START]: '%.2f, %.2f, %.2f, %.2f'.\n", camera_start[0], camera_start[1], camera_start[2], camera_start[3]); */
  /* printf("[CAMERA END]: '%.2f, %.2f, %.2f, %.2f'.\n", camera_end[0], camera_end[1], camera_end[2], camera_end[3]); */
  /* printf("[MESH]: '%.2f, %.2f, %.2f'.\n", mesh.position[0], mesh.position[1], mesh.position[2]); */
}
