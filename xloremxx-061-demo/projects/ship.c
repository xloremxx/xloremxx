#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <main.h>

static unsigned int vao, vbo, ebo;

struct Particle {
  float rotation[3];
  float scale[3];
  float position[3];
  float velocity[3];
  float acceleration[3];
  float gravity[3];
};

int turning_left = 0;
int turning_right = 0;
int thrusting = 0;

static int particle_store_count = 0;
static struct Particle **particle_store;

static int *keys;

static struct Particle *ship;
static struct Particle *tail;
static float angle = 0.0f;
static float thrust[3] = {0.0f, 0.0f, 0.0f};

static void vec_add_to(float vec[3], const float A[3]) {
  vec[0] += A[0];
  vec[1] += A[1];
  vec[2] += A[2];
}

static void vec_setf(float vec[3], float x, float y, float z) {
  vec[0] = x;
  vec[1] = y;
  vec[2] = z;
}

static void vec_set(float vec[3], const float T[3]) {
  vec[0] = T[0];
  vec[1] = T[1];
  vec[2] = T[2];
}

static float vec_get_angle(float vec[3]) {
  return atan2f(vec[1], vec[0]);
}

static void vec_set_length(float vec[3], float length) {
  float angle = vec_get_angle(vec);
  vec[0] = cosf(angle) * length;
  vec[1] = sinf(angle) * length;
}

static float vec_get_length(float vec[3]) {
  return sqrtf(vec[0] * vec[0] + vec[1] * vec[1]);
}

static void vec_set_angle(float vec[3], float angle) {
  float length = vec_get_length(vec);
  vec[0] = cosf(angle) * length;
  vec[1] = sinf(angle) * length;
}

static struct Particle *particle_create(float x, float y, float speed, float direction) {
  struct Particle *particle = malloc(sizeof(struct Particle));
  particle_store = realloc(particle_store, sizeof(struct Particle*) * particle_store_count+1);
  particle_store[particle_store_count] = particle;

  vec_setf(particle->acceleration, 0.0f, 0.0f, 0.0f);
  vec_setf(particle->scale, 0.5f, 0.5f, 0.5f);
  vec_setf(particle->position, x, y, 0.0f);
  vec_setf(particle->velocity, 0.0f, 0.0f, 0.0f);
  vec_setf(particle->gravity, 0.0f, 0.0f, 0.0f);
  vec_setf(particle->rotation, 0.0f, 0.0f, 0.0f);

  vec_set_length(particle->velocity, speed);
  vec_set_angle(particle->velocity, direction);

  particle_store_count += 1;
  return particle;
}

static void particle_update(struct Particle *particle) {
  vec_add_to(particle->velocity, particle->gravity);
  vec_add_to(particle->position, particle->velocity);
  vec_add_to(particle->velocity, particle->acceleration);
  mat4x4 model;
  mat4x4_translate(model, particle->position[0], particle->position[1], particle->position[2]);
  mat4x4_scale_aniso(model, model, particle->scale[0], particle->scale[1], particle->scale[1]);
  /* mat4x4_rotate_Z(model, model, -M_PI / 2); */
  mat4x4_rotate_X(model, model, particle->rotation[0]);
  mat4x4_rotate_Y(model, model, particle->rotation[1]);
  mat4x4_rotate_Z(model, model, particle->rotation[2]);
  /* mat4x4_rotate(model, model, 0.0f, 0.0f, 1.0f, angle); */
  main_set_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static void particle_accelerate(struct Particle *particle, const float vec[3]) {
  vec_add_to(particle->velocity, vec);
}

static void keyboard(int key, int scancode, int action, int mods) {
  if (action == GLFW_PRESS) {
    if (key == GLFW_KEY_UP) {
      thrusting = 1;
    }
    if (key == GLFW_KEY_LEFT) {
      turning_left = 1;
    }
    if (key == GLFW_KEY_RIGHT) {
      turning_right = 1;
    }
  }
  if (action == GLFW_RELEASE) {
    if (key == GLFW_KEY_UP) {
      thrusting = 0;
    }
    if (key == GLFW_KEY_LEFT) {
      turning_left = 0;
    }
    if (key == GLFW_KEY_RIGHT) {
      turning_right = 0;
    }
  }
}

static void initialize() {
  int i;

  keys = main_load_keys();

  main_register_keyboard(keyboard);

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.25f, -0.5f, 0.0f,
    +0.25f, -0.5f, 0.0f,
    +0.0f, +0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  ship = particle_create(0.0f, 0.0f, 0.0f, 0.0f);
  vec_setf(ship->scale, 1.0f, 1.0f, 1.0f);

  ship->rotation[2] = -M_PI / 2;

  tail = particle_create(0.0f, 0.0f, 0.0f, 0.0f);
  tail->rotation[2] = -M_PI / 2;
  /* mat4x4_rotate_Z(model, model, -M_PI / 2); */
}

static void animate() {
  int i;
  glBindVertexArray(vao);
  main_load_perspective();
  main_load_camera_view();
  for (i=0; i<particle_store_count; i+=1) {
    particle_update(particle_store[i]);
  }
  glBindVertexArray(0);

  /* ship movement */

  if (turning_left) {
    angle += 0.0005f;
  }
  if (turning_right) {
    angle -= 0.0005f;
  }

  vec_set_angle(thrust, angle);
  if (thrusting) {
    vec_set_length(thrust, 0.000002f);
  } else {
    vec_set_length(thrust, 0.0f);
  }

  /* handle world escape */
  if (ship->position[0] > 10.0f) {
    ship->position[0] = -10.0f;
  }
  if (ship->position[0] < -10.0f) {
    ship->position[0] = 10.0f;
  }
  if (ship->position[1] > 10.0f) {
    ship->position[1] = -10.0f;
  }
  if (ship->position[1] < -10.0f) {
    ship->position[1] = 10.0f;
  }

  particle_accelerate(ship, thrust);

  ship->rotation[2] = angle - M_PI / 2;
  /* ship->rotation[2] += angle; */

  /* update ship tail */
  if (thrusting) {
    vec_setf(tail->position, ship->position[0]-1.0f, ship->position[1], ship->position[2]);
  } else {
    vec_setf(tail->position, ship->position[0]-0.5f, ship->position[1], ship->position[2]);
  }
}

void project_ship_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
