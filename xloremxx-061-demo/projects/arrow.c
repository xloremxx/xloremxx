#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static unsigned int vao, vbo;
static struct World *world;

static struct {
  float position[3];
} mouse = { 0.0f, 0.0f, 0.0f };

struct {
  float position[3];
  float angle;
} arrow = {0.0f, 0.0f, 0.0f, 0.0f};

static float angle[2] = { 0.0f, 0.0f };
static float speed[2] = { 0.0f, 0.0f };
static float radius[2] = { 0.0f, 0.0f };
static float center[2] = { 0.0f, 0.0f };

static void mousemove(float x, float y) {
  mouse.position[0] = x;
  mouse.position[1] = -(y - world->height);

  /* float dx = mouse.position[0] - arrow.position[0]; */
  /* float dy = mouse.position[1] - arrow.position[1]; */

  /* arrow.angle = atan2f(dy, dx); */
}

static float random_number(float min, float max) {
  float scale = rand() / (float) RAND_MAX;
  return min + scale * ( max - min );
}

static void initialize() {
  world = malloc(sizeof(struct World));
  main_load_world(world);

  main_register_mousemove(mousemove);

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  float vertices[] = {
    20.0f, 0.0f, 0.0f,
    -20.0f, 0.0f, 0.0f,
    20.0f, 0.0f, 0.0f,
    10.0f, -10.0f, 0.0f,
    20.0f, 0.0f, 0.0f,
    10.0f, 10.0f, 0.0f,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  arrow.position[0] = world->width / 2;
  arrow.position[1] = world->height / 2;
  arrow.position[2] = 0.0f;

  speed[0] = random_number(0.001, 0.002);
  speed[1] = random_number(0.001, 0.002);

  radius[0] = random_number(300, world->width/2);
  radius[1] = random_number(200, world->height/2);

  center[0] = world->width / 2;
  center[1] = world->height / 2;
}

static void animate() {
  glBindVertexArray(vao);
  main_identity_all();
  mat4x4 model;
  mat4x4_translate(model, arrow.position[0], arrow.position[1], arrow.position[2]);
  mat4x4_rotate(model, model, 0.0f, 0.0f, 1.0f, arrow.angle);
  /* mat4x4_rotate(klkl); */
  main_set_model(model);
  /* main_load_perspective(); */
  /* main_load_camera_view(); */
  main_load_ortho();
  glLineWidth(2.0f);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);
  glDrawArrays(GL_LINES, 0, 6);
  glLineWidth(1.0f);
  glBindVertexArray(0);

  arrow.position[0] = center[0] + cosf(angle[0]) * radius[0];
  arrow.position[1] = center[1] + sinf(angle[1]) * radius[1];

  float dx = mouse.position[0] - arrow.position[0];
  float dy = mouse.position[1] - arrow.position[1];
  arrow.angle = atan2f(dy, dx);

  angle[0] += speed[0];
  angle[1] += speed[1];
  /* printf("MOUSE: [%.2f, %.2f].\n", mouse->default_position[0], mouse->default_position[1]); */
}

void project_arrow_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
