#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static unsigned int vao, vbo, ebo;

static float position[3] = { 5.0f, 0.0f, 0.0f };

struct Square {
  float position[3];
  float scale;
  float width;
  float height;
  float speed;
  float angle;
};

static struct Square square;

static void initialize() {

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  square.position[0] = 0.0f;
  square.position[1] = 0.0f;
  square.position[2] = 0.0f;

  square.speed = 0.001f;
  square.scale = 0.2f;

  square.angle = 0.0f;

  square.width = square.scale;
  square.height = square.scale;
}

static void animate() {

  glBindVertexArray(vao);

  main_identity_all();
  main_load_perspective();
  main_load_camera_view();

  mat4x4 model;
  mat4x4_translate(model, position[0], position[1], position[2]);
  mat4x4_translate_in_place(model, square.position[0], square.position[1], square.position[2]);
  mat4x4_scale_aniso(model, model, square.scale, square.scale, square.scale);
  main_set_model(model);

  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  glBindVertexArray(0);

  square.position[1] = sinf(square.angle) * 3.0f;

  square.angle += square.speed;
}

void project_demo_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
