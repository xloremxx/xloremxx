#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static void initialize() {

}

static void animate() {

}

void project_template_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
