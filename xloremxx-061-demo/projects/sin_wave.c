#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

#define NUM_SQUARES 629

static unsigned int vao, vbo, ebo;
static float position[3] = { -40.0f, -1.0f, -20.0f };

struct Square {
  float position[3];
  float scale[3];
};

static struct Square squares[NUM_SQUARES];

static void square_set_position(struct Square *square, float x, float y, float z) {
  square->position[0] = x;
  square->position[1] = y;
  square->position[2] = z;
}

static void square_set_scale(struct Square *square, float scale) {
  square->scale[0] = scale;
  square->scale[1] = scale;
  square->scale[2] = scale;
}

static void initialize() {
  int i;

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  /* initialize squares */

  float angle = 0.0f;

  for (i=0; i<NUM_SQUARES; i++) {
    struct Square *square = &squares[i];

    float x = angle * 50.0f;
    float y = sinf(angle) * 50.0f;

    square_set_scale(square, 0.05f);
    square_set_position(square, x*square->scale[0], y*square->scale[1], 0.0f);

    angle += 0.01f;
  }
  i = 0;
}

static void animate() {
  int i;

  glBindVertexArray(vao);
  main_load_perspective();
  main_load_camera_view();

  mat4x4 model;

  for (i=0; i<NUM_SQUARES; i++) {
    struct Square *square = &squares[i];

    mat4x4_identity(model);

    mat4x4_translate_in_place(model, position[0], position[1], position[2]);
    mat4x4_translate_in_place(model, square->position[0], square->position[1], square->position[2]);
    mat4x4_scale_aniso(model, model, square->scale[0], square->scale[1], square->scale[2]);
    main_set_model(model);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  }

  glBindVertexArray(0);
}

void project_sin_wave_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)())) {
  registerer(index, initialize, animate);
}
