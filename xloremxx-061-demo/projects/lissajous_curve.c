#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

#define NUM_SQUARES 10

static float angle = 0.0f;
static float speed = 0.005f;

static unsigned int vao, vbo, ebo;

static float position[3] = { -3.0f, 3.0f, 0.0f };

struct Square {
  float position[3];
  float scale;
  float width;
  float height;
  float speed[3];
  float angle[3];
  float radius[3];
};

static struct Square square0;
static struct Square square1;
static struct Square square2;
static struct Square square3;

static struct Square squares[NUM_SQUARES];

static struct Square square_create() {
  struct Square square;

  square.position[0] = 0.0f;
  square.position[1] = 0.0f;
  square.position[2] = 0.0f;

  square.speed[0] = 0.005f;
  square.speed[1] = 0.005f;
  square.scale = 0.05f;

  square.angle[0] = 0.0f;
  square.angle[1] = 0.0f;
  square.angle[2] = 0.0f;

  square.width = square.scale;
  square.height = square.scale;

  return square;
}

static float random_number(float min, float max) {
  float scale = rand() / (float) RAND_MAX;
  return min + scale * ( max - min );
}

static void square_move(struct Square *square, float x, float y, float z) {
  square->position[0] = x;
  square->position[1] = y;
  square->position[2] = z;
}

static void square_update(struct Square *square) {
  mat4x4 model;
  mat4x4_translate(model, position[0], position[1], position[2]);
  mat4x4_translate_in_place(model, square->position[0], square->position[1], square->position[2]);
  mat4x4_scale_aniso(model, model, square->scale, square->scale, square->scale);
  main_set_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static void square_update_special(struct Square *square) {
  mat4x4 model;
  mat4x4_translate(model, position[0], position[1], position[2]);
  mat4x4_translate_in_place(model, square->position[0], square->position[1], square->position[2]);
  mat4x4_scale_aniso(model, model, square->scale, square->scale, square->scale);
  main_set_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  float center[3] = { -2.0f, 0.0f, 0.0f };

  square->position[0] = center[0] + cosf(square->angle[0]) * square->radius[0];
  square->position[1] = center[1] + sinf(square->angle[1]) * square->radius[1];

  square->angle[0] += square->speed[0];
  square->angle[1] += square->speed[1];
}

static void initialize() {
  int i;

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  square0 = square_create();
  square1 = square_create();
  square2 = square_create();
  square3 = square_create();

  for (i=0; i<NUM_SQUARES; i+=1) {
    squares[i] = square_create();

    squares[i].speed[0] = random_number(0.0001f, 0.01f);
    squares[i].speed[1] = random_number(0.0001f, 0.01f);

    squares[i].radius[0] = random_number(0.5f, 1.0f);
    squares[i].radius[1] = random_number(0.5f, 1.0f);
  }

  square2.speed[0] = 0.00431f;
  square2.speed[1] = 0.005f;
  square2.radius[0] = 0.8f;
  square2.radius[1] = 1.0f;
}

static void animate() {
  int i;

  glBindVertexArray(vao);
  main_identity_all();
  main_load_perspective();
  main_load_camera_view();

  square_update(&square0);
  square_update(&square1);
  square_update(&square2);
  square_update(&square3);
  
  for (i=0; i<NUM_SQUARES; i+=1) {
    square_update_special(&squares[i]);
  }

  glBindVertexArray(0);

  float center[2] = {0.0f, 0.0f};
  float radius[2] = {0.25f, 1.0f};

  square1.position[0] = center[0] + cosf(angle) * radius[0];
  square1.position[1] = center[1] + sinf(angle) * radius[1];

  square2.position[0] = center[0] + cosf(square2.angle[0]) * square2.radius[0];
  square2.position[1] = center[1] + sinf(square2.angle[1]) * square2.radius[1];

  square_move(&square0, center[0] + cosf(angle) * 0.5f, center[1] + sinf(angle) * 0.5f, 0.0f);

  angle += speed;

  square2.angle[0] += square2.speed[0];
  square2.angle[1] += square2.speed[1];
}

void project_lissajous_curve_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
