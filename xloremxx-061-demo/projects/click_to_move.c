#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static unsigned int vao, vbo;
static float scale = 10.0f;

static float width, height;

static float center[3];
static float position[3] = { 0.0f, 0.0f, 0.0f };
static float velocity[3] = { 0.0f, 0.0f, 0.0f };
static float direction[3] = { 0.0f, 0.0f, 0.0f };
static float distance[3] = { 0.0f, 0.0f, 0.0f };

static struct {
  float position[3];
  int buttons[10];
} mouse;

static struct {
  float rotation;
  float speed;
  float position[3];
  float destination[3];
} monster;

static void mousemove(float x, float y) {
  mouse.position[0] = x;
  mouse.position[1] = -(y-height);
}

static void mouse_button_callback(int button, int action, int mods) {

  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    monster.destination[0] = mouse.position[0];
    monster.destination[1] = mouse.position[1];
  }

  if (action == GLFW_PRESS) {
    mouse.buttons[button] = 1;
  }
  if (action == GLFW_RELEASE) {
    mouse.buttons[button] = 0;
  }
}

static void initialize() {
  main_register_callback_mousemove(mousemove);
  window_register_mouse_button_callback(mouse_button_callback);

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  float vertices[3*3] = {
    -0.5f, -1.0f, 0.0f,
    +0.5f, -1.0f, 0.0f,
    +0.0f, +1.0f, 0.0f,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  window_get_size_f(&width, &height);

  center[0] = width / 2.0f;
  center[1] = height / 2.0f;
  center[2] = 0.0f;

  monster.position[0] = center[0];
  monster.position[1] = center[1];
  monster.position[2] = 0.0f;

  monster.destination[0] = monster.position[0];
  monster.destination[1] = monster.position[1];
  monster.destination[2] = monster.position[2];

  monster.speed = 0.5f;

  monster.rotation = 0.0f;
}

static void animate() {

  glBindVertexArray(vao);
  program_load_identity();

  program_load_ortho();

  mat4x4 model;

  mat4x4_translate(model, monster.position[0], monster.position[1], monster.position[2]);
  mat4x4_scale_aniso(model, model, scale, scale, scale);
  mat4x4_rotate_Z(model, model, monster.rotation + M_PI / -2);

  program_set_matrix_model(model);

  glDrawArrays(GL_TRIANGLES, 0, 3);

  /* move */

  distance[0] = monster.destination[0] - monster.position[0];
  distance[1] = monster.destination[1] - monster.position[1];

  /* if magnitude is < speed, set to destination */
  if (sqrtf(distance[0] * distance[0] + distance[1] * distance[1]) < monster.speed) {
    monster.position[0] = monster.destination[0];
    monster.position[1] = monster.destination[1];
  } else {
    monster.rotation = atan2f(distance[1], distance[0]);

    monster.position[0] += cosf(monster.rotation) * monster.speed;
    monster.position[1] += sinf(monster.rotation) * monster.speed;
  }
  if (mouse.buttons[GLFW_MOUSE_BUTTON_RIGHT]) {
    monster.destination[0] = mouse.position[0];
    monster.destination[1] = mouse.position[1];
  }
  glBindVertexArray(0);
}

void project_click_to_move_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
