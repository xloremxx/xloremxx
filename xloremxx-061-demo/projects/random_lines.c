#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

static unsigned int vao, vbo;
static const int NUM_LINES = 100;
static mat4x4 model;

static struct World *world;

static void initialize() {

  world = malloc(sizeof(struct World));

  main_load_world(world);

  int i;

  float vertices[NUM_LINES][3];

  int min = -1000;
  int max = 1000;

  for (i=0; i<NUM_LINES; i++) {

    int a = rand() % (max-1-min)+max;
    int b = rand() % (max-1-min)+max;

    vertices[i][0] = (float)a / 1000.0f - 2.0f;
    vertices[i][1] = (float)b / 1000.0f - 2.0f;
    vertices[i][2] = 0.0f;
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  mat4x4_identity(model);
  mat4x4_translate(model, 3.0f, 0.0f, 0.0f);
}

static void animate() {
  glBindVertexArray(vao);
  main_identity_all();
  main_load_perspective();
  main_load_camera_view();
  main_set_model(model);
  glDrawArrays(GL_LINES, 0, NUM_LINES);
  glBindVertexArray(0);
}

void project_random_lines_start(int index, void (*registerer)(int index, void (*initialize)(), void (*animate)())) {
  registerer(index, initialize, animate);
}
