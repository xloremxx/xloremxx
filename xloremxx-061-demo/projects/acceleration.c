#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <math.h>
#include <main.h>

#define NUM_PARTICLES 100

static float position[3] = {-6.0f, +3.0f, 0.0f};

static void vec_set_x(float vec[3], float value) {
  vec[0] = value;
}

static float random_number(float min, float max) {
  float scale = rand() / (float) RAND_MAX;
  return min + scale * ( max - min );
}

static float vec_get_x(float vec[3]) {
  return vec[0];
}

static void vec_set_y(float vec[3], float value) {
  vec[1] = value;
}

static float vec_get_y(float vec[3]) {
  return vec[1];
}

static float vec_get_angle(float vec[3]) {
  return atan2f(vec[1], vec[0]);
}

static void vec_set_length(float vec[3], float length) {
  float angle = vec_get_angle(vec);
  vec[0] = cosf(angle) * length;
  vec[1] = sinf(angle) * length;
}

static float vec_get_length(float vec[3]) {
  return sqrtf(vec[0] * vec[0] + vec[1] * vec[1]);
}

static void vec_set_angle(float vec[3], float angle) {
  float length = vec_get_length(vec);
  vec[0] = cosf(angle) * length;
  vec[1] = sinf(angle) * length;
}

static void vec_add(float vec[3], float A[3], float B[3]) {
  vec[0] = A[0] + B[0];
  vec[1] = A[1] + B[1];
  vec[2] = A[2] + B[2];
}

static void vec_add_in_place(float vec[3], float A[3]) {
  vec[0] += A[0];
  vec[1] += A[1];
  vec[2] += A[2];
}

static void vec_sub(float vec[3], float T[3]) {
  vec[0] -= T[0];
  vec[1] -= T[1];
  vec[2] -= T[2];
}

static void vec_mul(float vec[3], float value) {
  vec[0] *= value;
  vec[1] *= value;
  vec[2] *= value;
}

static void vec_div(float vec[3], float value) {
  vec[0] /= value;
  vec[1] /= value;
  vec[2] /= value;
}

static void vec_add_to(float vec[3], float A[3]) {
  vec[0] += A[0];
  vec[1] += A[1];
  vec[2] += A[2];
}

static void vec_sub_from(float vec[3], float A[3]) {
  vec[0] -= A[0];
  vec[1] -= A[1];
  vec[2] -= A[2];
}

static void vec_mul_by(float vec[3], float val) {
  vec[0] *= val;
  vec[1] *= val;
  vec[2] *= val;
}
static void vec_div_by(float vec[3], float val) {
  vec[0] /= val;
  vec[1] /= val;
  vec[2] /= val;
}

static void vec_set(float vec[3], const float T[3]) {
  vec[0] = T[0];
  vec[1] = T[1];
  vec[2] = T[2];
}

static void vec_setf(float vec[3], float x, float y, float z) {
  vec[0] = x;
  vec[1] = y;
  vec[2] = z;
}

struct Particle {
  float scale[3];
  float position[3];
  float velocity[3];
  float acceleration[3];
  float gravity[3];
};

static int particle_store_count = 0;
static struct Particle **particle_store;
static struct Particle *particles;

static struct Particle *particle_create(float x, float y, float speed, float direction) {
  struct Particle *particle = malloc(sizeof(struct Particle));
  particle_store = realloc(particle_store, sizeof(struct Particle*) * particle_store_count+1);
  particle_store[particle_store_count] = particle;

  vec_setf(particle->acceleration, 0.0f, 0.0f, 0.0f);
  vec_setf(particle->scale, 0.5f, 0.5f, 0.5f);
  vec_setf(particle->position, x, y, 0.0f);
  vec_setf(particle->velocity, 0.0f, 0.0f, 0.0f);
  vec_setf(particle->gravity, 0.0f, 0.0f, 0.0f);

  vec_set_length(particle->velocity, speed);
  vec_set_angle(particle->velocity, direction);

  particle_store_count += 1;
  return particle;
}

static void particle_update(struct Particle *particle) {
  vec_add_to(particle->velocity, particle->gravity);
  vec_add_to(particle->position, particle->velocity);
  vec_add_to(particle->velocity, particle->acceleration);
  mat4x4 model;
  mat4x4_translate(model, position[0], position[1], position[2]);
  mat4x4_translate_in_place(model, particle->position[0], particle->position[1], particle->position[2]);
  mat4x4_scale_aniso(model, model, particle->scale[0], particle->scale[1], particle->scale[1]);
  main_set_model(model);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static unsigned int vao, vbo, ebo;
static float scale = 0.2f;
static float velocity[3] = {0.0f, 0.0f, 0.0f};
static struct Particle *particle;

static void initialize() {
  int i;

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  float vertices[3][4] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f,
  };

  unsigned int indices[] = {
    0, 1, 2,
    0, 2, 3,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  vec_set_length(velocity, 0.001f);
  vec_set_angle(velocity, -M_PI / 6.0f);

  particle = particle_create(0.0f, 0.0f, 0.01f, M_PI / 3);
  vec_setf(particle->position, 0.0f, -5.0f, 0.0f);
  float acceleration[3] = {0.0000005f, -0.000008f};
  vec_set(particle->acceleration, acceleration);

  particles = malloc(sizeof(struct Particle) * NUM_PARTICLES);
  for (i=0; i<NUM_PARTICLES; i+=1) {
    float speed = random_number(0.01f, 0.001f);
    float direction = random_number(M_PI * 2.0f, 1.0f);
    struct Particle *particle = particle_create(0.0f, 0.0f, speed, direction);
    vec_setf(particle->scale, 0.05f, 0.05f, 0.05f);
    float gravity[3] = {0.00000f, -0.000013f, 0.0f};
    vec_set(particle->acceleration, gravity);
  }

}

static void animate() {
  int i;
  glBindVertexArray(vao);

  main_identity_all();
  main_load_perspective();
  main_load_camera_view();

  for (i=0; i<particle_store_count; i+=1) {
    particle_update(particle_store[i]);
  }

  glBindVertexArray(0);
}

void project_acceleration_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}
