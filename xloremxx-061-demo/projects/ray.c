#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <linmath.h>
#include <main.h>

#include <cglm/cglm.h>

/* n - define a normal (facing direction)
 * d - add a distance to offset it from origin
 * */

static unsigned int vao, vbo;
static unsigned int cube_vao, cube_vbo;
static unsigned int plane_vao, plane_ebo, plane_vbo;
static float camera_position[3];

static struct {
  float position[2];
} mouse;

struct Plane {
  float distance;
  float normal[3];
  float position[3];
  float scale;
  float color[3];
};

struct Cube {
  float position[3];
  float scale;
  float color[3];
};

static struct Plane *plane;

static void set_vec3(vec3 v, float x, float y, float z) {
  v[0] = x;
  v[1] = y;
  v[2] = z;
}

static struct Plane *plane_create(float x, float y, float z, float scale, float r, float g, float b) {
  struct Plane *plane = malloc(sizeof(struct Plane));

  set_vec3(plane->position, x, y, z);
  plane->scale = scale;
  set_vec3(plane->color, r, g, b);
  set_vec3(plane->normal, 0.0f, 1.0f, 0.0f);

  plane->distance = 0.0f;

  return plane;
}

static void draw_ray(vec3 origin, vec3 direction, float line_width) {
  vec3 end;

  float length = 100.0f;

  vec3_add(end, origin, direction);

  end[0] *= length;
  end[1] *= length;
  end[2] *= length;

  float vertices[2*3] = {
    origin[0], origin[1], origin[2],
    end[0], end[1], end[2],
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 2 * 3, vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  program_load_identity();
  program_load_default();

  glLineWidth(line_width);
  glDrawArrays(GL_LINES, 0, 2);
  glLineWidth(1.0f);
}

static void draw_vector(vec3 origin, vec3 vector, float line_width) {

  float length = 1.0f;

  float vertices[2*3] = {
    origin[0], origin[1], origin[2],
    origin[0] + vector[0] * length, origin[1] + vector[1] * length, origin[2] + vector[2] * length
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 2 * 3, vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  program_load_identity();
  program_load_default();

  glLineWidth(line_width);
  glDrawArrays(GL_LINES, 0, 2);
  glLineWidth(1.0f);
}

static void draw_line(vec3 start, vec3 end, float r, float g, float b, float size) {
  glBindVertexArray(vao);

  float vertices[2*3] = {
    start[0], start[1], start[2],
    end[0], end[1], end[2],
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 2 * 3, vertices);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  program_load_identity_model();

  program_set_color(r, g, b);
  glLineWidth(size);
  glDrawArrays(GL_LINES, 0, 2);
  glLineWidth(1.0f);

}

static void plane_update(struct Plane *plane) {
  glBindVertexArray(plane_vao);
  mat4x4 model;
  mat4x4_translate(model, plane->position[0], plane->position[1], plane->position[2]);
  mat4x4_scale_aniso(model, model, plane->scale, plane->scale, plane->scale);
  mat4x4_rotate_X(model, model, M_PI/2);
  program_set_matrix_model(model);
  program_set_color(plane->color[0], plane->color[1], plane->color[2]);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

  float x = plane->position[0] - camera_position[0];
  float y = plane->position[1] - camera_position[1];
  float z = plane->position[2] - camera_position[2];

  plane->distance = sqrtf( x*x + y*y + z*z );

  /* printf("distance: %.2f\n", plane->distance); */

  vec3 start = { 0.0f, 0.0f, 0.0f };
  vec3 end = { 0.0f, 50.0f, 0.0f };

  draw_line(plane->position, plane->normal, 1.0f, 0.0f, 0.0f, 5.0f);
}

static struct {
  vec3 origin;
  vec3 direction;
} ray;

static void mousemove(float x, float y) {
  mouse.position[0] = x;
  mouse.position[1] = y;
}

static float width, height;

static void initialize() {

  main_register_callback_mousemove(mousemove);

  window_get_size(&width, &height);

  {
    glGenVertexArrays(1, &plane_vao);
    glGenBuffers(1, &plane_vbo);
    glGenBuffers(1, &plane_ebo);
    float plane_vertices[3][4] = {
      -1.0f, -1.0f, 0.0f,
      +1.0f, -1.0f, 0.0f,
      +1.0f, +1.0f, 0.0f,
      -1.0f, +1.0f, 0.0f,
    };

    unsigned int indices[] = {
      0, 1, 2,
      0, 2, 3,
    };

    glBindVertexArray(plane_vao);
    glBindBuffer(GL_ARRAY_BUFFER, plane_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(plane_vertices), plane_vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, plane_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  {
    glGenVertexArrays(1, &cube_vao);
    glGenBuffers(1, &cube_vbo);

    float cube_vertices[36 * 3];
    data_load_vertices_cube(cube_vertices);

    glBindVertexArray(cube_vao);
    glBindBuffer(GL_ARRAY_BUFFER, cube_vbo);
    glBufferData(GL_ARRAY_BUFFER, 36 * 3 * sizeof(float), cube_vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  float vertices[2*3] = {
    +0.5f, 0.0f, 0.0f,
    -0.5f, 0.0f, 0.0f,
  };

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, 2 * 3 * sizeof(float), vertices, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  ray.origin[0] = 3.0f;
  ray.origin[1] = 3.0f;
  ray.origin[2] = 0.0f;

  ray.direction[0] = 1.0f;
  ray.direction[1] = 1.0f;
  ray.direction[2] = -1.0f;

  plane = plane_create(0.0f, 0.0f, 0.0f, 5.0f, 0.1f, 0.5f, 0.9f);
}

static void animate() {
  camera_get_position(camera_position);
  glBindVertexArray(vao);

  vec3 nds;
  nds[0] = (2.0f * mouse.position[0]) / width - 1.0f;
  nds[1] = 1.0f - (2.0f * mouse.position[1]) / height;
  nds[2] = 1.0f;

  program_load_identity();
  program_load_default();

  draw_ray(ray.origin, ray.direction, 5.0f);

  vec3 origin = {0.0f, 0.0f, 0.0f};
  vec3 demo = {3.0f, 9.0f, 0.0f};
  vec3 demo0 = {-8.0f, -2.0f, 0.0f};

  draw_vector(origin, demo, 5.0f);
  draw_vector(demo, demo0, 5.0f);

  /* printf("%.2f, %.2f, %.2f\n", nds[0], nds[1], nds[2]); */

  { 
    program_load_normalized_device_space();
    float vertices[] = {
      0.0f, 0.0f, 1.0f,
      0.0f, 0.0f, 1.0f,
    };
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 2 * 3, vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_POINTS, 0, 2);
  }

  {

    program_load_normalized_device_space();
    vec3 start, end;
    start[0] = 0.0f;
    start[1] = 0.0f;
    start[2] = 0.0f;
    end[0] = nds[0];
    end[1] = nds[1];
    end[2] = nds[2];
    /* draw_line(start, end); */
  }

  vec4 clip;
  clip[0] = nds[0];
  clip[1] = nds[1];
  clip[2] = -1.0f;
  clip[3] = +1.0f;

  { 
    program_load_normalized_device_space();
    program_set_color(1.0f, 0.0f, 0.0f);
    vec3 start = { 0.0f, 0.0f, 0.0f }, end;
    end[0] = clip[0];
    end[1] = clip[1];
    end[2] = clip[3];
    draw_line(start, end, 1.0f, 1.0f, 1.0f, 2.0f);
  }

  mat4x4 projection, view, inversed_view, inversed_projection;
  camera_calculate_view_matrix(view);
  mat4x4_invert(inversed_view, view);
  program_make_perspective_matrix(projection);
  mat4x4_invert(inversed_projection, projection);


  vec4 eye;
  mat4x4_mul_vec4(eye, inversed_projection, clip);
  eye[2] = -1.0f;
  eye[3] = 0.0f;

  /* program_set_matrix_view(view); */

  {
    program_load_identity();
    program_set_matrix_projection(projection);
    vec3 start = { 0.0f, 0.0f, -10.0f }, end;
    end[0] = eye[0];
    end[1] = eye[1];
    end[2] = eye[3];
    program_set_color(1.0f, 1.0f, 0.0f);
    /* draw_line(start, end); */
  }

  vec4 world;
  mat4x4_mul_vec4(world, inversed_view, eye);
  vec4_norm(world, world);

  {

    program_load_identity();
    program_set_matrix_projection(projection);
    program_set_matrix_view(view);
    /* vec3 start = {20.0f, 20.0f, 0.0f}; */
    vec3 start = {0.0f, 0.0f, 0.0f};
    vec3 end = { world[0], world[1], world[2] };
    /* vec3 start = { camera_position[0], camera_position[1], camera_position[2], }; */
    /* end[0] = world[0]; */
    /* end[1] = world[1]; */
    /* end[2] = world[3]; */
    program_set_color(0.5f, 0.3f, 1.0f);
    /* printf("{%.2f, %.2f, %.2f}.\n", world[0], world[1], world[2]); */
    draw_line(start, end, 1.0f, 1.0f, 1.0f, 2.0f);
    draw_ray(start, end, 5.0f);
    draw_ray(camera_position, world, 30.0f);
    /* draw_line(camera_position, start); */
    /* printf("POSITION: [%.2f, %.2f, %.2f].\n", camera_position[0], camera_position[1], camera_position[2]); */
  }

  {
    program_set_color(1.0f, 0.0f, 1.0f);
    vec3 start = {0.0f, 0.0f, 15.0f};
    vec3 end = {0.0f, 0.0f, 0.0f};
    draw_line(start, world, 1.0f, 1.0f, 1.0f, 2.0f);
  }

  {

    float position[3];

    position[0] = camera_position[0] + world[0] * 45;
    position[1] = camera_position[1] + world[1] * 45;
    position[2] = camera_position[2] + world[2] * 45;

    position[1] += 5.0f;

    glBindVertexArray(cube_vao);
    mat4x4 model;
    mat4x4_translate(model, position[0], position[1], position[2]);
    program_set_matrix_model(model);
    /* program_load_identity(); */
    glDrawArrays(GL_TRIANGLES, 0, 36); 
  }

  /* plane */
  {
    plane_update(plane);
  }

  {
    /* printf("%.2f, %.2f\n", mouse.position[0], mouse.position[1]); */
    /* [0][0] [0][1] [0][2] [0][3]  [x0] */
    /* [1][0] [1][1] [1][2] [1][3]  [x1] */
    /* [2][0] [2][1] [2][2] [2][3]  [x2] */
    /* [3][0] [3][1] [3][2] [3][3]  [x3] */
    /* vec4 my_view; */
    /* my_view[0] = view[0][0] * camera_position[0] - view[0][1] * camera_position[1] - view[0][2] * camera_position[2] - view[0][3] * camera_position[3]; */
    /* my_view[1] = view[1][0] * camera_position[0] - view[1][1] * camera_position[1] - view[1][2] * camera_position[2] - view[1][3] * camera_position[3]; */
    /* my_view[2] = view[2][0] * camera_position[0] - view[2][1] * camera_position[1] - view[2][2] * camera_position[2] - view[2][3] * camera_position[3]; */
    /* my_view[3] = view[3][0] * camera_position[0] - view[3][1] * camera_position[1] - view[3][2] * camera_position[2] - view[3][3] * camera_position[3]; */
    /* vec4_norm(my_view, my_view); */

    /* vec3 up; */
    /* camera_get_up(up); */

    /* vec3 h; */
    /* vec3_mul_cross(h, my_view, up); */
    /* vec3_norm(h, h); */

    /* vec3 v; */
    /* vec3_mul_cross(v, h, my_view); */
    /* vec3_norm(v, v); */

    /* printf("[%.2f, %.2f, %.2f, %.2f]\n", my_view[0], my_view[1], my_view[2], my_view[3]); */
  }

  glBindVertexArray(0);
  program_set_color(1.0f, 1.0f, 1.0f);
}

void project_ray_start(void (*registerer)(void (*initialize)(), void (*animate)())) {
  registerer(initialize, animate);
}

/* print_matrix(projection_matrix); */
/* print_matrix(view_matrix); */

/* using the algorithm */
/* we simply test all boxes one after the other */
/* if you have many objects, you might need an additional acceleration structure like 'Binary Space Partitioning Tree'
 * or a 'Bounding Volume Hierarchy' */

/* int i; */

/* for (i=0; i<10; i++) { */
/*   float intersection_distance; /1* output of the test_ray_intersection() *1/ */

/*   vec3 aabb_min = { -1.0f, -1.0f, -1.0f }; */
/*   vec3 aabb_max = { +1.0f, +1.0f, +1.0f }; */

/* model_matrix transforms */
/* mat4x4_identity(model_matrix); */
/* mat4x4_translate(model_matrix, position[0], position[1], position[2]); */

/* if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) { */
/*   vec3 ray_origin; */
/*   vec3 ray_direction; */

/*   float screen_width; */
/*   float screen_height; */

/*   window_get_size_f(&screen_width, &screen_height); */

/*   screen_pos_to_world_ray(mouse.position[0], mouse.position[1], screen_width, screen_height, view_matrix, projection_matrix, ray_origin, ray_direction); */

/*   int result = test_ray_intersection(ray_origin, ray_direction, aabb_min, aabb_max, model_matrix, &intersection_distance); */

/*   printf("RESULT: '%d'.\n", result); */

/*   /1* if () { *1/ */
/*   /1*   static int i = 0.0f; *1/ */
/*   /1*   printf("MESH [%d]\n", i); *1/ */
/*   /1*   i += 1; *1/ */
/*   /1* } *1/ */
/* } */

/* program_set_matrix_model(model_matrix); */

/* glDrawArrays(GL_TRIANGLES, 0, 36); */

/* } */

static void screen_pos_to_world_ray(float mouse_x, float mouse_y, float screen_width, float screen_height, mat4x4 view_matrix, mat4x4 projection_matrix, vec3 out_origin, vec3 out_direction) {
  /* mouse_x, mouse_y: mouse position in pixels, from bottom-left corner of the window */
  /* screen_width, screen_height: winsow size in pixels */
  /* view_matrix: camera position and orientation */
  /* projection_matrix: camera parameters (ratio, field of view, near and far planes) */
  /* out_origin: output of the ray, starts at the near plane, so if want the raay to start at the camera's position instead, ignore this */
  /* out_direction: direction, in world space, of the ray that goes 'through' the mouse */
  /* ndc - Normalised Device Coordinates */

  /* mouse_y = -(mouse_y - screen_height); */

  /* the ray start and end positions, in normalized device coordinates | tutorial 4 */
  vec4 l_ray_start_ndc;
  l_ray_start_ndc[0] = (mouse_x / screen_width - 0.5f) * 2.0f; /* [0,800] -> [-1,+1] */
  l_ray_start_ndc[1] = (mouse_y / screen_height - 0.5f) * 2.0f; /* [0,600] -> [-1,+1] */
  l_ray_start_ndc[2] = -1.0f; /* the near plane maps to Z = -1 in Normalized Device Coordinates */
  l_ray_start_ndc[3] = 1.0f;

  /* printf("%.2f, %.2f, %.2f\n", l_ray_start_ndc[0], l_ray_start_ndc[1], l_ray_start_ndc[2], l_ray_start_ndc[3]); */

  vec4 l_ray_end_ndc;
  l_ray_end_ndc[0] = (mouse_x / screen_width - 0.5f) * 2.0f;
  l_ray_end_ndc[1] = (mouse_y / screen_height - 0.5f) * 2.0f;
  l_ray_end_ndc[2] = 0.0f;
  l_ray_end_ndc[3] = 1.0f;

  /* the projection matrix goes from camera space to ndc. */
  /* so inverse(projection_matrix) goes from ndc to camera space */

  mat4x4 inverse_projection_matrix;
  mat4x4_invert(inverse_projection_matrix, projection_matrix); /* might be wrong 'invert' */

  /* view matrix goes from world space to camera space */
  /* so inverse view matrix goes from camera space to world space */
  mat4x4 inverse_view_matrix;
  mat4x4_invert(inverse_view_matrix, view_matrix); /* might be wrong 'invert' */

  vec4 l_ray_start_camera;
  /* l_ray_start_camera = inverse_projection_matrix * l_ray_start_ndc; l_ray_start_ndc /= l_ray_start_camera[3] */
  mat4x4_mul_vec4(l_ray_start_camera, inverse_projection_matrix, l_ray_start_ndc);
  l_ray_start_camera[0] /= l_ray_start_camera[3];
  l_ray_start_camera[1] /= l_ray_start_camera[3];
  l_ray_start_camera[2] /= l_ray_start_camera[3];
  l_ray_start_camera[3] /= l_ray_start_camera[3];

  vec4 l_ray_start_world;
  mat4x4_mul_vec4(l_ray_start_world, inverse_view_matrix, l_ray_start_camera);
  l_ray_start_world[0] /= l_ray_start_world[3];
  l_ray_start_world[1] /= l_ray_start_world[3];
  l_ray_start_world[2] /= l_ray_start_world[3];
  l_ray_start_world[3] /= l_ray_start_world[3];

  vec4 l_ray_end_camera;
  mat4x4_mul_vec4(l_ray_end_camera, inverse_projection_matrix, l_ray_end_ndc);
  l_ray_end_camera[0] /= l_ray_end_camera[3];
  l_ray_end_camera[1] /= l_ray_end_camera[3];
  l_ray_end_camera[2] /= l_ray_end_camera[3];
  l_ray_end_camera[3] /= l_ray_end_camera[3];

  vec4 l_ray_end_world;
  mat4x4_mul_vec4(l_ray_end_world, inverse_view_matrix, l_ray_end_camera);
  l_ray_end_world[0] /= l_ray_end_world[3];
  l_ray_end_world[1] /= l_ray_end_world[3];
  l_ray_end_world[2] /= l_ray_end_world[3];
  l_ray_end_world[3] /= l_ray_end_world[3];

  /* glEnable(GL_LINE_SMOOTH); */
  /* glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST); */

  /* faster way (just one inverse) */
  /* mat4x4 M; */
  /* mat4x4 T; */
  /* mat4x4_mul(T, projection_matrix, view_matrix); */
  /* mat4x4_invert(M, T); */
  /* vec4 l_ray_start_world; */
  /* mat4x4_mul_vec4(l_ray_start_world, M, l_ray_start_ndc); */
  /* l_ray_start_world[0] /= l_ray_start_world[3]; */
  /* l_ray_start_world[1] /= l_ray_start_world[3]; */
  /* l_ray_start_world[2] /= l_ray_start_world[3]; */
  /* l_ray_start_world[3] /= l_ray_start_world[3]; */
  /* vec4 l_ray_end_world; */
  /* mat4x4_mul_vec4(l_ray_end_world, M, l_ray_end_ndc); */
  /* l_ray_end_world[0] /= l_ray_end_world[3]; */
  /* l_ray_end_world[1] /= l_ray_end_world[3]; */
  /* l_ray_end_world[2] /= l_ray_end_world[3]; */
  /* l_ray_end_world[3] /= l_ray_end_world[3]; */

  vec3 l_ray_dir_world;
  vec3_sub(l_ray_dir_world, l_ray_end_world, l_ray_start_world);

  vec3_norm(l_ray_dir_world, l_ray_dir_world);

  out_origin[0] = l_ray_start_world[0];
  out_origin[1] = l_ray_start_world[1];
  out_origin[2] = l_ray_start_world[2];

  vec3_norm(out_direction, l_ray_dir_world);

}

static int test_ray_intersection(vec3 ray_origin, vec3 ray_direction, vec3 aabb_min, vec3 aabb_max, mat4x4 model_matrix, float *intersection_distance) {

  /* t_min is the largest 'near' intersection currently found */
  /* t_max is the smallest 'far' intersection currently found */
  /* delta is used to compute the intersectiosn with the planes */

  float t_min = 0.0f;
  float t_max = 100000.0f;

  vec3 OBBposition_worldspace;

  OBBposition_worldspace[0] = model_matrix[3][0];
  OBBposition_worldspace[1] = model_matrix[3][1];
  OBBposition_worldspace[2] = model_matrix[3][2];

  /* delta = OBBposition_worldspace - ray_origin */
  vec3 delta;
  vec3_sub(delta, OBBposition_worldspace, ray_origin);

  /* compute the intersections with the 2 planes that delimit the OBB on the x axis */

  vec3 x_axis;
  x_axis[0] = model_matrix[0][0];
  x_axis[1] = model_matrix[0][1];
  x_axis[2] = model_matrix[0][2];

  /* e = dot( x_axis, delta ) */
  /* f = dot( ray_direction, x_axis ) */

  float e = vec3_mul_inner(x_axis, delta);
  float f = vec3_mul_inner(ray_direction, x_axis);

  if (fabsf(f) <= 0.001f) { /* rare case: the ray is almost parallel to the planes, so they don't have 'intersection' */
    if (-e + aabb_min[0] > 0.0f || -e + aabb_max[0] < 0.0f) {
      return 0;
    }
  }

  /* beaware, don't do the division if f is near 0! */
  float t1 = (e+aabb_min[0]) / f;
  float t2 = (e+aabb_max[0]) / f;

  /* t1 and t2 now contain distances between ray origin and ray-plane intersections, but we don't know in what order, 
   * so we make sure that t1 represents the 'near' intersection and t2 the 'far' */

  if ( t1 > t2 ) {
    float w = t1;
    t1 = t2;
    t2 = w;
  }

  /* update t_min and t_max */

  /* t_max is the nearest 'far' interesection (amongst the x,y and z planes pairs) */
  if ( t2 < t_max ) t_max = t2;

  /* t_min is the farthest 'near' intersection (amongst the x,y and z planes pairs) */
  if ( t1 > t_min ) t_min = t1;

  /* trick: if 'far' is closer than 'near', there is no intersection */
  if (t_max < t_min) {
    return 0;
  }

  /* this was the x axis, all other axes are exectly the same! */

  /* test intersection with the 2 planes perpendicular to the OBB's Y axis */
  vec3 y_axis;
  y_axis[0] = model_matrix[1][0];
  y_axis[1] = model_matrix[1][1];
  y_axis[2] = model_matrix[1][2];

  e = vec3_mul_inner(y_axis, delta);
  f = vec3_mul_inner(ray_direction, y_axis);

  if (fabsf(f) <= 0.001f) { 
    if (-e + aabb_min[1] > 0.0f || -e + aabb_max[1] < 0.0f) {
      return 0;
    }
  }

  t1 = ( e + aabb_min[1] ) / f;
  t2 = ( e + aabb_max[1] ) / f;

  if ( t1 > t2 ) {
    float w = t1;
    t1 = t2;
    t2 = w;
  }

  if ( t2 < t_max ) {
    t_max = t2;
  }
  if ( t1 > t_min ) {
    t_min = t1;
  }
  if (t_min > t_max) {
    return 0;
  }

  /* z axis */
  vec3 z_axis;
  y_axis[0] = model_matrix[2][0];
  y_axis[1] = model_matrix[2][1];
  y_axis[2] = model_matrix[2][2];

  e = vec3_mul_inner(z_axis, delta);
  f = vec3_mul_inner(ray_direction, z_axis);

  if (fabsf(f) <= 0.001f) { 
    if (-e + aabb_min[2] > 0.0f || -e + aabb_max[2] < 0.0f) {
      return 0;
    }
  }

  t1 = ( e + aabb_min[2] ) / f;
  t2 = ( e + aabb_max[2] ) / f;

  if ( t1 > t2 ) {
    float w = t1;
    t1 = t2;
    t2 = w;
  }

  if ( t2 < t_max ) {
    t_max = t2;
  }
  if ( t1 > t_min ) {
    t_min = t1;
  }
  if (t_min > t_max) {
    return 0;
  }

  *intersection_distance = t_min;
  return 1;
}

/* static struct { */
/*   float position[3]; */
/* } mesh; */

/* static struct { */
/*   float position[3]; */
/* } mouse; */

/* float width, height; */

/* void print_matrix(mat4x4 M) { */
/*   int i, j; */

/*   for (i=0; i<4; i++) { */
/*     for (j=0; j<4; j++) { */
/*       printf("%.4f ", M[i][j]); */
/*     } */
/*   } */
/*   printf("\n"); */
/* } */

/* void print_vector(vec4 v) { */
/*   printf("%.2f, %.2f, %.2f, %.2f\n", v[0], v[1], v[2], v[3]); */
/* } */

/* static void mousemove(float x, float y) { */
/*   mouse.position[0] = x; */
/*   mouse.position[1] = y; */
/*   mouse.position[2] = 0; */
/* } */

/* static GLFWwindow* window; */

/* static mat4x4 model_matrix; */

/* static float position[3] = {0.0f, 0.0f, 0.0f}; */

/* static unsigned int vaos[1], vbos[1]; */

/* static void dot() { /1* -> vec3_mul_inner *1/ */

/*   /1* a - vector a magnitude *1/ */
/*   /1* b - vector b magnitude *1/ */
/*   /1* angle - angle between a and b *1/ */

/*   /1* a * b = |a| * |b| * cos(angle)  *1/ */

/*   /1* or *1/ */
/*   /1* a * b = a1 * b1 + a2 * b2 *1/ */

/* } */
/* { */
/*   glGenVertexArrays(1, vaos); */
/*   glGenBuffers(1, vbos); */

/*   float vertices[1 * 3] = { */
/*     0.0f, 0.0f, 0.0f */
/*   }; */

/*   glBindVertexArray(vaos[0]); */
/*   glBindBuffer(GL_ARRAY_BUFFER, vbos[0]); */
/*   glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW); */
/*   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0); */
/*   glEnableVertexAttribArray(0); */
/*   glBindBuffer(GL_ARRAY_BUFFER, 0); */
/*   glBindVertexArray(0); */
/* } */

/* window = window_return_pointer(); */

/* mesh.position[0] = 0.0f; */
/* mesh.position[1] = 0.0f; */
/* mesh.position[2] = 0.0f; */

/* window_get_size_f(&width, &height); */

/* main_register_callback_mousemove(mousemove); */
/* glBindVertexArray(vao); */

/* program_load_identity(); */

/* mat4x4 view; */
/* mat4x4 projection; */
/* mat4x4 model; */

/* camera_load_view_matrix(view); */
/* program_make_perspective_matrix(projection); */

/* mat4x4_translate(model, mesh.position[0], mesh.position[1], mesh.position[2]); */

/* program_set_matrix_view(view); */
/* program_set_matrix_projection(projection); */
/* program_set_matrix_model(model); */
/* program_set_color(1.0f, 1.0f, 0.0f); */

/* glDrawArrays(GL_TRIANGLES, 0, 36); */

/* glBindVertexArray(vaos[0]); */
/* program_load_identity(); */
/* mat4x4_translate(model, 0.0f, 0.0f, 0.0f); */
/* program_set_matrix_model(model); */
/* program_set_color(1.0f, 0.0f, 0.0f); */
/* glDrawArrays(GL_POINTS, 0, 1); */

/* mat4x4 inversed_projection; */
/* mat4x4_invert(inversed_projection, projection); */

/* mat4x4 inversed_view; */
/* mat4x4_invert(inversed_view, view); */

/* { */ 
/*   vec3 currentRay; */
/*   /1* 1. convert viewport mouse coordinates to normalized device space  *1/ */
/*   vec2 nds; */

/*   /1* normalized device coordinates *1/ */
/*   nds[0] = (2.0f * mouse.position[0]) / width - 1.0f; */
/*   nds[1] = 1.0f - (2.0f * mouse.position[1]) / height; */

/*   /1* 2. convert normalized device coordinates to clip space *1/ */
/*   vec4 clip; */

/*   clip[0] = nds[0]; */
/*   clip[1] = nds[1]; */
/*   clip[2] = -1.0f; */
/*   clip[3] = +1.0f; */

/*   /1* 3. convert clip to eye space. *1/ */
/*   vec4 eye; */
/*   mat4x4_mul_vec4(eye, inversed_projection, clip); */
/*   eye[2] = -1.0f; */
/*   eye[3] = 0.0f; */

/*   /1* 4. convert eye space to world space. *1/ */
/*   vec3 world; */
/*   mat4x4_mul_vec4(world, inversed_view, eye); */
/*   vec3_norm(world, world); */
/* } */

/* vec3 ray_nds; */

/* printf("width: %.2f.\n", width); */

/* ray_nds[0] = (2.0f * mouse.position[0]) / width - 1.0f; */
/* ray_nds[1] = 1.0f - (2.0f * mouse.position[1]) / height; */
/* ray_nds[2] = 1.0f; */

/* mat4x4_translate(model, ray_nds[0], ray_nds[1], 0.0f); */
/* program_set_matrix_model(model); */
/* program_set_color(1.0f, 0.0f, 0.0f); */
/* glDrawArrays(GL_POINTS, 0, 1); */

/* vec4 ray_clip; */

/* ray_clip[0] = ray_nds[0]; */
/* ray_clip[1] = ray_nds[1]; */
/* ray_clip[2] = -1.0f; */
/* ray_clip[3] = +1.0f; */

/* vec4 ray_eye; */
/* mat4x4_mul_vec4(ray_eye, inversed_projection, ray_clip); */
/* ray_eye[2] = -1.0f; */
/* ray_eye[3] = 0.0f; */

/* mat4x4_translate(model, ray_eye[0], ray_eye[1], 0.0f); */
/* program_set_matrix_model(model); */
/* program_set_color(1.0f, 0.0f, 0.0f); */
/* glDrawArrays(GL_POINTS, 0, 1); */

/* vec3 ray_wor; */

/* mat4x4_mul_vec4(ray_wor, inversed_view, ray_eye); */
/* vec3_norm(ray_wor, ray_wor); */

/* mat4x4_translate(model, ray_wor[0], ray_wor[1], 0.0f); */
/* program_set_matrix_model(model); */
/* program_set_color(1.0f, 0.0f, 0.0f); */
/* glDrawArrays(GL_POINTS, 0, 1); */

/* glBindVertexArray(0); */

/* printf("---------------------------------------------------------------\n"); */
/* printf("[model]\t\t\t"); */
/* print_matrix(model); */
/* printf("[view]\t\t\t"); */
/* print_matrix(view); */
/* printf("[projection]\t\t"); */
/* print_matrix(projection); */
/* printf("[mesh] POSITION:\t[%.2f, %.2f, %.2f].\n", mesh.position[0], mesh.position[1], mesh.position[2]); */
/* printf("[mouse] POSITION:\t[%.2f, %.2f, %.2f].\n", mouse.position[0], mouse.position[1], mouse.position[2]); */
/* printf("[RAY NDS]:\t\t[%.2f, %.2f, %.2f].\n", ray_nds[0], ray_nds[1], ray_nds[2]); */
/* printf("[RAY CLIP]:\t\t[%.2f, %.2f, %.2f, %.2f].\n", ray_clip[0], ray_clip[1], ray_clip[2], ray_clip[3]); */
/* printf("[RAY EYE]:\t\t[%.2f, %.2f, %.2f, %.2f].\n", ray_eye[0], ray_eye[1], ray_eye[2], ray_eye[3]); */
/* printf("[inversed_projection]\t"); */
/* print_matrix(inversed_projection); */
/* printf("[inversed_view]\t\t"); */
/* print_matrix(inversed_view); */
/* printf("[ray_wor] POSITION:\t[%.2f, %.2f, %.2f].\n", ray_wor[0], ray_wor[1], ray_wor[2]); */

/* program_load_identity(); */
/* program_set_color(1.0f, 1.0f, 1.0f); */
