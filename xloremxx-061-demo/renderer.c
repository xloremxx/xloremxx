#include <stdlib.h>

static int animate_count = 0;
static void (**animate_callbacks)();

void renderer_register_animate(void (*animate)()) {
  animate_callbacks = realloc(animate_callbacks, sizeof(void*) * animate_count+1);
  animate_callbacks[animate_count] = animate;
  animate_count += 1;
}

void renderer_start() {

}

void renderer_update() {
  int i;

  for (i=0; i<animate_count; i+=1) {
    animate_callbacks[i]();
  }
}
